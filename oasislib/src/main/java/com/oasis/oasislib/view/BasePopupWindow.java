package com.oasis.oasislib.view;

import android.content.Context;
import android.view.WindowManager;
import android.widget.PopupWindow;

public class BasePopupWindow extends PopupWindow {
    protected int SCREEN_WIDTH;
    protected int SCREEN_HEIGHT;

    public BasePopupWindow(Context context) {
        super(context);
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        SCREEN_WIDTH = wm.getDefaultDisplay().getWidth();
        SCREEN_HEIGHT = wm.getDefaultDisplay().getHeight();

        setFocusable(true);
    }

}
