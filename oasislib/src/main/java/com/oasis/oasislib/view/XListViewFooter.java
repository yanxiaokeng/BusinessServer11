package com.oasis.oasislib.view;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.oasis.oasislib.R;


public class XListViewFooter extends LinearLayout {

	public final static int STATE_NORMAL = 0;
	public final static int STATE_READY = 1;
	public final static int STATE_LOADING = 2;

	private View mContentView;
	private ImageView mImageView;
	private TextView mHintView;

	public XListViewFooter(Context context) {
		super(context);
		initView(context);
	}

	public XListViewFooter(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView(context);
	}

	private void initView(Context context) {
		LinearLayout moreView = (LinearLayout) LayoutInflater.from(context)
				.inflate(R.layout.xlistview_footer, null);
		addView(moreView);
		moreView.setLayoutParams(new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		mContentView = moreView.findViewById(R.id.xlistview_footer_content);
		mImageView = (ImageView) moreView.findViewById(R.id.xlistview_footer_img);
		mHintView = (TextView) moreView
				.findViewById(R.id.xlistview_footer_hint_textview);
	}

	public void setState(int state) {

		if (state == STATE_READY) {
            mImageView.setImageResource(R.mipmap.ic_pulltorefresh_arrow_up);
			mHintView.setText(R.string.pull_to_refresh_footer_pull_label);
		} else if (state == STATE_LOADING) {
			mImageView.setImageResource(R.mipmap.default_ptr_rotate);
            mHintView.setText(R.string.pull_to_refresh_footer_refreshing_label);
		} else {
            mImageView.setImageResource(R.mipmap.ic_pulltorefresh_arrow);
			mHintView.setText(R.string.pull_to_refresh_footer_release_label);
		}
	}

	public void setBottomMargin(int height) {
		if (height < 0)
			return;
		LayoutParams lp = (LayoutParams) mContentView
				.getLayoutParams();
		lp.bottomMargin = height;
		mContentView.setLayoutParams(lp);
	}

	public int getBottomMargin() {
		LayoutParams lp = (LayoutParams) mContentView
				.getLayoutParams();
		return lp.bottomMargin;
	}

	public ImageView getmImageView() {
		return mImageView;
	}

	/**
	 */
	public void normal() {
		mHintView.setVisibility(View.VISIBLE);
		mImageView.setVisibility(View.GONE);
	}

	/**
	 */
	public void loading() {
		mHintView.setVisibility(View.GONE);
		mImageView.setVisibility(View.VISIBLE);
	}

	/**
	 */
	public void hide() {
		LayoutParams lp = (LayoutParams) mContentView.getLayoutParams();
		lp.height = 0;
		mContentView.setLayoutParams(lp);
	}

	/**
	 */
	public void show() {
		LayoutParams lp = (LayoutParams) mContentView
				.getLayoutParams();
		lp.height = LayoutParams.WRAP_CONTENT;
		mContentView.setLayoutParams(lp);
	}
	
	/**
	 * */
	public void showResult(String text){
		mHintView.setText(text);
		mImageView.setVisibility(View.GONE);
	}
}
