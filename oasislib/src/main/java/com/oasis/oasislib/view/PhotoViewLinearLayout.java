package com.oasis.oasislib.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;


public class PhotoViewLinearLayout extends LinearLayout {
    public PhotoViewLinearLayout(Context context) {
        super(context);
    }

    public PhotoViewLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PhotoViewLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }



    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        try {
            return super.onInterceptTouchEvent(ev);
        } catch (IllegalArgumentException e) {
            //uncomment if you really want to see these errors
            //e.printStackTrace();
            return false;
        }
    }
}
