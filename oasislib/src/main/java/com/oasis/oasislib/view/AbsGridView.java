package com.oasis.oasislib.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

public class AbsGridView extends GridView {
    public AbsGridView(Context context) {
        super(context);
    }

    public AbsGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AbsGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // TODO Auto-generated method stub
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
