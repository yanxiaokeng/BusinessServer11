package com.oasis.oasislib.view;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by simaben on 16/6/6.
 */
public class VerificationCodeButton extends Button {

    private static final int MSG_START = 1;
    private static final int MSG_END = 2;
    private static final int MSG_STOP = 3;
    private static final int DEFAULT_DURATION = 60;

    int duration = DEFAULT_DURATION;//一分钟
    int currentDuration;
    boolean isDuration = false;
    String btnText;

    Context context;

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
        currentDuration = duration;
    }

    public VerificationCodeButton(Context context) {
        this(context, null);
    }

    public VerificationCodeButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VerificationCodeButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setClickable(true);
        this.context = context;
        currentDuration = duration;
        btnText = getText().toString();
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (isDuration) return;
        super.setEnabled(enabled);
        if (enabled) {
            currentDuration = duration;
        } else {
            duration();
        }
    }

    public void start() {
        setEnabled(false);
        isDuration = true;
    }

    public void stop(String reptyStr) {
        handler.removeMessages(MSG_START);
        Message msg = Message.obtain();
        msg.what = MSG_STOP;
        msg.obj = reptyStr;
        handler.sendMessage(msg);
    }


    private void duration() {
        if (currentDuration == 1) {
            handler.sendEmptyMessage(MSG_END);
        } else {
            currentDuration--;
            setText(currentDuration + "S");
            handler.sendEmptyMessageDelayed(MSG_START, 1000);
        }
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_START:
                    duration();
                    break;
                case MSG_END:
                    isDuration = false;
                    setEnabled(true);
                    setText(btnText);
                    break;
                case MSG_STOP:
                    isDuration = false;
                    setEnabled(true);
                    if (msg.obj != null) {
                        setText(msg.obj.toString());
                    }
                    break;
            }

        }
    };

}