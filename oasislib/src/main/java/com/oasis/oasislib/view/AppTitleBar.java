package com.oasis.oasislib.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.oasis.oasislib.R;


public class AppTitleBar extends LinearLayout {

    public static final int STATUS_ENABLE_TRUE = 0;
    public static final int STATUS_ENABLE_FALSE = 1;

    private Context mContext;
    private LayoutInflater mInflater;
    private ImageView leftImage;
    private View titleBarView;
    private View allLeftView;
    private ImageView rightImageView;
    private TextView titleBarRightText;
    private TextView titleBarCenterTitle;

    private TextView titleBarLeftButton ;


    public AppTitleBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        initView(context);
    }

    public AppTitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        initView(context);
        // TODO Auto-generated constructor stub
    }

    public AppTitleBar(Context context) {
        super(context);
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        initView(context);
    }

    /**
     * rightTextView Button 16sp
     * titleBarRightText  TextView 14sp
     */
    private void initView(Context context) {
        // TODO Auto-generated method stub
        View titleView = mInflater.inflate(R.layout.title_bar, this, true);
        titleBarView = titleView.findViewById(R.id.titleBar);
//        RelativeLayout.LayoutParams layoutParamsWW = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        leftImage = (ImageView) titleView.findViewById(R.id.titleBarLeftImage2);
        allLeftView = titleView.findViewById(R.id.titleBarAllLeftView);
        rightImageView = (ImageView) titleView.findViewById(R.id.titleBarRightImage);
        titleBarCenterTitle = (TextView) titleView.findViewById(R.id.titleBarCenterTitle);
        titleBarRightText = (TextView) titleView.findViewById(R.id.titleBarRightText);
        titleBarLeftButton = (TextView) titleView.findViewById(R.id.titleBarLeftButton);
//        this.addView(titleBarView, layoutParamsWW);
    }

    public void setbacktext(String titleStr) {

        titleBarLeftButton.setText(titleStr);
    }


    public void setCenterTitle(String titleStr) {
        titleBarCenterTitle.setText(titleStr);
        titleBarCenterTitle.setVisibility(VISIBLE);
    }

    public void setCenterTitle(int titleRes) {
        titleBarCenterTitle.setText(titleRes);
        titleBarCenterTitle.setVisibility(VISIBLE);
    }

    public void setLeftImageDrawable(int resId) {
        leftImage.setBackgroundResource(resId);
        leftImage.setVisibility(VISIBLE);
    }

    public void setLeftImageVisiable(boolean isshow) {
        leftImage.setVisibility(isshow ? VISIBLE : GONE);
    }

    public void setRightImageVisiable(boolean isshow) {
        rightImageView.setVisibility(isshow ? VISIBLE : GONE);
    }

    public void setTitleBarRightText(int resId) {
        titleBarRightText.setText(resId);
        titleBarRightText.setVisibility(VISIBLE);
    }
    public void setTitleBarRightText(String resId) {
        titleBarRightText.setText(resId);
        titleBarRightText.setVisibility(VISIBLE);
        if (TextUtils.isEmpty(resId))titleBarRightText.setVisibility(GONE);
    }


    public void setTitleBarRightTextOnClickListener(OnClickListener listener){
        titleBarRightText.setOnClickListener(listener);
    }

    public void setRightImageDrawable(int resId) {
        rightImageView.setBackgroundResource(resId);
        rightImageView.setVisibility(VISIBLE);
    }

    public void setLeftImageOnClickListener(OnClickListener listener) {
        leftImage.setOnClickListener(listener);
    }

    public void setBackViewVisiable(boolean isshow) {
        allLeftView.setVisibility(isshow ? VISIBLE : GONE);
    }

    public void setBackListener(OnClickListener listener) {
        allLeftView.setOnClickListener(listener);
    }

    public void setTitleBarBackgroundColor(int resId) {
        titleBarView.setBackgroundColor(resId);
    }


    public void setRightImageViewOnClickListener(OnClickListener listener) {
        rightImageView.setOnClickListener(listener);
    }

    public void setRightTextViewOnClickListener(OnClickListener listener) {
        titleBarRightText.setOnClickListener(listener);
    }


}
