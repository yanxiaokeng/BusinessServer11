package com.oasis.oasislib.utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;




/**
 * Created by simaben on 16/6/21.
 */
public class ToastUtils {
    public static void shortToast(Context context,String msg) {
        if (TextUtils.isEmpty(msg)) return;
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void shortToast(Context context,int msgId) {
        Toast.makeText(context, msgId, Toast.LENGTH_SHORT).show();
    }

}
