package com.oasis.oasislib.utils;

import android.graphics.Bitmap;
import android.graphics.Matrix;

/**
 * Created by liling on 2016/8/25.
 */
public class ImageUtils {
    /**
     * 计算 bitmap大小，如果超过64kb，则进行压缩
     * @param bitmap
     * @return
     */
    private Bitmap ImageCompressL(Bitmap bitmap) {
        double targetwidth = Math.sqrt(64.00 * 1000);
        if (bitmap.getWidth() > targetwidth || bitmap.getHeight() > targetwidth) {
            // 创建操作图片用的matrix对象
            Matrix matrix = new Matrix();
            // 计算宽高缩放率
            double x = Math.max(targetwidth / bitmap.getWidth(), targetwidth
                    / bitmap.getHeight());
            // 缩放图片动作
            matrix.postScale((float) x, (float) x);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                    bitmap.getHeight(), matrix, true);
        }
        return bitmap;
    }


    /***
     * 图片压缩方法二
     *
     * @param bgimage
     *            ：源图片资源
     * @param newWidth
     *            ：缩放后宽度
     * @param newHeight
     *            ：缩放后高度
     * @return
     */
    public static Bitmap zoomImage(Bitmap bgimage, double newWidth, double newHeight) {
        // 获取这个图片的宽和高
        float width = bgimage.getWidth();
        float height = bgimage.getHeight();
        // 创建操作图片用的matrix对象
        Matrix matrix = new Matrix();
        // 计算宽高缩放率
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // 缩放图片动作
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap bitmap = Bitmap.createBitmap(bgimage, 0, 0, (int) width,
                (int) height, matrix, true);
        return bitmap;
    }

    /**
     * /storage/emulated/0/Seen/uploadImage/1468468035980_share.jpeg
     * @param absolupath
     * @return
     */
    public static  String getFileName(String absolupath){
        int index = absolupath.lastIndexOf("/")+1 ;
        String result = absolupath.substring(index,absolupath.length()) ;
        return  result ;
    }

    /**
     * /storage/emulated/0/Seen/uploadImage/1468468035980_share.jpeg
     * @param absolupath
     * @return
     */
    public static  String getFilePath(String absolupath){
        int index = absolupath.lastIndexOf("/")+1 ;
        String result = absolupath.substring(0,index) ;
        return  result ;
    }


}
