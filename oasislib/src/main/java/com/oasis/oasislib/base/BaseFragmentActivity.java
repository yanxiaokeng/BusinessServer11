package com.oasis.oasislib.base;


import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Toast;



import com.githang.statusbar.StatusBarCompat;
import com.oasis.oasislib.R;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.PreferencesUtils;
import com.oasis.oasislib.utils.ToastUtils;
import com.oasis.oasislib.view.AppTitleBar;

import org.xutils.common.Callback;

import org.xutils.http.HttpMethod;
import org.xutils.http.RequestParams;
import org.xutils.x;

public  class BaseFragmentActivity extends AppCompatActivity {

    //ok.setNormalBackgroundColor(getResources().getColor(R.color.app_red));



    public void loadData(RequestParams requestParams , final HttpCallBack callBack){

        x.http().request(HttpMethod.POST, requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                callBack.onSuccess(result);
                //
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                callBack.onError(ex,isOnCallback);
            }

            @Override
            public void onCancelled(CancelledException cex) {
                callBack.onCancelled(cex);
            }

            @Override
            public void onFinished() {
                callBack.onFinished();
            }
        }) ;


    }

    protected final String TAG = this.getClass().getSimpleName();
    private BroadcastReceiver mExitRecevier = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            exit();
        }
    };
    public AppTitleBar mTitleBar;
    protected Context context;
    public PreferencesUtils preferencesUtils;
    public void exit() {
        this.finish();
    }
    public ActionBar actionBar;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtil.d(TAG, this.getClass().getSimpleName() + " onCreate() invoked!!");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        registerReceiver(mExitRecevier, new IntentFilter("android.action.exit"));
        context = this;
        x.view().inject(this);
        preferencesUtils = new PreferencesUtils(this);
        initTitleBar();
        start();



    }

    protected void start() {

    }

    protected void initTitleBar() {
        mTitleBar = new AppTitleBar(this);
        mTitleBar.setBackListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                finish();
            }
        });
        actionBar = getSupportActionBar();
        if (null != actionBar) {
            actionBar.setElevation(0);

            actionBar.setCustomView(mTitleBar, new ActionBar.LayoutParams(
                    ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT));
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);

            Toolbar parent =(Toolbar)mTitleBar.getParent();
            parent.setContentInsetsAbsolute(0,0);
        }
    }


    protected void showMessage(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }


    @Override
    protected void onStart() {
        super.onStart();
        LogUtil.e(TAG, this.getClass().getSimpleName() + " onStart() invoked!!");
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogUtil.e(TAG, this.getClass().getSimpleName() + " onResume() invoked!!");

    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        LogUtil.e(TAG, this.getClass().getSimpleName() + " onPause() invoked!!");
        super.onPause();
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        LogUtil.e(TAG, this.getClass().getSimpleName() + " onStop() invoked!!");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        LogUtil.e(TAG, this.getClass().getSimpleName() + " onDestroy() invoked!!");
        unregisterReceiver(mExitRecevier);
        super.onDestroy();
    }


    public void exitApp() {
        Intent intent = new Intent();
        intent.setAction("android.action.exit");
        sendBroadcast(intent);
    }

    public ActionBar getActivityActionBar() {
        if (actionBar != null) {
            return actionBar;
        }
        return null;
    }


    public void replace(Fragment f) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.contentPanel, f)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void showPage(Fragment fragment) {
        replace(fragment);
    }

    public void add(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.contentPanel, fragment)
                .commitAllowingStateLoss();
    }
    public void pop(){
        getSupportFragmentManager().popBackStack();
    }

    public void dismissDialog(Dialog dialog) {
        if (dialog != null && dialog.isShowing() && !isFinishing()) {
            dialog.dismiss();
        }
    }

    public void showDialog(Dialog dialog) {
        if (dialog != null && !dialog.isShowing() && !isFinishing()) {
            dialog.show();
        }
    }

    public void showMsg(final String s){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ToastUtils.shortToast(context,s);
            }
        });
    }
}
