package com.oasis.oasislib.base;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.PreferencesUtils;
import com.oasis.oasislib.utils.ToastUtils;

import org.xutils.HttpManager;
import org.xutils.common.Callback;
import org.xutils.http.HttpMethod;
import org.xutils.http.RequestParams;
import org.xutils.x;

/**
 * Created by liling on 2015/11/24.
 */
public abstract class BaseFragment<T extends Context> extends Fragment {

    public T mParent;
    public View rootView;
    public PreferencesUtils preferencesUtils;


    public void loadData(RequestParams requestParams , final HttpCallBack callBack){



        x.http().request(HttpMethod.POST, requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                callBack.onSuccess(result);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                callBack.onError(ex,isOnCallback);
            }

            @Override
            public void onCancelled(CancelledException cex) {
                callBack.onCancelled(cex);
            }

            @Override
            public void onFinished() {
                callBack.onFinished();
            }
        }) ;


    }

    public void dismissDialog(Dialog dialog) {
        if (dialog != null && dialog.isShowing() && !getActivity().isFinishing()) {
            dialog.dismiss();
        }
    }

    public void showDialog(Dialog dialog) {
        if (dialog != null && !dialog.isShowing() && !getActivity().isFinishing()) {
            dialog.show();
        }
    }

    public Dialog dialog;

    public void showMessage(String s) {
        Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
    }

    protected final String TAG = this.getClass().getSimpleName();

    private boolean injected = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        injected = true;
        rootView = x.view().inject(this, inflater, container);
        preferencesUtils = new PreferencesUtils(mParent);
        start();
        return rootView;
    }

    protected abstract void start();

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LogUtil.e(TAG, this.getClass().getSimpleName() + " onViewCreated() invoked!!");
        if (!injected) {
            x.view().inject(this, this.getView());
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        LogUtil.e(TAG, this.getClass().getSimpleName() + " onActivityResult() invoked!!");
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        LogUtil.e(TAG, this.getClass().getSimpleName() + " onAttach() invoked!!");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        LogUtil.e(TAG, this.getClass().getSimpleName() + " onCreate() invoked!!");
        mParent = (T) getActivity();
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        LogUtil.e(TAG, this.getClass().getSimpleName() + " onDestroy() invoked!!");
    }

    @Override
    public void onDestroyView() {
        // TODO Auto-generated method stub
        super.onDestroyView();
        LogUtil.e(TAG, this.getClass().getSimpleName() + " onDestroyView() invoked!!");
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        LogUtil.e(TAG, this.getClass().getSimpleName() + " onDetach() invoked!!");
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {
        // TODO Auto-generated method stub
        super.onOptionsMenuClosed(menu);
        LogUtil.e(TAG, this.getClass().getSimpleName() + " onOptionsMenuClosed() invoked!!");
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        LogUtil.e(TAG, this.getClass().getSimpleName() + " onResume() invoked!!");
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        LogUtil.e(TAG, this.getClass().getSimpleName() + " onStart() invoked!!");
    }

    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        LogUtil.e(TAG, this.getClass().getSimpleName() + " onStop() invoked!!");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        // TODO Auto-generated method stub
        super.setUserVisibleHint(isVisibleToUser);
        LogUtil.e(TAG, this.getClass().getSimpleName() + " setUserVisibleHint() invoked!!");
    }

    @Override
    public void startActivity(Intent intent) {
        // TODO Auto-generated method stub
        super.startActivity(intent);
        LogUtil.e(TAG, this.getClass().getSimpleName() + " startActivity() invoked!!");
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        // TODO Auto-generated method stub
        super.startActivityForResult(intent, requestCode);
        LogUtil.e(TAG, this.getClass().getSimpleName() + " startActivityForResult() invoked!!");
    }

    public Bundle getArgs() {
        Bundle args = getArguments();
        if (args == null) args = new Bundle();
        return args;
    }

    public void showMsg(final String s){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ToastUtils.shortToast(getActivity(),s);
            }
        });
    }
}
