package com.oasis.oasislib.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.oasis.oasislib.R;

import java.util.ArrayList;

/**
 * Created by Administrator on 2018/6/29 0029.
 */
public class ImageShowActivity extends BaseFragmentActivity {
    private TextView tvPicIndex;
    private RelativeLayout imageViewerGuide;
    private ImageView imageViewerGuideLeft;
    private ImageView imageViewerGuideRight;
    private ViewPager mViewPager;
    private ImageView change ;

    private Context context = ImageShowActivity.this;

    private ImageLoaderConfiguration config;
    private DisplayImageOptions options;

    private ArrayList<String> contentList;
    private ArrayList<View> viewList;

    private int len;
    private int current ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imageviewer);
        change = (ImageView) findViewById(R.id.btIvOriginal) ;


        config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .memoryCacheExtraOptions(480, 800) // default = device screen dimensions
                .threadPoolSize(3) // default
                .threadPriority(Thread.NORM_PRIORITY - 1) // default
                .tasksProcessingOrder(QueueProcessingType.FIFO) // default
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024).memoryCacheSizePercentage(13) // default
                .discCacheSize(50 * 1024 * 1024).discCacheFileCount(100)
                .writeDebugLogs().build();
        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.mipmap.preview_card_pic_loading) // resource or drawable
                .cacheInMemory(false)
                .cacheOnDisc(true)
                .build();
        ImageLoader.getInstance().init(config);

        tvPicIndex = (TextView) findViewById(R.id.tvPicIndex);
        imageViewerGuide = (RelativeLayout) findViewById(R.id.imageViewerGuide);
        imageViewerGuideLeft = (ImageView) findViewById(R.id.imageViewerGuideLeft);
        imageViewerGuideRight = (ImageView) findViewById(R.id.imageViewerGuideRight);
        mViewPager = (ViewPager) findViewById(R.id.picPager);

        Intent fromIntent = getIntent();

        contentList = fromIntent.getStringArrayListExtra("showdata");
        current = fromIntent.getIntExtra("current", 0) ;
        if (contentList != null) {
            len = contentList.size();
            tvPicIndex.setText("1/" + len);
            if (len > 0) {
                for (int i = 0; i < len; i++) {
                    ImageView imageView = new ImageView(context);
                    imageView.setLayoutParams(new LayoutParams(
                            LayoutParams.WRAP_CONTENT,
                            LayoutParams.WRAP_CONTENT));
                    imageView.setAdjustViewBounds(true);
                    imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    if (viewList == null) {
                        viewList = new ArrayList<View>();
                    }
                    viewList.add(imageView);
                }
            }
        }

        mViewPager.setAdapter(new MyViewPagerAdapter());
        mViewPager.setOnPageChangeListener(new ImageShowPageChangeListener());
        mViewPager.setCurrentItem(current) ;

        change.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
            }
        }) ;
    }

    // 指引页面更改事件监听器
    class ImageShowPageChangeListener implements OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onPageSelected(int arg0) {
            tvPicIndex.setText((arg0 + 1) + "/" + len);
        }
    }

    class MyViewPagerAdapter extends PagerAdapter {
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            // TODO Auto-generated method stub
            // super.destroyItem(container, position, object);
            container.removeView(viewList.get(position));
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            // TODO Auto-generated method stub
            container.addView(viewList.get(position));
            ImageView i = (ImageView) viewList.get(position);

            i.setOnClickListener(new MyListener(position));
            String path = "";
            if (contentList.get(position).contains("http")||contentList.get(position).contains("https")) {
                path = contentList.get(position);
            } else {
                path = "file://" + contentList.get(position);
            }
            ImageLoader.getInstance().displayImage(path, i, options);
            return viewList.get(position);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return contentList.size();
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            // TODO Auto-generated method stub
            return arg0 == arg1;
        }

        class MyListener implements OnClickListener {

            int position;

            public MyListener(int p) {
                this.position = p;
            }

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        }
    }
}
