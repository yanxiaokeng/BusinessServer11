package com.oasis.oasislib.base;

import android.app.Activity;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;


import com.oasis.oasislib.R;

import org.xutils.common.Callback;
import org.xutils.http.HttpMethod;
import org.xutils.http.RequestParams;
import org.xutils.image.ImageOptions;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter基类
 */
public abstract class AppBaseAdapter<T> extends BaseAdapter {

    public List<T> list = new ArrayList<T>();
    public LayoutInflater inflater;
    public Activity mContext;
    public Resources mResource;
    public static ImageOptions options;

    public AppBaseAdapter(Activity context) {
        this.mContext = context;

        inflater = LayoutInflater.from(mContext);
        mResource = context.getResources();

        options = new ImageOptions.Builder()
                .setImageScaleType(ImageView.ScaleType.FIT_XY)
                .setLoadingDrawableId(R.mipmap.jiazai1)
                .setFailureDrawableId(R.mipmap.jiazai1)
                .setUseMemCache(true)
                .setRadius(5)
                .setSize(150, 150)
                .build();
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public T getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return convertView;
    }

    public void refreshAdapter(List<T> arrayList) {
        if (arrayList != null) {
            list.addAll(arrayList);
            notifyDataSetChanged();
        }
    }

    public void refreshAdapter(int index, List<T> arrayList) {
        if (arrayList != null) {
            list.addAll(index, arrayList);
            notifyDataSetChanged();
        }
    }

    public void refreshAllAdapter(List<T> arrayList) {
        if (arrayList != null) {
            list.clear();
            list.addAll(arrayList);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }

    public void remove(int index) {
        list.remove(index);
        notifyDataSetChanged();
    }

    public void remove(Object obj) {
        list.remove(obj);
        notifyDataSetChanged();
    }

    public void add(T obj) {
        list.add(obj);
        notifyDataSetChanged();
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }


    public void update(int index, T bean) {
        list.remove(index);
        list.add(index, bean);
        notifyDataSetChanged();
    }

    public void loadData(RequestParams requestParams , final HttpCallBack callBack){




        x.http().request(HttpMethod.POST, requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                callBack.onSuccess(result);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                callBack.onError(ex,isOnCallback);
            }

            @Override
            public void onCancelled(CancelledException cex) {
                callBack.onCancelled(cex);
            }

            @Override
            public void onFinished() {
                callBack.onFinished();
            }
        }) ;


    }
}
