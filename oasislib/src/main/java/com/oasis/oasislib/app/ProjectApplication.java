package com.oasis.oasislib.app;

import android.app.Application;
import android.os.Environment;


import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.Utils;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import org.xutils.x;
import java.io.File;

/**
 * Application 基础类 做一些初始化
 */
public class ProjectApplication extends Application {

    private final String appPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/House/";
    private final String imageCachePath = appPath + "cacheImage/";

    private final String imageUploadPath = appPath + "uploadImage/";
    private static ProjectApplication instance;

    public static ProjectApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = (ProjectApplication) getApplicationContext();
        Utils.init(instance);

        if(!NetworkUtils.isConnected()){
            com.blankj.utilcode.util.ToastUtils.showLong("网络不可用");
        }

        //初始化xutils
        x.Ext.init(this);
        //x.Ext.setDebug(true);

        //创建默认的ImageLoader配置参数
        ImageLoaderConfiguration configuration = new ImageLoaderConfiguration.Builder(this)
                .memoryCache(new LruMemoryCache(4 * 1024 * 1024))
                .memoryCacheSize(4 * 1024 * 1024)
                .diskCache(new UnlimitedDiscCache(new File(imageCachePath)))
                .denyCacheImageMultipleSizesInMemory()
                .imageDownloader(new BaseImageDownloader(this, 5 * 1000, 30 * 1000)) // connectTimeout (5 s), readTimeout (30 s)超时时间
                .build();
        ImageLoader.getInstance().init(configuration);

    }




    public String getImageUploadPath() {
        return imageUploadPath;
    }

    public String getImageCachePath() {
        return imageCachePath;
    }

    public String getAppPath() {
        return appPath;
    }

    public static boolean isRequestWrong(String status) {
        return status.equals("wrong");
    }


}
