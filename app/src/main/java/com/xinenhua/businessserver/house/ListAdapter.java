package com.xinenhua.businessserver.house;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.oasis.oasislib.base.AppBaseAdapter;
import com.oasis.oasislib.utils.LogUtil;
import com.xinenhua.businessserver.R;

import org.apache.commons.lang3.StringUtils;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import co.lujun.androidtagview.TagContainerLayout;

/**
 * 实体房源列表适配器！
 */

public class ListAdapter extends AppBaseAdapter<ListPojo> {

    public ListAdapter(Activity context) {
        super(context);
        this.context = context;
    }

    private Context context;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.houselistitem, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final ListPojo t = list.get(position) ;

        LogUtil.e("!!!!!!!!!",t.toString());

        //图片
        if(!StringUtils.isBlank(t.getImage())){
            Glide.with(context)
                    .load(t.getImage())
                    .asBitmap()
                    .placeholder(R.mipmap.bannerdefault)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imageView11);
        }
        //ImageLoader.getInstance().displayImage(t.getImage(),holder.imageView11);
        //title
        holder.title.setText(t.getTitle());
        //标题
        if(t.getTag()!=null&&!"".equals(t.getTag())){
            String[] ssss = t.getTag().split(",");
            holder.mTagGroup.setTags(ssss);
        }
        // 平米 楼层
        holder.des.setText(t.getSquare()+"㎡ |  "+t.getFloor()+"/"+t.getTotal_floor()+"    "+t.getHouse_num());


        //图片 tag
        holder.tagg.setText(t.getEntity_status());

        //到期时间
        holder.des2.setText(t.getEntity_finsh_time());


        return convertView;
    }

    static class ViewHolder {

        @ViewInject(R.id.imageView11)
        ImageView imageView11 ;

        @ViewInject(R.id.title)
        TextView title ;

        @ViewInject(R.id.tagg)
        TextView tagg;

        @ViewInject(R.id.des)
        TextView des ;

        @ViewInject(R.id.des2)
        TextView des2 ;

        @ViewInject(R.id.tttttt)
        TagContainerLayout mTagGroup;

        public ViewHolder(View itemView) {
            x.view().inject(this, itemView);
        }
    }
}
