package com.xinenhua.businessserver.house;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bm.library.PhotoView;
import com.githang.statusbar.StatusBarCompat;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.DialogUtils;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.ToastUtils;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.Holder;
import com.orhanobut.dialogplus.ViewHolder;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.entity.Yuyue;
import com.xinenhua.businessserver.http.BaseHttp;
import com.xinenhua.businessserver.personal.finance.Voucher;

import org.apache.commons.lang3.StringUtils;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * 个人中心 ---  我的签约  ---  实体租赁客户 ----------  签约详情 -------- 查看支付凭证 （确认已付款）
 */
@ContentView(R.layout.confirmpayfor)
public class ConfirmPayFor extends BaseFragmentActivity {

    protected ImageLoader imageLoader;


    @ViewInject(R.id.img)
    ImageView photoView;

    //账单id
    String zid ;

    //汇款方式
    @ViewInject(R.id.huikuanfangshi)
    TextView huikuanfangshi;

    //汇款金额
    @ViewInject(R.id.huikuanjine)
    TextView huikuanjine;

    @ViewInject(R.id.img)
    ImageView img;

    //确认按钮
    @ViewInject(R.id.confirm_button)
    Button confirm_button;

    @Override
    protected void start() {

        confirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirm();
            }
        });

        findViewById(R.id.warpper_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        zid = getIntent().getStringExtra("zid") ;

        imageLoader = ImageLoader.getInstance();

        actionBar.hide();
        StatusBarCompat.setStatusBarColor(ConfirmPayFor.this, getResources().getColor(R.color.app_green));
        super.start();

        //通过dialog 显示图片
        photoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Holder holder = new ViewHolder(R.layout.confirmpayfordialog);
                final DialogPlus dialog = DialogPlus.newDialog(ConfirmPayFor.this)
                        .setContentBackgroundResource(android.R.color.transparent)
                        .setGravity(Gravity.CENTER)
                        .setContentHolder(holder)
                        .create();

                PhotoView photoView = (PhotoView) dialog.getHolderView().findViewById(R.id.img);
                photoView.enable();

                ImageView back = (ImageView) dialog.getHolderView().findViewById(R.id.imageView14);
                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                if(!StringUtils.isBlank(voucher.getImages())){
                    ImageLoader.getInstance().displayImage(voucher.getImages(),photoView);

                }


                dialog.show();


            }
        });

        getData();

    }
    Dialog dialog ;
    Voucher voucher;

    //确认
    private void confirm(){

        dialog = DialogUtils.createLoadingDialog(context) ;
        dialog.show();


        HashMap<String,String> map = new HashMap<>() ;
        map.put("zhangdan_id",zid);
        map.put("admin_id",Const.getUser().getAdmin_id()+"");

        Call call = BaseHttp.getCallWithUrlAndData(Const.auditVoucher,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                showMsg("Fail");
                dialog.dismiss();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                LogUtil.e(TAG,"adminhousevoucher  "+rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    showMsg(BaseHttp.getJsonMsg(rr));
                    finish();
                }else {
                    showMsg(BaseHttp.getJsonMsg(rr));
                }

                dialog.dismiss();

            }
        });
    }

    /**
     * 查看支付凭证
     */
    private void getData(){

        dialog = DialogUtils.createLoadingDialog(context) ;
        dialog.show();


        HashMap<String,String> map = new HashMap<>() ;
        map.put("zhangdan_id",zid);//订单号

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminhousevoucher,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                showMsg("Fail");
                dialog.dismiss();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
               LogUtil.e(TAG,"adminhousevoucher  "+rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);

                    Gson g = new Gson();
                    voucher = g.fromJson(s, Voucher.class);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ImageLoader.getInstance().displayImage(voucher.getImages(),img);
                            huikuanfangshi.setText(voucher.getFukuanfangshi());
                            huikuanjine.setText(voucher.getHuikuanjine());

                        }
                    });

                }else {
                    showMsg(BaseHttp.getJsonMsg(rr));
                }

                dialog.dismiss();

            }
        });
    }


    private void queren(){
        AlertDialog.Builder tt = DialogUtils.getMessageDialog(context, "汇款信息已发送财务等待确认", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ToastUtils.shortToast(context, "11111111");
            }
        });

        tt.show();
    }
}
