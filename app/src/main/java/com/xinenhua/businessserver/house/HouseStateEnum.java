package com.xinenhua.businessserver.house;

import com.xinenhua.businessserver.login.PositionEnum;

/**
 * 实体房源列表  第二个筛选条件
 */
public enum HouseStateEnum {

    QUANBU("5","显示全部"),
    KONGZHI("0","空置"),
    YIZU("1","已租"),
    JIANGDAOQI("3","将到期");
    ;

    public String mCode ;
    public String mDes ;

    HouseStateEnum(String code , String des){
        mCode =code ;
        mDes =des;
    }

    public static HouseStateEnum getPosition(String status) {
        for (HouseStateEnum taskStatus : values()) {
            if (status.equals(taskStatus.mCode)) {
                return taskStatus;
            }
        }
        return  null;
    }

    @Override
    public String toString() {
        return mDes;
    }

}
