package com.xinenhua.businessserver.house;

import java.util.List;

/**
 * 房源详情 pojo
 */

public class HouseDetailPojo {
    private int id;

    private List<String> image ;

    private String title;

    private int rent;

    private String tag;

    private String create_time;

    private String build_name;

    private String address;

    private String house_num;

    private String kuandai;

    private String wuyeguanlifei;

    private String menjinyajin;

    private String fuhuafuwufei;

    private String shuidiankayajin;

    private String shenghuozilaishui;

    private String zhuceyajin;

    private String zhongshui;

    private String dianfei;

    private String gonggongwangluo;

    private String nengyuan;

    private String kaifashang;

    private String cainuanfei;

    private String wuyeguanligongsi;

    private String qitayajin;

    private String qita_1;

    private String qita_2;

    private String qita_3;

    private int day_rent;

    private int house_status;

    private String dingdanhao;

    private int wuye_status;

    private int hetong_id;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getImage() {
        return image;
    }

    public void setImage(List<String> image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getRent() {
        return rent;
    }

    public void setRent(int rent) {
        this.rent = rent;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getBuild_name() {
        return build_name;
    }

    public void setBuild_name(String build_name) {
        this.build_name = build_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHouse_num() {
        return house_num;
    }

    public void setHouse_num(String house_num) {
        this.house_num = house_num;
    }

    public String getKuandai() {
        return kuandai;
    }

    public void setKuandai(String kuandai) {
        this.kuandai = kuandai;
    }

    public String getWuyeguanlifei() {
        return wuyeguanlifei;
    }

    public void setWuyeguanlifei(String wuyeguanlifei) {
        this.wuyeguanlifei = wuyeguanlifei;
    }

    public String getMenjinyajin() {
        return menjinyajin;
    }

    public void setMenjinyajin(String menjinyajin) {
        this.menjinyajin = menjinyajin;
    }

    public String getFuhuafuwufei() {
        return fuhuafuwufei;
    }

    public void setFuhuafuwufei(String fuhuafuwufei) {
        this.fuhuafuwufei = fuhuafuwufei;
    }

    public String getShuidiankayajin() {
        return shuidiankayajin;
    }

    public void setShuidiankayajin(String shuidiankayajin) {
        this.shuidiankayajin = shuidiankayajin;
    }

    public String getShenghuozilaishui() {
        return shenghuozilaishui;
    }

    public void setShenghuozilaishui(String shenghuozilaishui) {
        this.shenghuozilaishui = shenghuozilaishui;
    }

    public String getZhuceyajin() {
        return zhuceyajin;
    }

    public void setZhuceyajin(String zhuceyajin) {
        this.zhuceyajin = zhuceyajin;
    }

    public String getZhongshui() {
        return zhongshui;
    }

    public void setZhongshui(String zhongshui) {
        this.zhongshui = zhongshui;
    }

    public String getDianfei() {
        return dianfei;
    }

    public void setDianfei(String dianfei) {
        this.dianfei = dianfei;
    }

    public String getGonggongwangluo() {
        return gonggongwangluo;
    }

    public void setGonggongwangluo(String gonggongwangluo) {
        this.gonggongwangluo = gonggongwangluo;
    }

    public String getNengyuan() {
        return nengyuan;
    }

    public void setNengyuan(String nengyuan) {
        this.nengyuan = nengyuan;
    }

    public String getKaifashang() {
        return kaifashang;
    }

    public void setKaifashang(String kaifashang) {
        this.kaifashang = kaifashang;
    }

    public String getCainuanfei() {
        return cainuanfei;
    }

    public void setCainuanfei(String cainuanfei) {
        this.cainuanfei = cainuanfei;
    }

    public String getWuyeguanligongsi() {
        return wuyeguanligongsi;
    }

    public void setWuyeguanligongsi(String wuyeguanligongsi) {
        this.wuyeguanligongsi = wuyeguanligongsi;
    }

    public String getQitayajin() {
        return qitayajin;
    }

    public void setQitayajin(String qitayajin) {
        this.qitayajin = qitayajin;
    }

    public String getQita_1() {
        return qita_1;
    }

    public void setQita_1(String qita_1) {
        this.qita_1 = qita_1;
    }

    public String getQita_2() {
        return qita_2;
    }

    public void setQita_2(String qita_2) {
        this.qita_2 = qita_2;
    }

    public String getQita_3() {
        return qita_3;
    }

    public void setQita_3(String qita_3) {
        this.qita_3 = qita_3;
    }

    public int getDay_rent() {
        return day_rent;
    }

    public void setDay_rent(int day_rent) {
        this.day_rent = day_rent;
    }

    public int getHouse_status() {
        return house_status;
    }

    public void setHouse_status(int house_status) {
        this.house_status = house_status;
    }

    public String getDingdanhao() {
        return dingdanhao;
    }

    public void setDingdanhao(String dingdanhao) {
        this.dingdanhao = dingdanhao;
    }

    public int getWuye_status() {
        return wuye_status;
    }

    public void setWuye_status(int wuye_status) {
        this.wuye_status = wuye_status;
    }

    public int getHetong_id() {
        return hetong_id;
    }

    public void setHetong_id(int hetong_id) {
        this.hetong_id = hetong_id;
    }
}
