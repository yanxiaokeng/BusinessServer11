package com.xinenhua.businessserver.house;

/**
 * 实体房源列表 pojo
 */

public class ListPojo {
    private int id;

    private String title;

    private String image;

    private String tag;

    private String square;

    private String floor;

    private String total_floor;

    private int build_id;

    private String build_name;

    private String entity_status;

    private String entity_finsh_time;

    private String house_num;

    public String getHouse_num() {
        return house_num;
    }

    public void setHouse_num(String house_num) {
        this.house_num = house_num;
    }

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setTitle(String title){
        this.title = title;
    }
    public String getTitle(){
        return this.title;
    }
    public void setImage(String image){
        this.image = image;
    }
    public String getImage(){
        return this.image;
    }
    public void setTag(String tag){
        this.tag = tag;
    }
    public String getTag(){
        return this.tag;
    }
    public void setSquare(String square){
        this.square = square;
    }
    public String getSquare(){
        return this.square;
    }
    public void setFloor(String floor){
        this.floor = floor;
    }
    public String getFloor(){
        return this.floor;
    }
    public void setTotal_floor(String total_floor){
        this.total_floor = total_floor;
    }
    public String getTotal_floor(){
        return this.total_floor;
    }
    public void setBuild_id(int build_id){
        this.build_id = build_id;
    }
    public int getBuild_id(){
        return this.build_id;
    }
    public void setBuild_name(String build_name){
        this.build_name = build_name;
    }
    public String getBuild_name(){
        return this.build_name;
    }

    public String getEntity_status() {
        return entity_status;
    }

    public void setEntity_status(String entity_status) {
        this.entity_status = entity_status;
    }

    public void setEntity_finsh_time(String entity_finsh_time){
        this.entity_finsh_time = entity_finsh_time;
    }
    public String getEntity_finsh_time(){
        return this.entity_finsh_time;
    }

    @Override
    public String toString() {
        return "ListPojo{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}
