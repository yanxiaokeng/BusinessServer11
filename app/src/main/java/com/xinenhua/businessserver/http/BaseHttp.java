package com.xinenhua.businessserver.http;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.oasis.oasislib.utils.ToastUtils;
import com.xinenhua.businessserver.Const;
import org.apache.commons.lang3.StringUtils;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * http请求基础工具
 */
public class BaseHttp {

    public static final int DATA_ERROR = 99;
    public static final int DATA_OK = 98;

    /**
     * http返回结果预处理
     * @param s
     * @return
     */
    public static int preResult(String s){
        JsonParser parser = new JsonParser();
        JsonObject asJsonObject = parser.parse(s).getAsJsonObject();
        int code = asJsonObject.get("code").getAsInt();
        if(code==0){
            return BaseHttp.DATA_OK;
        }else {
            return BaseHttp.DATA_ERROR;
        }
    }

    public static String getJsonMsg(String s){
        JsonParser parser = new JsonParser();
        JsonObject asJsonObject = parser.parse(s).getAsJsonObject();
        return asJsonObject.get("msg").getAsString();
    }

    public static String getJsonDataAsString(String s){
        JsonParser parser = new JsonParser();
        JsonObject asJsonObject = parser.parse(s).getAsJsonObject();
        return asJsonObject.get("data").toString();
    }




    public static String getJsonDataAsString(String s,String target){
        JsonParser parser = new JsonParser();
        JsonObject asJsonObject = parser.parse(s).getAsJsonObject();
        return asJsonObject.get(target).toString();
    }

    public static Integer getJsonDataAsInt(String s,String target){
        JsonParser parser = new JsonParser();
        JsonObject asJsonObject = parser.parse(s).getAsJsonObject();
        return asJsonObject.get(target).getAsInt();
    }

    /**
     * @param url 请求url地址
     * @param map 请求参数  如果没有参数  直接传入null
     * @return Call
     */
    public static Call getCallWithUrlAndData(String url, HashMap<String, String> map) {
        OkHttpClient client = BaseHttp.getHttpClient();
        FormBody formBody = BaseHttp.getFormBody(map);
        Request request = BaseHttp.getPostRequest(url, formBody);
        return client.newCall(request);
    }


    public static final int TIMEOUT_TIME_SECONDS = 6;

    public static Request getPostRequest(String url, FormBody formBody) {
        if (StringUtils.isBlank(url) || formBody == null) {
            return null;
        }

        return new Request.Builder().url(url)
                .post(formBody).build();
    }

    public static OkHttpClient getHttpClient() {
        return new OkHttpClient.Builder().readTimeout(BaseHttp.TIMEOUT_TIME_SECONDS, TimeUnit.SECONDS).build();
    }

    public static FormBody getFormBody(HashMap<String, String> map) {

        FormBody.Builder formBody = new FormBody.Builder()
                .add("access_token", Const.access_token());

        if (map != null && map.size() > 0) {
            Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, String> next = iterator.next();
                formBody.add(next.getKey(), next.getValue());
            }
        }

        return formBody.build();
    }

}
