package com.xinenhua.businessserver.contract;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.flyco.tablayout.SlidingTabLayout;
import com.githang.statusbar.StatusBarCompat;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.xinenhua.businessserver.R;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;

/**
 * 合同审核
 */
@ContentView(R.layout.bargaindetail)
public class BargainDetail extends BaseFragmentActivity {

    private ArrayList<BargainItemFragment> mFragments = new ArrayList<>();

    private final String[] mTitles = {
            "全部", "待审核", "已审核"
    };
    @ViewInject(R.id.tl_3)
    SlidingTabLayout tabLayout_3 ;

    @ViewInject(R.id.vp)
    ViewPager vp;

    private BargainDetail.MyPagerAdapter mAdapter;

    @Override
    protected void start() {
        super.start();
        actionBar.hide();
        StatusBarCompat.setStatusBarColor(BargainDetail.this, getResources().getColor(R.color.app_green));

        for (String title : mTitles) {
            mFragments.add(BargainItemFragment.getInstance(title,"1"));
        }

        mAdapter = new BargainDetail.MyPagerAdapter(getSupportFragmentManager());
        vp.setAdapter(mAdapter);
        vp.setOffscreenPageLimit(3);
        tabLayout_3.setViewPager(vp);
    }


    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mTitles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }


}
