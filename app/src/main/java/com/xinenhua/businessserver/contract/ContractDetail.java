package com.xinenhua.businessserver.contract;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bigkoo.convenientbanner.ConvenientBanner;
import com.bigkoo.convenientbanner.holder.CBViewHolderCreator;
import com.bigkoo.convenientbanner.holder.Holder;
import com.bigkoo.convenientbanner.listener.OnItemClickListener;
import com.githang.statusbar.StatusBarCompat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.DialogUtils;
import com.oasis.oasislib.utils.LogUtil;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.house.ConfirmPayFor;
import com.xinenhua.businessserver.house.HouseDetail;
import com.xinenhua.businessserver.http.BaseHttp;
import com.xinenhua.businessserver.personal.order.Jiaofeidan;
import com.xinenhua.businessserver.personal.order.OrderDetail;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;


/**
 *    个人中心----我的签约-----实体签约列表-------签约详情
 */
@ContentView(R.layout.contractdetail)
public class ContractDetail extends BaseFragmentActivity implements OnItemClickListener {

    //企业名称
    @ViewInject(R.id.qiyemingcheng)
    TextView qiyemingcheng;

    //房号信息
    @ViewInject(R.id.qiyemingcheng)
    TextView fanghao ;

    //企业联系方式
    @ViewInject(R.id.lianxifangshi)
    TextView lianxifangshi;

    //合同起止时间
    @ViewInject(R.id.hetongqizhi)
    TextView hetongqizhi;

    //待缴详情
    @ViewInject(R.id.xiangqing)
    TextView xiangqing;

    //包含list 的哪个ll
    @ViewInject(R.id.qi_linear)
    LinearLayout qi_linear;

    //右上角刷新图片
    @ViewInject(R.id.refreshimg)
    ImageView refreshimg;

    //订单号
    String dingdanhao ;


    @Override
    protected void start() {
        super.start();

        findViewById(R.id.warpper_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        dingdanhao = getIntent().getStringExtra("ding");

        actionBar.hide();
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.app_green));

        refreshimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
            }
        });

    }

    @Override
    public void onItemClick(int i) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        getData();
    }

    Dialog dialog;

    public void getData(){

        dialog = DialogUtils.createLoadingDialog(context);
        dialog.show();

        HashMap<String,String> map = new HashMap<>() ;
        map.put("dingdanhao",dingdanhao);

        Call call = BaseHttp.getCallWithUrlAndData(Const.ServicecontractDetails,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                showMsg("Fail");
                dialog.dismiss();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                LogUtil.e(TAG,"签约详情"+rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);

                    Gson gson = new Gson();
                    Type listType = new TypeToken<OrderDetail>(){}.getType();
                    final OrderDetail item = gson.fromJson(s, listType);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            qiyemingcheng.setText(item.getMingcheng());
                            fanghao.setText(item.getWeizhi());
                            lianxifangshi.setText(item.getPhone());
                            hetongqizhi.setText(item.getQizhi());
                            xiangqing.setText(item.getFukuanfangshi());


                            qi_linear.removeAllViews();
                            List<Jiaofeidan> list = item.getJiaofeidan();
                            if(list!=null&&list.size()>0){

                                for(int i=0;i<list.size();i++){
                                    View view = LayoutInflater.from(context).inflate(R.layout.contractdetail_item,null);
                                    TextView dijiqi = (TextView) view.findViewById(R.id.dijiqi);
                                    TextView time = (TextView) view.findViewById(R.id.time);
                                    TextView state = (TextView) view.findViewById(R.id.state);

                                    final Jiaofeidan ii = list.get(i);

                                    dijiqi.setText("第"+ii.getQishu()+"期");
                                    time.setText(ii.getBegin_time()+"----"+ii.getFinsh_time());

                                    //账单状态？？？？？
                                    /**
                                     *    3  查看凭证
                                     *
                                     *    2  待上传
                                     *
                                     *    1  已缴
                                     *
                                     *    4  待财务审核
                                     *
                                     *    其他 未缴
                                     */

                                    state.setClickable(false);

                                    if(ii.getZhangdan_status()==3){
                                        state.setClickable(true);
                                        state.setEnabled(true);
                                        state.setText("查看凭证");
                                        state.setBackgroundColor(Color.parseColor("#29cbb4"));

                                        state.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                Intent openWelcomeActivityIntent=new Intent();
                                                Bundle bundle=new Bundle();
                                                bundle.putString("zid",ii.getZhangdan_id()+"");
                                                openWelcomeActivityIntent.putExtras(bundle);
                                                openWelcomeActivityIntent.setClass(context, ConfirmPayFor.class);
                                                startActivity(openWelcomeActivityIntent);

                                            }
                                        });
                                    }else if(ii.getZhangdan_status()==2){
                                        state.setText("待上传");
                                        state.setBackgroundColor(Color.parseColor("#29cbb4"));
                                    }else if(ii.getZhangdan_status()==5){
                                        state.setText("已缴");
                                        state.setBackgroundColor(Color.parseColor("#cdcdcd"));
                                    }else if(ii.getZhangdan_status()==4){
                                        state.setText("待财务审核");
                                        state.setBackgroundColor(Color.parseColor("#29cbb4"));
                                    }else {
                                        state.setText("未缴");
                                        state.setBackgroundColor(Color.parseColor("#cdcdcd"));
                                    }

                                    qi_linear.addView(view);
                                }


                            }

                        }
                    });

                }else {
                    showMsg(rr);
                }
                dialog.dismiss();


            }
        });
    }


}
