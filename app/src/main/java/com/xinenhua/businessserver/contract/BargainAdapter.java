package com.xinenhua.businessserver.contract;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasis.oasislib.base.AppBaseAdapter;
import com.oasis.oasislib.utils.LogUtil;
import com.xinenhua.businessserver.R;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

/**
 * Created by oasis on 2019/3/11.
 */
public class BargainAdapter extends AppBaseAdapter<BargainItemPojo> {


    public BargainAdapter(Activity context) {
        super(context);
    }


    private Context context;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BargainAdapter.ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.bargain_item, parent, false);
            holder = new BargainAdapter.ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (BargainAdapter.ViewHolder) convertView.getTag();
        }
        final BargainItemPojo t = list.get(position) ;

        LogUtil.e("!!!!!!!!!",t.toString());

       /* ImageLoader.getInstance().displayImage(t.getImageurl(),holder.circleImageView);
        holder.begin.setText("开始时间："+t.getBegin());
        holder.createtime.setText(t.getCreate_time().split(" ")[0]);

        holder.end.setText("结束时间："+t.getFinsh());

        holder.who.setText(t.getMember_nickname()+"的请假");



        holder.type.setText("请假类型："+tt);
        //holder.status.setText(t.getStatus());
        holder.ss.setText(ss);


*/

       /* ImageLoader.getInstance().displayImage(t.getImage(),holder.imageView11);


        //holder.mTagGroup.setTags(new String[]{"Tag1", "Tag2", "Tag3"});
        holder.title.setText(t.getTitle());

        if(t.getTag()!=null&&!"".equals(t.getTag())){
            String[] ssss = t.getTag().split(",");
            holder.mTagGroup.setTags(ssss);
        }

        holder.des.setText(t.getSquare()+"㎡ |  "+t.getFloor()+"/"+t.getTotal_floor());


        String stString = "";

        int st =  t.getEntity_status();
        if(st==0){
            stString = "空置";
        }
        if(st==1){
            stString = "已租";
        }
        if(st==3){
            stString = "将到期";
        }


        holder.tagg.setText(stString);*/


        return convertView;
    }

    static class ViewHolder {


        @ViewInject(R.id.textView19)
        TextView user ;


        @ViewInject(R.id.textView23)
        TextView dataString ;



        public ViewHolder(View itemView) {
            x.view().inject(this, itemView);
        }
    }

}
