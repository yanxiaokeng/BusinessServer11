package com.xinenhua.businessserver.contract;

/**
 * Created by oasis on 2019/4/12.
 */

public class BargainItemPojo {
    private String name ;
    private String timeString ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimeString() {
        return timeString;
    }

    public void setTimeString(String timeString) {
        this.timeString = timeString;
    }
}
