package com.xinenhua.businessserver.contract;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.AdapterView;

import com.kennyc.view.MultiStateView;
import com.oasis.oasislib.base.BaseFragment;
import com.oasis.oasislib.view.XListView;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.login.pojo.User;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;

/**
 * Created by Administrator on 2018/7/12 0012.
 */
@ContentView(R.layout.list)
public class BargainItemFragment extends BaseFragment implements XListView.XListViewListener{

    @ViewInject(R.id.multiStateView)
    MultiStateView multiStateView ;

    @ViewInject(R.id.xlist_campaign)
    XListView xListView ;

     String mTitle;

    BargainAdapter adapter;

    ArrayList<BargainItemPojo> list = new ArrayList<>() ;

    public String mbuild_id;
    public String entity_status="";
    public String keyword = "";

    public String getKeyword() {
        return keyword;
    }

    private boolean hasMoreData = true ;
    private int pageIndex = 1 ;

    private String mmember_id="";

    User user;

    String member_id="";

    public void setMbuild_id(String mbuild_id) {
        this.mbuild_id = mbuild_id;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public static BargainItemFragment getInstance(String title, String build_id) {
        BargainItemFragment sf = new BargainItemFragment();
        sf.mTitle = title;
        sf.mbuild_id = build_id;
        return sf;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        for(int i=0;i<30;i++){
            list.add(new BargainItemPojo());
        }


       /* user = Const.getUser();
        if(user!=null){
            LogUtil.e(TAG,user.toString());
            member_id = user.getMember_id();
        }else {
            LogUtil.e(TAG,"user null");
        }

        if("全部".equals(mTitle)){
            entity_status = "";
        }
        if("空置".equals(mTitle)){
            entity_status = "0";
        }
        if("已租".equals(mTitle)){
            entity_status = "1";
        }
        if("将到期".equals(mTitle)){
            entity_status = "3";
        }*/

    }

    @Override
    protected void start() {


        multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        xListView.setPullLoadEnable(true);
        xListView.setPullRefreshEnable(true);
        xListView.setHeaderProgressDrawable(ContextCompat.getDrawable(getActivity(),R.mipmap.default_ptr_rotate));
        xListView.setRefreshTime();
        xListView.setPULL_LOAD_MORE_DELTA(60);
        xListView.setSCROLL_DURATION(400);
        xListView.setOFFSET_RADIO(3.0f);
        xListView.setXListViewListener(this);

        adapter = new BargainAdapter(getActivity()) ;
        adapter.setList(list);
        xListView.setAdapter(adapter);
        xListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /*Intent openWelcomeActivityIntent=new Intent();
                Bundle bundle=new Bundle();
                bundle.putString("id",String.valueOf(list.get(position-1).getApply_id()));

                openWelcomeActivityIntent.putExtras(bundle);
                openWelcomeActivityIntent.setClass(Shenpi.this, ShenpiDetail.class);
                startActivity(openWelcomeActivityIntent);*/

              /*  Intent intent = new Intent();
                intent.setClass(getActivity(), ConfirmPayFor.class);
                startActivity(intent);*/
            }
        });

        getData();


    }


    @Override
    public void onRefresh() {
        list.clear();
        pageIndex = 1 ;
        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        //adapter.notifyDataSetChanged();
        getData();
    }

    @Override
    public void onLoadMore() {
        if (hasMoreData) {
            getData() ;
        }else {
            xListView.hideFooter();
        }
    }


    private void getData() {

       /* LogUtil.e(TAG,"title--"+mTitle+"      entity_status=="+entity_status+"   keyword  "+keyword);

        RequestParams requestParams = new RequestParams(Const.houseentity) ;
        requestParams.setConnectTimeout(10*1000);
        requestParams.addBodyParameter("access_token", Const.access_token());
        requestParams.addBodyParameter("page",String.valueOf(pageIndex));
        requestParams.addBodyParameter("list_rows","10");
        requestParams.addBodyParameter("build_id",mbuild_id);
        requestParams.addBodyParameter("entity_status",entity_status);
        requestParams.addBodyParameter("keyword",keyword);
        requestParams.addBodyParameter("member_id",mmember_id);
        loadData(requestParams, new HttpCallBack() {
            @Override
            public void onSuccess(String result) {
                LogUtil.e(TAG,"onSuccess   "+Const.houseentity +"--result--"+result);
                HttpResultPojo httpResultPojo = HttpRequest.dealHttpResult(result) ;
                if(httpResultPojo.getCode()==0){
                    String data = httpResultPojo.getData() ;

                    try {
                        JSONObject jsonObject = new JSONObject(data) ;
                        String total = jsonObject.getString("total") ;
                        String per_page = jsonObject.getString("per_page") ;
                        String current_page = jsonObject.getString("current_page") ;
                        String last_page = jsonObject.getString("last_page") ;

                        JSONArray ja = jsonObject.getJSONArray("data") ;

                        Gson gson = new Gson();
                        List<ListPojo> listson = gson.fromJson(ja.toString(),new TypeToken<ArrayList<ListPojo>>(){}.getType());
                        list.addAll(listson);
                        if (Integer.parseInt(current_page)== 1) {
                            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
                        }
                        if (Integer.parseInt(total) == 0) {
                            multiStateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
                            //ToastUtils.shortToast(getActivity(),"无数据");
                        }
                        LogUtil.e(TAG,"total "+ total );

                        if(Integer.parseInt(total)<=list.size()){
                            hasMoreData = false ;
                        }else {
                            hasMoreData = true ;
                        }

                        if (hasMoreData) {
                            xListView.showFooter();
                        } else {
                            xListView.hideFooter();
                        }

                        xListView.setRefreshTime();
                        xListView.stopLoadMore();
                        xListView.stopRefresh();

                        pageIndex = Integer.parseInt(current_page)+1 ;

                        adapter.notifyDataSetChanged();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }else {
                    com.blankj.utilcode.util.ToastUtils.showLong(httpResultPojo.getMsg());
                }
            }

            @Override
            public void onFinished() {

            }

            @Override
            public void onCancelled(Callback.CancelledException cex) {

            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                ex.printStackTrace();
                LogUtil.e(TAG,ex.getMessage());
                multiStateView.setViewState(MultiStateView.VIEW_STATE_ERROR);
                multiStateView.getView(MultiStateView.VIEW_STATE_ERROR).findViewById(R.id.retry).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onRefresh();
                    }
                });
            }
        });*/

    }
}
