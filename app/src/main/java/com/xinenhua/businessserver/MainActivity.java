package com.xinenhua.businessserver;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.LogUtil;
import com.startsmake.mainnavigatetabbar.widget.MainNavigateTabBar;
import com.xinenhua.businessserver.login.pojo.User;
import com.xinenhua.businessserver.main.MainFragment;
import com.xinenhua.businessserver.personal.PersonalFragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;

import org.xutils.http.RequestParams;

/**
 * 项目主页面 包含两个fragment
 * */
public class MainActivity extends BaseFragmentActivity {

    MainNavigateTabBar navigateTabBar;

    private static boolean wantExit = false;
    private Handler handler = new Handler();

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //取得登录用户
        user = Const.getUser();
        if(user!=null){
            LogUtil.e(TAG,user.toString());
        }else {
            LogUtil.e(TAG,"user null");
        }

        //初始化页面
        navigateTabBar=(MainNavigateTabBar)findViewById(R.id.navigateTabBar);
        //对应xml中的containerId
        navigateTabBar.setFrameLayoutId(R.id.main_container);
        //对应xml中的navigateTabTextColor
        navigateTabBar.setTabTextColor(getResources().getColor(R.color.tab_text_normal));
        //对应xml中的navigateTabSelectedTextColor
        navigateTabBar.setSelectedTabTextColor(getResources().getColor(R.color.app_green));
        //恢复选项状态
        navigateTabBar.onRestoreInstanceState(savedInstanceState);
        navigateTabBar.addTab(MainFragment.class, new MainNavigateTabBar.TabParam(R.mipmap.homeoff, R.mipmap.homeon, "主页"));
//        navigateTabBar.addTab(OrderFragment.class, new MainNavigateTabBar.TabParam(R.mipmap.o_off, R.mipmap.o_on, "订单"));
        navigateTabBar.addTab(PersonalFragment.class, new MainNavigateTabBar.TabParam(R.mipmap.personoff, R.mipmap.personon, "我的"));


    }



    @Override
    protected void start() {
        super.start();
        mTitleBar.setVisibility(View.GONE);
        actionBar.hide();
    }
    //退出app的逻辑
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (!wantExit) {
                wantExit = true;
                showMessage("再按一次退出程序！");
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        wantExit = false;
                    }
                }, 2000);
            } else {
                Intent intent = new Intent();
                intent.setAction("android.action.exit");
                sendBroadcast(intent);
                finish();
            }
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }
}
