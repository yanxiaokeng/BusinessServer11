package com.xinenhua.businessserver.enumtype;

/**
 * 账单状态 1已生成待激活 2 已激活待上传缴费 3 待业务审核 4 待财务审核 5 完成
 */

public enum ZhangdanState {

    ALREADY_PRODUCE(1,"已生成待激活","已生成待激活"),
    WAIT_PAY(2,"已激活待上传缴费","已激活待上传缴费"),
    WAIT_SALEPERSON_CONFIRM(3,"待业务审核","待业务审核"),
    WAIT_FINANCE_CONFIRM(4,"待财务审核","待财务审核"),
    COMPLETED(5,"完成","完成");

    public final int mCode ;
    public String mDes ;
    public String mShowMsg;

    ZhangdanState(int code , String des,String showmsg){
        mCode =code ;
        mDes =des;
        mShowMsg = showmsg;
    }

    public static ZhangdanState getTaskStatus(int status) {
        for (ZhangdanState taskStatus : values()) {
            if (status == taskStatus.mCode) {
                return taskStatus;
            }
        }

        return  null;
    }





}
