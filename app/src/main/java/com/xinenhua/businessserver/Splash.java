package com.xinenhua.businessserver;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.xinenhua.businessserver.login.Login;
import com.xinenhua.businessserver.login.pojo.User;

import org.xutils.view.annotation.ContentView;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 * 启动页 就是一张图片  在xml里面定义
 * */
@ContentView(R.layout.splash)
public class Splash extends BaseFragmentActivity {
	
	private Handler mHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
				Intent intent = new Intent() ;
				intent.setClass(context, Login.class) ;
				startActivity(intent) ;
				finish() ; }
	} ;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		StatusBarCompat.setStatusBarColor(this, Color.parseColor("#FFFFFF"));
		actionBar.hide();
		mHandler.sendEmptyMessageDelayed(9998, 3000);

	}
}
