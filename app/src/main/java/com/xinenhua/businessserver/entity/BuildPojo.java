package com.xinenhua.businessserver.entity;

/**
 * Created by oasis on 2018/12/22.
 */

public class BuildPojo {
    private int id;

    private String name;

    private String address;

    private String create_time;

    private String update_time;

    private String status;

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    public void setAddress(String address){
        this.address = address;
    }
    public String getAddress(){
        return this.address;
    }
    public void setCreate_time(String create_time){
        this.create_time = create_time;
    }
    public String getCreate_time(){
        return this.create_time;
    }
    public void setUpdate_time(String update_time){
        this.update_time = update_time;
    }
    public String getUpdate_time(){
        return this.update_time;
    }
    public void setStatus(String status){
        this.status = status;
    }
    public String getStatus(){
        return this.status;
    }

}
