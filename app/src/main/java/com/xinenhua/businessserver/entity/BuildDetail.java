package com.xinenhua.businessserver.entity;

import com.githang.statusbar.StatusBarCompat;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.LogUtil;
import com.xinenhua.businessserver.R;

import org.xutils.view.annotation.ContentView;

import java.util.HashMap;

/**
 *  大厦详情 也是 楼盘详情  （开会说暂时不要了 。。20190514 ）
 */
@ContentView(R.layout.builddetail)
public class BuildDetail extends BaseFragmentActivity {

    private String buildingId ;

    @Override
    protected void start() {
        super.start();
        actionBar.hide();
        StatusBarCompat.setStatusBarColor(BuildDetail.this, getResources().getColor(R.color.app_green));

        buildingId = getIntent().getStringExtra("id");
        LogUtil.e(TAG,"-----buildingId----"+buildingId);
    }
}
