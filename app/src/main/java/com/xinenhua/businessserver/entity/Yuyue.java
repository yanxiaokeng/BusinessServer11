package com.xinenhua.businessserver.entity;

import com.githang.statusbar.StatusBarCompat;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.xinenhua.businessserver.R;

import org.xutils.view.annotation.ContentView;

/**
 * 这个页面是客户端预约的界面
 */
@ContentView(R.layout.yuyue)
public class Yuyue extends BaseFragmentActivity {

    @Override
    protected void start() {
        super.start();
        StatusBarCompat.setStatusBarColor(Yuyue.this, getResources().getColor(R.color.app_green));
        actionBar.hide();

    }
}
