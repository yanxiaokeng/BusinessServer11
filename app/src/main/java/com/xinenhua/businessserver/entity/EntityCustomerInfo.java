package com.xinenhua.businessserver.entity;

import android.app.Dialog;
import android.view.View;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.DialogUtils;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.ToastUtils;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.http.BaseHttp;
import com.xinenhua.businessserver.pojo.CustomerDetail;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * 房源详细---------》查看客户资料----------》客户资料详情
 */
@ContentView(R.layout.customerinfo_from_housedetail)
public class EntityCustomerInfo extends BaseFragmentActivity {

    String hetongid = "";

    Dialog loadingDialog;

    @ViewInject(R.id.xiaoshouren)
    TextView xiaoshouren;

    @ViewInject(R.id.zhucefanghao)
    TextView zhucefanghao;

    @ViewInject(R.id.shengxiaoriqi)
    TextView shengxiaoriqi;

    @ViewInject(R.id.fukuanfangshi)
    TextView fukuanfangshi;

    @ViewInject(R.id.danyuezujin)
    TextView danyuezujin;

    @ViewInject(R.id.yajinjine)
    TextView yajinjine;

    /*
    -------------

     */

    @ViewInject(R.id.qiyemingcheng)
    TextView qiyemingcheng;

    @ViewInject(R.id.lianxiren)
    TextView lianxiren;

    @ViewInject(R.id.lianxifangshi)
    TextView lianxifangshi;

    @ViewInject(R.id.kaishi)
    TextView kaishi;

    @ViewInject(R.id.jieshu)
    TextView jieshu;

    @Override
    protected void start() {
        super.start();
        StatusBarCompat.setStatusBarColor(EntityCustomerInfo.this, getResources().getColor(R.color.app_green));
        actionBar.hide();

        hetongid = getIntent().getStringExtra("id");
        LogUtil.e(TAG,"EntityCustomerInfo  hetongid " + hetongid);
        loadingDialog = DialogUtils.createLoadingDialog(context);
        loadingDialog.show();

        HashMap<String,String> map = new HashMap<>() ;
        map.put("hetong_id",hetongid);

        Call call = BaseHttp.getCallWithUrlAndData(Const.ServicememberDetail,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");
                loadingDialog.dismiss();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String rr = new String(response.body().string());
                LogUtil.e(TAG,rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);

                    Gson gson = new Gson();
                    Type listType = new TypeToken<CustomerDetail>(){}.getType();
                    final CustomerDetail customerDetail = gson.fromJson(s, listType);

                    LogUtil.e(TAG,"---"+customerDetail);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            xiaoshouren.setText(customerDetail.getNickname());
                            zhucefanghao.setText(customerDetail.getFanghao());
                            shengxiaoriqi.setText(customerDetail.getShengxiao_time());
                            fukuanfangshi.setText(customerDetail.getFangshi());
                            danyuezujin.setText(customerDetail.getZujin());
                            yajinjine.setText(customerDetail.getYajin());

                            qiyemingcheng.setText(customerDetail.getMingcheng());
                            lianxiren.setText(customerDetail.getLianxiren());
                            lianxifangshi.setText(customerDetail.getLianxifangshi());
                            kaishi.setText(customerDetail.getBegin_time());
                            jieshu.setText(customerDetail.getFinsh_time());

                        }
                    });
                    System.out.println("------------------------------------------");
                    loadingDialog.dismiss();
                }else {
                    //System.out.println(BaseHttp.getJsonMsg(rr));
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ToastUtils.shortToast(context,BaseHttp.getJsonMsg(rr));
                            finish();
                        }
                    });
                    loadingDialog.dismiss();
                }

            }
        });

        findViewById(R.id.warpper_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }



    private void getData(String hetongid){
        HashMap<String,String> map = new HashMap<>() ;
        map.put("hetong_id",hetongid);
        //loadingDialog.show();
        Call call = BaseHttp.getCallWithUrlAndData(Const.ServicememberDetail,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");
                //loadingDialog.dismiss();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);

                    Gson gson = new Gson();
                    Type listType = new TypeToken<CustomerDetail>(){}.getType();
                    final CustomerDetail customerDetail = gson.fromJson(s, listType);

                    LogUtil.e(TAG,"---"+customerDetail);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            xiaoshouren.setText(customerDetail.getNickname());
                            zhucefanghao.setText(customerDetail.getFanghao());
                            shengxiaoriqi.setText(customerDetail.getShengxiao_time());
                            fukuanfangshi.setText(customerDetail.getFangshi());
                            danyuezujin.setText(customerDetail.getZujin());
                            yajinjine.setText(customerDetail.getYajin());

                            qiyemingcheng.setText(customerDetail.getMingcheng());
                            lianxiren.setText(customerDetail.getLianxiren());
                            lianxifangshi.setText(customerDetail.getLianxifangshi());
                            kaishi.setText(customerDetail.getBegin_time());
                            jieshu.setText(customerDetail.getFinsh_time());

                        }
                    });
                    System.out.println("------------------------------------------");
                    //loadingDialog.dismiss();
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }

}
