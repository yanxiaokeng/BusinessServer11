package com.xinenhua.businessserver.entity;

/**
 * 预约列表 的pojo
 */

public class YuyueListItemPojo {

    private int id;

    private String title;

    private String image;

    private String tag;

    private String square;

    private String floor;

    private String total_floor;

    private String entity_status;

    private String fictitious_status;

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setTitle(String title){
        this.title = title;
    }
    public String getTitle(){
        return this.title;
    }
    public void setImage(String image){
        this.image = image;
    }
    public String getImage(){
        return this.image;
    }
    public void setTag(String tag){
        this.tag = tag;
    }
    public String getTag(){
        return this.tag;
    }
    public void setSquare(String square){
        this.square = square;
    }
    public String getSquare(){
        return this.square;
    }
    public void setFloor(String floor){
        this.floor = floor;
    }
    public String getFloor(){
        return this.floor;
    }
    public void setTotal_floor(String total_floor){
        this.total_floor = total_floor;
    }
    public String getTotal_floor(){
        return this.total_floor;
    }
    public void setEntity_status(String entity_status){
        this.entity_status = entity_status;
    }
    public String getEntity_status(){
        return this.entity_status;
    }
    public void setFictitious_status(String fictitious_status){
        this.fictitious_status = fictitious_status;
    }
    public String getFictitious_status(){
        return this.fictitious_status;
    }
}
