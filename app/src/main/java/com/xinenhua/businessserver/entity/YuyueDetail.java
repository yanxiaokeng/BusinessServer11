package com.xinenhua.businessserver.entity;

/**
 * 服务端预约信息详情
 */

public class YuyueDetail {

    private int house_id;

    private int yuyue_id;

    private String title;

    private String image;

    private String tag;

    private String square;

    private String floor;

    private String total_floor;

    private String entity_status;

    private String fictitious_status;

    private String name;

    private int sex;

    private String phone;

    private String ps;

    private int yuyue_status;

    private int house_status;

    private String dingdanhao;

    private int wuye_status;

    private int hetong_id;

    public void setHouse_id(int house_id){
        this.house_id = house_id;
    }
    public int getHouse_id(){
        return this.house_id;
    }
    public void setYuyue_id(int yuyue_id){
        this.yuyue_id = yuyue_id;
    }
    public int getYuyue_id(){
        return this.yuyue_id;
    }
    public void setTitle(String title){
        this.title = title;
    }
    public String getTitle(){
        return this.title;
    }
    public void setImage(String image){
        this.image = image;
    }
    public String getImage(){
        return this.image;
    }
    public void setTag(String tag){
        this.tag = tag;
    }
    public String getTag(){
        return this.tag;
    }
    public void setSquare(String square){
        this.square = square;
    }
    public String getSquare(){
        return this.square;
    }
    public void setFloor(String floor){
        this.floor = floor;
    }
    public String getFloor(){
        return this.floor;
    }
    public void setTotal_floor(String total_floor){
        this.total_floor = total_floor;
    }
    public String getTotal_floor(){
        return this.total_floor;
    }
    public void setEntity_status(String entity_status){
        this.entity_status = entity_status;
    }
    public String getEntity_status(){
        return this.entity_status;
    }
    public void setFictitious_status(String fictitious_status){
        this.fictitious_status = fictitious_status;
    }
    public String getFictitious_status(){
        return this.fictitious_status;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    public void setSex(int sex){
        this.sex = sex;
    }
    public int getSex(){
        return this.sex;
    }
    public void setPhone(String phone){
        this.phone = phone;
    }
    public String getPhone(){
        return this.phone;
    }
    public void setPs(String ps){
        this.ps = ps;
    }
    public String getPs(){
        return this.ps;
    }
    public void setYuyue_status(int yuyue_status){
        this.yuyue_status = yuyue_status;
    }
    public int getYuyue_status(){
        return this.yuyue_status;
    }
    public void setHouse_status(int house_status){
        this.house_status = house_status;
    }
    public int getHouse_status(){
        return this.house_status;
    }
    public void setDingdanhao(String dingdanhao){
        this.dingdanhao = dingdanhao;
    }
    public String getDingdanhao(){
        return this.dingdanhao;
    }
    public void setWuye_status(int wuye_status){
        this.wuye_status = wuye_status;
    }
    public int getWuye_status(){
        return this.wuye_status;
    }
    public void setHetong_id(int hetong_id){
        this.hetong_id = hetong_id;
    }
    public int getHetong_id(){
        return this.hetong_id;
    }


    @Override
    public String toString() {
        return "YuyueDetail{" +
                "house_id=" + house_id +
                ", yuyue_id=" + yuyue_id +
                ", title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", tag='" + tag + '\'' +
                ", square='" + square + '\'' +
                ", floor='" + floor + '\'' +
                ", total_floor='" + total_floor + '\'' +
                ", entity_status='" + entity_status + '\'' +
                ", fictitious_status='" + fictitious_status + '\'' +
                ", name='" + name + '\'' +
                ", sex=" + sex +
                ", phone='" + phone + '\'' +
                ", ps='" + ps + '\'' +
                ", yuyue_status=" + yuyue_status +
                ", house_status=" + house_status +
                ", dingdanhao='" + dingdanhao + '\'' +
                ", wuye_status=" + wuye_status +
                ", hetong_id=" + hetong_id +
                '}';
    }
}
