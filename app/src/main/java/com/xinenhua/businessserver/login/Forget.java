package com.xinenhua.businessserver.login;

import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.ActivityUtils;
import com.githang.statusbar.StatusBarCompat;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.ToastUtils;
import com.xinenhua.businessserver.R;
import com.xw.repo.XEditText;

import org.apache.commons.lang3.StringUtils;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;

/**
 * 忘记密码
 */

@ContentView(R.layout.forget)
public class Forget extends BaseFragmentActivity {


    @ViewInject(R.id.imageButton)
    RelativeLayout next;

    @ViewInject(R.id.XEditText)
    XEditText phonenum;

    @Override
    protected void start() {
        actionBar.hide();
        StatusBarCompat.setStatusBarColor(this, Color.WHITE);
        super.start();

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String content = phonenum.getText().toString();
                if(StringUtils.isBlank(content)){
                    ToastUtils.shortToast(context,"请输入电话号码");
                    return;
                }

                Intent intent = new Intent(context,ForgetTwo.class) ;
                intent.putExtra("phone",content);
                ActivityUtils.startActivity(intent);
            }
        });
    }


    @Event(value = R.id.imageView8, type = View.OnClickListener.class)
    private void finish(View view) {
       finish();
    }



}
