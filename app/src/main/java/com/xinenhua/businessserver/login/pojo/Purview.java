package com.xinenhua.businessserver.login.pojo;

/**
 * Created by oasis on 2019/5/8.
 */

public class Purview {

    private String querenhetong;

    private String gerenziliao;

    private String kehuziliao;

    private String daijiaozhangdan;

    private String kehuyuyue;

    private String wodeqianyue;

    private String zhuceyonghu;

    private String daozhangqueren;



    public String getGerenziliao() {
        return gerenziliao;
    }

    public void setGerenziliao(String gerenziliao) {
        this.gerenziliao = gerenziliao;
    }

    public String getKehuziliao() {
        return kehuziliao;
    }

    public void setKehuziliao(String kehuziliao) {
        this.kehuziliao = kehuziliao;
    }

    public String getDaijiaozhangdan() {
        return daijiaozhangdan;
    }

    public void setDaijiaozhangdan(String daijiaozhangdan) {
        this.daijiaozhangdan = daijiaozhangdan;
    }

    public String getKehuyuyue() {
        return kehuyuyue;
    }

    public void setKehuyuyue(String kehuyuyue) {
        this.kehuyuyue = kehuyuyue;
    }

    public String getWodeqianyue() {
        return wodeqianyue;
    }

    public void setWodeqianyue(String wodeqianyue) {
        this.wodeqianyue = wodeqianyue;
    }

    public String getZhuceyonghu() {
        return zhuceyonghu;
    }

    public void setZhuceyonghu(String zhuceyonghu) {
        this.zhuceyonghu = zhuceyonghu;
    }

    public String getDaozhangqueren() {
        return daozhangqueren;
    }

    public void setDaozhangqueren(String daozhangqueren) {
        this.daozhangqueren = daozhangqueren;
    }

    public String getQuerenhetong() {
        return querenhetong;
    }

    public void setQuerenhetong(String querenhetong) {
        this.querenhetong = querenhetong;
    }


    @Override
    public String toString() {
        return "Purview{" +
                "querenhetong='" + querenhetong + '\'' +
                ", gerenziliao='" + gerenziliao + '\'' +
                ", kehuziliao='" + kehuziliao + '\'' +
                ", daijiaozhangdan='" + daijiaozhangdan + '\'' +
                ", kehuyuyue='" + kehuyuyue + '\'' +
                ", wodeqianyue='" + wodeqianyue + '\'' +
                ", zhuceyonghu='" + zhuceyonghu + '\'' +
                ", daozhangqueren='" + daozhangqueren + '\'' +
                '}';
    }
}
