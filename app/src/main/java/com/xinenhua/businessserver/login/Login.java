package com.xinenhua.businessserver.login;

import android.app.Dialog;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.SPUtils;
import com.githang.statusbar.StatusBarCompat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.base.HttpCallBack;
import com.oasis.oasislib.utils.DialogUtils;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.ToastUtils;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnClickListener;
import com.orhanobut.dialogplus.ViewHolder;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.MainActivity;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.http.BaseHttp;
import com.xinenhua.businessserver.login.pojo.User;
import com.xw.repo.XEditText;


import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 登录
 */
@ContentView(R.layout.login)
public class Login extends BaseFragmentActivity {


    @ViewInject(R.id.imageButton)
    RelativeLayout imageButton;

    @ViewInject(R.id.textView)
    TextView registerTextview ;

    @ViewInject(R.id.textVi2ew2)
    TextView forgetTextview ;

    Dialog dialog ;
    //电话
    String phone ;
    //密码
    String psw ;

    @ViewInject(R.id.editText)
    XEditText editText;

    @ViewInject(R.id.editText1)
    XEditText pswEdittext ;

    @Override
    protected void start() {
        super.start();
        StatusBarCompat.setStatusBarColor(this, Color.BLACK);
        mTitleBar.setBackViewVisiable(false);
        actionBar.hide();

        //从sp缓存中拿用户信息啊
        SPUtils spUtils = SPUtils.getInstance();
        String phone = spUtils.getString("phone","") ;
        String psw = spUtils.getString("psw","") ;

        if(!StringUtils.isBlank(phone)&&!StringUtils.isBlank(psw)){
            editText.setText(phone);
            pswEdittext.setText(psw);
        }

        //登录
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    login();
            }
        });


        //跳到登陆页面
        registerTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(Register.class);
            }
        });

        //跳到忘记密码页面
        forgetTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(Forget.class);
            }
        });
    }

    /**
     * 执行登录操作
     */
    private void login(){

        phone = editText.getText().toString();
        if(StringUtils.isBlank(phone)){
            ToastUtils.shortToast(context,"请输入电话号码");
            return;
        }

        psw = pswEdittext.getText().toString();
        if(StringUtils.isBlank(psw)){
            ToastUtils.shortToast(context,"请输入密码");
            return;
        }

        dialog = DialogUtils.createLoadingDialog(context);
        dialog.show();

        HashMap<String,String> map = new HashMap<>() ;
        map.put("phone",phone);
        map.put("password",psw);

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminlogin,map);
        call.enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                LogUtil.e(TAG,"Fail");
                dialog.cancel();

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String rr = new String(response.body().string());
                LogUtil.e(TAG,"login onResponse  "+rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    //登录成功
                    String s = BaseHttp.getJsonDataAsString(rr);
                    Gson gson = new Gson();
                    Type listType = new TypeToken<User>(){}.getType();
                    //得到了用户信息
                    User user = gson.fromJson(s, listType);

                    //存储用户信息
                    SPUtils spUtils = SPUtils.getInstance();
                    spUtils.put("user",s);

                    LogUtil.e(TAG,"phone"+phone);
                    LogUtil.e(TAG,"psw"+psw);

                    spUtils.put("phone",phone);
                    spUtils.put("psw",psw);

                    //跳到首页
                    ActivityUtils.startActivity(MainActivity.class);

                    System.out.println(s);
                    showMsg("登录成功");

                }else {
                    LogUtil.e(TAG,BaseHttp.getJsonMsg(rr));
                    showMsg(BaseHttp.getJsonMsg(rr));
                    dialog.cancel();
                }

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    //显示登陆错误提示！！！！！！！！！！
    private void showmsg(){
        DialogPlus dialog = DialogPlus.newDialog(context)
                .setContentHolder(new ViewHolder(R.layout.wai1_dialog))
                .setGravity(Gravity.CENTER)
                .setContentBackgroundResource(android.R.color.transparent)
                .setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(DialogPlus dialog, View view) {
                        dialog.dismiss();
                    }
                })
                .setExpanded(true)
                .create();

        dialog.show();
    }
}
