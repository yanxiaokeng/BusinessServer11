package com.xinenhua.businessserver.login;

import com.xinenhua.businessserver.enumtype.ZhangdanState;

/**
 * 职位枚举
 */
public enum PositionEnum {

    BOSS(1,"董事长"),
    MANAGER(2,"项目经理"),
    FINANCE(3,"财务"),
    ASSISTANT(4,"项目助理");

    public final int mCode ;
    public String mDes ;

    PositionEnum(int code , String des){
        mCode =code ;
        mDes =des;
    }

    public static PositionEnum getPosition(int status) {
        for (PositionEnum taskStatus : values()) {
            if (status == taskStatus.mCode) {
                return taskStatus;
            }
        }
        return  null;
    }

    @Override
    public String toString() {
        return mDes;
    }
}
