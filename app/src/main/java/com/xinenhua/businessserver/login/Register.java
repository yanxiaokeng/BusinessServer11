package com.xinenhua.businessserver.login;

import android.content.Intent;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ActivityUtils;
import com.githang.statusbar.StatusBarCompat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.CountDownUtil;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.ToastUtils;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.http.BaseHttp;
import com.xinenhua.businessserver.personal.PersonInfo;
import com.xinenhua.businessserver.pojo.PositionPojo;
import com.xinenhua.businessserver.pojo.ProjectPojo;
import com.xw.repo.XEditText;

import org.apache.commons.lang3.StringUtils;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

import static com.blankj.utilcode.util.LogUtils.I;

/**
 * 注册页面
 */
@ContentView(R.layout.register)
public class Register extends BaseFragmentActivity {

    @ViewInject(R.id.textView7)
    TextView textView7;

    //下一步按钮
    @ViewInject(R.id.imageButton1)
    RelativeLayout next ;

    @ViewInject(R.id.phonenum)
    XEditText phonenum;

    @ViewInject(R.id.zhiweipo)
    RelativeLayout zhiwei;

    @ViewInject(R.id.depart)
    RelativeLayout depart;

    //职位列表 （非网络请求）
    PositionEnum[] array = {PositionEnum.BOSS,PositionEnum.MANAGER,PositionEnum.FINANCE,PositionEnum.ASSISTANT};

    //显示职位
    @ViewInject(R.id.zhiweiText)
    TextView zhiweiText;

    //显示项目
    @ViewInject(R.id.project_text)
    TextView projectText ;

    //职位
    List<PositionPojo> listPosition = null;
    PositionPojo currentPosition = null;

    //项目列表 （从网络获取）
    List<ProjectPojo> listProject = null;
    ProjectPojo currentProject = null;

    @Override
    protected void start() {
        super.start();
        mTitleBar.setCenterTitle("注册");
        mTitleBar.setBackViewVisiable(true);
        mTitleBar.setVisibility(View.GONE);

        //获得项目信息
        getProject();
        //获得职位信息
        getPosition();

        StatusBarCompat.setStatusBarColor(this, Color.WHITE);
        actionBar.hide();

        //点击下一步
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(currentPosition==null){
                    showMsg("请选择职位");
                    return;
                }

                if(currentProject==null){
                    showMsg("请选择项目");
                    return;
                }

                String content = phonenum.getText().toString();

                if(StringUtils.isBlank(content)){
                    showMsg("请输入电话号码");
                    return;
                }

                if(content.length()!=11){
                    showMsg("请输入合法的电话号码");
                    return;
                }



                Intent intent = new Intent(context,RegisterTwo.class) ;
                intent.putExtra("phone",content);
                Gson gson = new Gson();
                intent.putExtra("positionINT",currentPosition.getId()) ;
                intent.putExtra("projectString",gson.toJson(currentProject)) ;
                ActivityUtils.startActivity(intent);
            }
        });

        //选择职位
        zhiwei.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogPlus dialog = DialogPlus.newDialog(Register.this)
                        .setAdapter(new ArrayAdapter<PositionPojo>(Register.this, android.R.layout.simple_list_item_1, new ArrayList<PositionPojo>(listPosition)))
                        .setContentBackgroundResource(android.R.color.white)
                        .setGravity(Gravity.CENTER)
                        .setOnItemClickListener(new OnItemClickListener() {
                            @Override
                            public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                                dialog.dismiss();
                                //ToastUtils.shortToast(context,array[position].mDes);
                                currentPosition = listPosition.get(position);
                                zhiweiText.setText(currentPosition.getName());
                            }
                        })
                        .create();

                dialog.show();
            }
        });

        //选择项目
        depart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogPlus dialog = DialogPlus.newDialog(Register.this)
                        .setAdapter(new ArrayAdapter<ProjectPojo>(Register.this, android.R.layout.simple_list_item_1, new ArrayList<ProjectPojo>(listProject)))
                        .setContentBackgroundResource(android.R.color.white)
                        .setGravity(Gravity.CENTER)
                        .setOnItemClickListener(new OnItemClickListener() {
                            @Override
                            public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                                dialog.dismiss();
                                //ToastUtils.shortToast(context,array[position].mDes);
                                currentProject = listProject.get(position);
                                projectText.setText(currentProject.getName());

                            }
                        })
                        .create();

                dialog.show();
            }
        });

    }

    @Event(value = R.id.textView7, type = View.OnClickListener.class)
    private void getyanzheng(View view) {
        change_sign_button_state();
    }


    private void change_sign_button_state(){
        new CountDownUtil(textView7)
                .setCountDownMillis(60_000L)//倒计时60000ms
                .setCountDownColor(R.color.app_gloden,android.R.color.darker_gray)//不同状态字体颜色
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getyanzheng(textView7);
                    }
                })
                .start();
    }


    @Event(value = R.id.imageView8, type = View.OnClickListener.class)
    private void finish(View view) {
        finish();
    }


    //网络获取职位信息
    private void getPosition(){
        Call call = BaseHttp.getCallWithUrlAndData(Const.adminposition,null);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                showMsg("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                LogUtil.e(TAG,"adminproject"+ rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<PositionPojo>>(){}.getType();
                    List<PositionPojo> projectList = gson.fromJson(s, listType);
                    listPosition = projectList;

                }else {
                    showMsg(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }


    //获得项目列表
    private void getProject(){

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminproject,null);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                LogUtil.e(TAG,"adminproject"+ rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<ProjectPojo>>(){}.getType();
                    List<ProjectPojo> projectList = gson.fromJson(s, listType);
                    listProject = projectList;
                    System.out.println(Arrays.toString(projectList.toArray()));
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });

    }
}
