package com.xinenhua.businessserver.login;

import android.view.View;
import android.widget.TextView;

import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.CountDownUtil;
import com.xinenhua.businessserver.R;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;

/**
 * 老版本的找回密码 。。。。。  现在废弃了。。。。。。。
 */
@ContentView(R.layout.find)
public class Find extends BaseFragmentActivity {

    @ViewInject(R.id.textView7)
    TextView textView7;

    @Override
    protected void start() {
        super.start();
        mTitleBar.setCenterTitle("找回密码");
        mTitleBar.setBackViewVisiable(false);
    }


    @Event(value = R.id.textView7, type = View.OnClickListener.class)
    private void getyanzheng(View view) {
        change_sign_button_state();
    }

    private void change_sign_button_state(){
        new CountDownUtil(textView7)
                .setCountDownMillis(60_000L)//倒计时60000ms
                .setCountDownColor(R.color.app_gloden,android.R.color.darker_gray)//不同状态字体颜色
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getyanzheng(textView7);
                    }
                })
                .start();
    }


}
