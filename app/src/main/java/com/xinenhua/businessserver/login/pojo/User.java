package com.xinenhua.businessserver.login.pojo;

public class User {
    private int admin_id;

    private String phone;

    private int is_check;

    private String create_time;

    private String nickname;

    private String position;

    private String project_id;

    private String project;

    private Purview purview;

    private String imageurl;

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public void setAdmin_id(int admin_id){
        this.admin_id = admin_id;
    }
    public int getAdmin_id(){
        return this.admin_id;
    }
    public void setPhone(String phone){
        this.phone = phone;
    }
    public String getPhone(){
        return this.phone;
    }
    public void setIs_check(int is_check){
        this.is_check = is_check;
    }
    public int getIs_check(){
        return this.is_check;
    }
    public void setCreate_time(String create_time){
        this.create_time = create_time;
    }
    public String getCreate_time(){
        return this.create_time;
    }
    public void setNickname(String nickname){
        this.nickname = nickname;
    }
    public String getNickname(){
        return this.nickname;
    }
    public void setPosition(String position){
        this.position = position;
    }
    public String getPosition(){
        return this.position;
    }
    public void setProject_id(String project_id){
        this.project_id = project_id;
    }
    public String getProject_id(){
        return this.project_id;
    }
    public void setProject(String project){
        this.project = project;
    }
    public String getProject(){
        return this.project;
    }
    public void setPurview(Purview purview){
        this.purview = purview;
    }
    public Purview getPurview(){
        return this.purview;
    }


    @Override
    public String toString() {
        return "User{" +
                "admin_id=" + admin_id +
                ", phone='" + phone + '\'' +
                ", is_check=" + is_check +
                ", create_time='" + create_time + '\'' +
                ", nickname='" + nickname + '\'' +
                ", position='" + position + '\'' +
                ", project_id='" + project_id + '\'' +
                ", project='" + project + '\'' +
                ", purview=" + purview +
                '}';
    }
}
