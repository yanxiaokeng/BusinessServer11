package com.xinenhua.businessserver.fictitious;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.kennyc.view.MultiStateView;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.ToastUtils;
import com.oasis.oasislib.view.XListView;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.house.EntityHouseList;
import com.xinenhua.businessserver.house.ListAdapter;
import com.xinenhua.businessserver.house.ListPojo;

import org.apache.commons.lang3.StringUtils;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;

/**
 * 虚拟房源列表
 */
@ContentView(R.layout.houseficlist)
public class FictitiousHouseList extends BaseFragmentActivity implements XListView.XListViewListener{

    //筛选下拉
    @ViewInject(R.id.spinner)
    MaterialSpinner spinner;

    //筛选下拉
    @ViewInject(R.id.spinner1)
    MaterialSpinner spinner1;

    @ViewInject(R.id.multiStateView)
    MultiStateView multiStateView ;

    @ViewInject(R.id.xlist_campaign)
    XListView xListView ;

    ArrayList list = new ArrayList<>() ;
    private boolean hasMoreData = true ;
    private int pageIndex = 1 ;

    FicListAdapter adapter;

    @ViewInject(R.id.te3xtView5)
    EditText search ;

    @Override
    protected void start() {
        super.start();
        StatusBarCompat.setStatusBarColor(FictitiousHouseList.this, getResources().getColor(R.color.app_green));
        actionBar.hide();

        spinner.setItems("Ice Cream", "Jelly Bean", "KitKat", "Lollipop", "Marshmallow");
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                ToastUtils.shortToast(context,item);
            }
        });

        spinner1.setItems("Ice Cream", "Jelly Bean", "KitKat", "Lollipop", "Marshmallow");
        spinner1.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                ToastUtils.shortToast(context,item);
            }
        });


        for(int i = 0 ;i<20;i++){
            list.add(new ListPojo());
        }

        multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        xListView.setPullLoadEnable(true);
        xListView.setPullRefreshEnable(true);
        xListView.setHeaderProgressDrawable(ContextCompat.getDrawable(FictitiousHouseList.this,R.mipmap.default_ptr_rotate));
        xListView.setRefreshTime();
        xListView.setPULL_LOAD_MORE_DELTA(60);
        xListView.setSCROLL_DURATION(400);
        xListView.setOFFSET_RADIO(3.0f);
        xListView.setXListViewListener(this);

        adapter = new FicListAdapter(FictitiousHouseList.this) ;
        adapter.setList(list);
        xListView.setAdapter(adapter);
        xListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /*Intent openWelcomeActivityIntent=new Intent();
                Bundle bundle=new Bundle();
                bundle.putString("id",String.valueOf(list.get(position-1).getApply_id()));

                openWelcomeActivityIntent.putExtras(bundle);
                openWelcomeActivityIntent.setClass(Shenpi.this, ShenpiDetail.class);
                startActivity(openWelcomeActivityIntent);*/


            }
        });

        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_SEARCH){

                    String key = v.getText().toString();
                    LogUtil.e(TAG,key);
                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);


                    if(StringUtils.isBlank(key)){

                    }else {
                        ToastUtils.shortToast(context,key);
                    }




                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public void onRefresh() {
        list.clear();
        pageIndex = 1 ;
        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        //adapter.notifyDataSetChanged();
        getData();
    }

    @Override
    public void onLoadMore() {
        if (hasMoreData) {
            getData() ;
        }else {
            xListView.hideFooter();
        }
    }

    private void getData() {

    }
}
