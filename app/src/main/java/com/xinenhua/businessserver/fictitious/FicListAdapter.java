package com.xinenhua.businessserver.fictitious;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.oasis.oasislib.base.AppBaseAdapter;
import com.oasis.oasislib.utils.LogUtil;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.house.ListPojo;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import co.lujun.androidtagview.TagContainerLayout;

/**
 * 虚拟房源列表适配器
 */

public class FicListAdapter extends AppBaseAdapter<ListPojo> {

    public FicListAdapter(Activity context) {
        super(context);
    }


    private Context context;


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.houseficlistitem, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final ListPojo t = list.get(position) ;

        LogUtil.e("!!!!!!!!!",t.toString());

       /* ImageLoader.getInstance().displayImage(t.getImageurl(),holder.circleImageView);
        holder.begin.setText("开始时间："+t.getBegin());
        holder.createtime.setText(t.getCreate_time().split(" ")[0]);

        holder.end.setText("结束时间："+t.getFinsh());

        holder.who.setText(t.getMember_nickname()+"的请假");



        holder.type.setText("请假类型："+tt);
        //holder.status.setText(t.getStatus());
        holder.ss.setText(ss);


*/

        //ImageLoader.getInstance().displayImage(t.getImage(),holder.imageView11);
        ImageLoader.getInstance().displayImage("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1533566443368&di=b056ce1f74ee9a2d4fd910866c21de39&imgtype=0&src=http://p5.pccoo.cn/news/20160720/2016072010271681587483.png",holder.imageView11);


        //holder.title.setText(t.getTitle());
        holder.title.setText("创智东园");
        if(t.getTag()!=null&&!"".equals(t.getTag())){
            String[] ssss = t.getTag().split(",");
            holder.mTagGroup.setTags(ssss);
        }

        holder.des.setText(t.getSquare()+"㎡ |  "+t.getFloor()+"/"+t.getTotal_floor());
        holder.mTagGroup.setTags(new String[]{"Tag1", "Tag2", "Tag3"});

        String stString = "";

        holder.tagg.setText(t.getEntity_status());


        return convertView;
    }

    static class ViewHolder {

        @ViewInject(R.id.imageView11)
        ImageView imageView11 ;

        @ViewInject(R.id.title)
        TextView title ;

        @ViewInject(R.id.tagg)
        TextView tagg;

        @ViewInject(R.id.des)
        TextView des ;

        @ViewInject(R.id.des2)
        TextView des2 ;

        @ViewInject(R.id.tttttt)
        TagContainerLayout mTagGroup;

        public ViewHolder(View itemView) {
            x.view().inject(this, itemView);
        }
    }
}
