package com.xinenhua.businessserver.main;

/**
 * 首页轮播图
 */

public class BannerItem {

    private String  title ;
    private String imageurl ;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    @Override
    public String toString() {
        return imageurl;
    }
}
