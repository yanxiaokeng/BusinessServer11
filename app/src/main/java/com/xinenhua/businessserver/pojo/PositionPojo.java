package com.xinenhua.businessserver.pojo;

/**
 *注册--职位
 */

public class PositionPojo {
    private String id ;
    private String name ;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
