package com.xinenhua.businessserver.pojo;

/**
 * {"code":0,"msg":"操作成功","data":[{"id":1,"name":"晶采"},{"id":2,"name":"青林"},{"id":3,"name":"鼎力"},{"id":4,"name":"创智"},{"id":7,"name":"成功"}],"exe_time":"0.018001"}
 */
public class ProjectPojo {
    private String id ;
    private String name ;
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

  /*  @Override
    public String toString() {
        return "ProjectPojo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }*/

    @Override
    public String toString() {
        return name;
    }
}
