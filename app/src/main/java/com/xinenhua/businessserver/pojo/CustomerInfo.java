package com.xinenhua.businessserver.pojo;

/**
 * 实体/虚拟所有客户资料信息 列表 pojo
 */

public class CustomerInfo {
    private int htong_id;

    private String dingdanhao;

    private String yf_chengzufang;

    private String yf_jinjilianxiren;

    private String yf_lianxifangshi;

    private int house_id;

    public void setHtong_id(int htong_id){
        this.htong_id = htong_id;
    }
    public int getHtong_id(){
        return this.htong_id;
    }
    public void setDingdanhao(String dingdanhao){
        this.dingdanhao = dingdanhao;
    }
    public String getDingdanhao(){
        return this.dingdanhao;
    }
    public void setYf_chengzufang(String yf_chengzufang){
        this.yf_chengzufang = yf_chengzufang;
    }
    public String getYf_chengzufang(){
        return this.yf_chengzufang;
    }
    public void setYf_jinjilianxiren(String yf_jinjilianxiren){
        this.yf_jinjilianxiren = yf_jinjilianxiren;
    }
    public String getYf_jinjilianxiren(){
        return this.yf_jinjilianxiren;
    }
    public void setYf_lianxifangshi(String yf_lianxifangshi){
        this.yf_lianxifangshi = yf_lianxifangshi;
    }
    public String getYf_lianxifangshi(){
        return this.yf_lianxifangshi;
    }
    public void setHouse_id(int house_id){
        this.house_id = house_id;
    }
    public int getHouse_id(){
        return this.house_id;
    }


    @Override
    public String toString() {
        return "CustomerInfo{" +
                "htong_id=" + htong_id +
                ", dingdanhao='" + dingdanhao + '\'' +
                ", yf_chengzufang='" + yf_chengzufang + '\'' +
                ", yf_jinjilianxiren='" + yf_jinjilianxiren + '\'' +
                ", yf_lianxifangshi='" + yf_lianxifangshi + '\'' +
                ", house_id=" + house_id +
                '}';
    }
}
