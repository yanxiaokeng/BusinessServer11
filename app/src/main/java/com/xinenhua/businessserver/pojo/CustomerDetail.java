package com.xinenhua.businessserver.pojo;

/**
 * Created by oasis on 2019/5/10.
 */

public class CustomerDetail {
    private int hetong_id;

    private String nickname;

    private String fanghao;

    private String shengxiao_time;

    private String fangshi;

    private String zujin;

    private String yajin;

    private String mingcheng;

    private String lianxiren;

    private String lianxifangshi;

    private String begin_time;

    private String finsh_time;

    public void setHetong_id(int hetong_id){
        this.hetong_id = hetong_id;
    }
    public int getHetong_id(){
        return this.hetong_id;
    }
    public void setNickname(String nickname){
        this.nickname = nickname;
    }
    public String getNickname(){
        return this.nickname;
    }
    public void setFanghao(String fanghao){
        this.fanghao = fanghao;
    }
    public String getFanghao(){
        return this.fanghao;
    }
    public void setShengxiao_time(String shengxiao_time){
        this.shengxiao_time = shengxiao_time;
    }
    public String getShengxiao_time(){
        return this.shengxiao_time;
    }
    public void setFangshi(String fangshi){
        this.fangshi = fangshi;
    }
    public String getFangshi(){
        return this.fangshi;
    }
    public void setZujin(String zujin){
        this.zujin = zujin;
    }
    public String getZujin(){
        return this.zujin;
    }
    public void setYajin(String yajin){
        this.yajin = yajin;
    }
    public String getYajin(){
        return this.yajin;
    }
    public void setMingcheng(String mingcheng){
        this.mingcheng = mingcheng;
    }
    public String getMingcheng(){
        return this.mingcheng;
    }
    public void setLianxiren(String lianxiren){
        this.lianxiren = lianxiren;
    }
    public String getLianxiren(){
        return this.lianxiren;
    }
    public void setLianxifangshi(String lianxifangshi){
        this.lianxifangshi = lianxifangshi;
    }
    public String getLianxifangshi(){
        return this.lianxifangshi;
    }
    public void setBegin_time(String begin_time){
        this.begin_time = begin_time;
    }
    public String getBegin_time(){
        return this.begin_time;
    }
    public void setFinsh_time(String finsh_time){
        this.finsh_time = finsh_time;
    }
    public String getFinsh_time(){
        return this.finsh_time;
    }

    @Override
    public String toString() {
        return "CustomerDetail{" +
                "hetong_id=" + hetong_id +
                ", nickname='" + nickname + '\'' +
                ", fanghao='" + fanghao + '\'' +
                ", shengxiao_time='" + shengxiao_time + '\'' +
                ", fangshi='" + fangshi + '\'' +
                ", zujin='" + zujin + '\'' +
                ", yajin='" + yajin + '\'' +
                ", mingcheng='" + mingcheng + '\'' +
                ", lianxiren='" + lianxiren + '\'' +
                ", lianxifangshi='" + lianxifangshi + '\'' +
                ", begin_time='" + begin_time + '\'' +
                ", finsh_time='" + finsh_time + '\'' +
                '}';
    }
}
