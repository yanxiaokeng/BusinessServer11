package com.xinenhua.businessserver.hetong;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.google.gson.Gson;
import com.kennyc.view.MultiStateView;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.ToastUtils;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.login.pojo.User;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

/**
 * 添加合同的测试类  不是生产环境的   生产环境的生成合同 是 HeTong.class
 */
@ContentView(R.layout.webview)
public class Add extends BaseFragmentActivity {

    @ViewInject(R.id.webView)
    BridgeWebView mWebView ;

    @ViewInject(R.id.multiStateView)
    MultiStateView mMultiStateView;

    String token = "" ;
    String userid = "" ;

    User user ;

    @Override
    protected void start() {
        super.start();

        token= Const.access_token();
        user = Const.getUser();
        userid=String.valueOf(user.getAdmin_id());


        mWebView.getSettings().setJavaScriptEnabled(true);
        String url = "http://x.oliwen.cn/#/hetong?token="+token+"&userid="+userid;
        mWebView.loadUrl(url);


        WebSettings webSettings = mWebView.getSettings();
        // 使能JavaScript
        webSettings.setJavaScriptEnabled(true);


        //让网页自适应设备大小
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);

        mWebView.setWebChromeClient(new WebChromeClient(){
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                LogUtil.e(TAG,consoleMessage.message() + " -- From line "
                        + consoleMessage.lineNumber() + " of "
                        + consoleMessage.sourceId() );
                return super.onConsoleMessage(consoleMessage);
            }
        });

        mWebView.setWebViewClient(new WebViewClient(){


            @Override
            public void onPageFinished(WebView view, String url) {
                // TODO Auto-generated method stub
                super.onPageFinished(view, url);
                mMultiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);


                /*testButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mWebView.loadUrl("javascript:success('安卓调用js')");
                        //ToastUtils.shortToast(context,"1111");
                    }
                });*/

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // TODO Auto-generated method stub
                super.onPageStarted(view, url, favicon);
                mMultiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
            }

        });

        mWebView.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && mWebView.canGoBack()) {  //表示按返回键时的操作
                        mWebView.goBack();   //后退
                        return true;    //已处理
                    }
                }
                return false;
            }
        });



        mWebView.addJavascriptInterface(new Add.JsJavaBridge(this, mWebView), "$App");



    }

    private class ParamsData{
        private String userid;
        private String token ;

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }


    public class JsJavaBridge {

        private Activity activity;
        private BridgeWebView webView;

        public JsJavaBridge(Activity activity, BridgeWebView webView) {
            this.activity = activity;
            this.webView = webView;
        }

        @JavascriptInterface
        public void onFinishActivity() {
            activity.finish();
        }

        @JavascriptInterface
        public void showToast(String msg) {
            ToastUtils.shortToast(activity,msg);
        }

        @JavascriptInterface
        public String getJson() {
            ParamsData pd = new ParamsData();
            pd.setToken(token);
            pd.setUserid(userid);
            Gson gson = new Gson();
            return gson.toJson(pd) ;
        }


    }



}
