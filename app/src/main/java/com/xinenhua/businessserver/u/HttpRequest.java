package com.xinenhua.businessserver.u;


import com.oasis.oasislib.base.HttpCallBack;
import com.oasis.oasislib.utils.LogUtil;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.HttpMethod;
import org.xutils.http.RequestParams;
import org.xutils.x;

/**
 * http请求的工具类
 */
public class HttpRequest {

    public static HttpResultPojo dealHttpResult(String s){
        HttpResultPojo result = null;
        //{"code":1010006,"msg":"手机号码格式错误","exe_time":"0.010000"}
        //{"code":0,"msg":"操作成功","data":{"sign":"7967"},"exe_time":"0.536030"}
        LogUtil.e("HttpResultPojo","result   "+s);
//        Gson gson = new Gson();
//
//        Type type = new TypeToken<HttpResultPojo>() {}.getType();
        //result = (HttpResultPojo)gson.fromJson(s.substring(1,s.length()-1),type);
        //result = (HttpResultPojo)gson.fromJson(s,type);

        result = new HttpResultPojo();

        try {
            JSONObject jsonObject = new JSONObject(s) ;
            result.setCode(jsonObject.getInt("code"));
            result.setMsg(jsonObject.getString("msg"));
            result.setExe_time(jsonObject.getString("exe_time"));
            result.setData(jsonObject.getString("data"));

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return result ;

    }

    public void loadData(RequestParams requestParams , final HttpCallBack callBack){

        x.http().request(HttpMethod.POST, requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                callBack.onSuccess(result);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                callBack.onError(ex,isOnCallback);
            }

            @Override
            public void onCancelled(CancelledException cex) {
                callBack.onCancelled(cex);
            }

            @Override
            public void onFinished() {
                callBack.onFinished();
            }
        }) ;


    }

    /**
     * xutils同步方法只能在子线程中进行！！！！！！！！！！！！
     * @param requestParams
     */
    public String net_getSync(RequestParams requestParams) throws Throwable {
        String response  = x.http().getSync(requestParams,String.class) ;
        return response;
    }

    public String net_postSync(RequestParams requestParams) throws Throwable{
        String response = x.http().postSync(requestParams,String.class) ;
        return response;
    }

    public void loadData_getSync(final RequestParams requestParams , final HttpCallBack callBack){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String result  = net_getSync(requestParams) ;
                    callBack.onSuccess(result);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    callBack.onError(throwable,false);
                }

                callBack.onFinished();

            }
        }).start();
    }

    public void loadData_postSync(final RequestParams requestParams , final HttpCallBack callBack){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String result  = net_postSync(requestParams) ;
                    callBack.onSuccess(result);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    callBack.onError(throwable,false);
                }

                callBack.onFinished();

            }
        }).start();
    }



}
