package com.xinenhua.businessserver.bill;

/**
 * 到账确认列表
 */

public class AccountListPojo {
    private int id;

    private String jihuo_time;

    private String username;

    private String dingdanhao;

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setJihuo_time(String jihuo_time){
        this.jihuo_time = jihuo_time;
    }
    public String getJihuo_time(){
        return this.jihuo_time;
    }
    public void setUsername(String username){
        this.username = username;
    }
    public String getUsername(){
        return this.username;
    }
    public void setDingdanhao(String dingdanhao){
        this.dingdanhao = dingdanhao;
    }
    public String getDingdanhao(){
        return this.dingdanhao;
    }

}
