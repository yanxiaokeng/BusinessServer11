package com.xinenhua.businessserver.bill;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasis.oasislib.base.AppBaseAdapter;
import com.oasis.oasislib.utils.LogUtil;
import com.xinenhua.businessserver.R;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

/**
 * 個人中心 缴费账单  实体缴费账单 列表适配器* */
public class PayBillAdapter extends AppBaseAdapter<EntityPayBillPojo> {

    public PayBillAdapter(Activity context) {
        super(context);
    }
    private Context context;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PayBillAdapter.ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.paybillitem, parent, false);
            holder = new PayBillAdapter.ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (PayBillAdapter.ViewHolder) convertView.getTag();
        }
        final EntityPayBillPojo t = list.get(position) ;

        LogUtil.e("!!!!!!!!!",t.toString());

//        holder.name.setText("天津市河西区陈塘科技区内江路创智东园2号楼");
//        holder.date.setText("创智东园");

        holder.name.setText(t.getCustom_name());
        holder.housenum.setText(t.getHouse_num());
        holder.num.setText(t.getZhangdan_status());


        return convertView;
    }

    static class ViewHolder {

        @ViewInject(R.id.name)
        TextView name ;

        @ViewInject(R.id.housenum)
        TextView housenum;

        @ViewInject(R.id.num)
        TextView num;

        public ViewHolder(View itemView) {
            x.view().inject(this, itemView);
        }
    }

}
