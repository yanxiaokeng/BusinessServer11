package com.xinenhua.businessserver.bill;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kennyc.view.MultiStateView;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.ToastUtils;
import com.oasis.oasislib.view.XListView;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.http.BaseHttp;

import org.apache.commons.lang3.StringUtils;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * 个人中心-----缴费账单---虚拟待缴
 */
@ContentView(R.layout.ficpaybill)
public class FicPayBill extends BaseFragmentActivity implements XListView.XListViewListener{

    @ViewInject(R.id.multiStateView)
    MultiStateView multiStateView ;

    @ViewInject(R.id.xlist_campaign)
    XListView xListView ;

    ArrayList<EntityPayBillPojo> list = new ArrayList<>() ;
    private boolean hasMoreData = true ;
    private int pageIndex = 1 ;

    @ViewInject(R.id.te3xtView5)
    EditText searchEditText;

    PayBillAdapter adapter;

    String keyword="";

    @Override
    protected void start() {

        StatusBarCompat.setStatusBarColor(FicPayBill.this, getResources().getColor(R.color.app_green));
        actionBar.hide();

        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);

        xListView.setPullLoadEnable(true);
        xListView.setPullRefreshEnable(true);
        xListView.setHeaderProgressDrawable(ContextCompat.getDrawable(context,R.mipmap.default_ptr_rotate));
        xListView.setRefreshTime();
        xListView.setPULL_LOAD_MORE_DELTA(60);
        xListView.setSCROLL_DURATION(400);
        xListView.setOFFSET_RADIO(3.0f);
        xListView.setXListViewListener(this);

        adapter = new PayBillAdapter(FicPayBill.this) ;
        adapter.setList(list);
        xListView.setAdapter(adapter);
        xListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent openWelcomeActivityIntent=new Intent();
                Bundle bundle=new Bundle();
                bundle.putString("dingdanhao",String.valueOf(list.get(position-1).getDingdanhao()));

                openWelcomeActivityIntent.putExtras(bundle);
                openWelcomeActivityIntent.setClass(context, PayBillDetail.class);
                startActivity(openWelcomeActivityIntent);
            }
        });

        getData();

        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    String searchkey = searchEditText.getText().toString();
                    if(!StringUtils.isBlank(searchkey)){
                        keyword = searchkey;
                        onRefresh();
                    }
                    return true;
                }
                return false;
            }
        });

        findViewById(R.id.warpper_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public void onRefresh() {
        list.clear();
        pageIndex = 1 ;
        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        //adapter.notifyDataSetChanged();
        getData();
    }

    @Override
    public void onLoadMore() {
        if (hasMoreData) {
            getData() ;
        }else {
            xListView.hideFooter();
        }
    }

    private void getData() {

        HashMap<String,String> map = new HashMap<>() ;
        map.put("page",String.valueOf(pageIndex));
        map.put("list_rows", Const.list_rows+"");
        map.put("type","1"); //（1:虚拟代缴账单 2：实体代缴账单）
        map.put("key",keyword);


        LogUtil.e(TAG,"search---page--"+String.valueOf(pageIndex));
        LogUtil.e(TAG,"search---list_rows--"+Const.list_rows+"");
        LogUtil.e(TAG,"search---keyword--"+keyword);


        Call call = BaseHttp.getCallWithUrlAndData(Const.ServicepayBill,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        multiStateView.setViewState(MultiStateView.VIEW_STATE_ERROR);
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);

                    final int total = BaseHttp.getJsonDataAsInt(s,"total") ;
                    int per_page = BaseHttp.getJsonDataAsInt(s,"per_page") ;
                    final int current_page = BaseHttp.getJsonDataAsInt(s,"current_page") ;
                    int last_page = BaseHttp.getJsonDataAsInt(s,"last_page") ;

                    String son = BaseHttp.getJsonDataAsString(s);

                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<EntityPayBillPojo>>(){}.getType();
                    List<EntityPayBillPojo> projectList = gson.fromJson(son, listType);

                    list.addAll(projectList);


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(total==0){
                                multiStateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
                            }else {
                                adapter.notifyDataSetChanged();
                                multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);

                                xListView.setRefreshTime();
                                xListView.stopLoadMore();
                                xListView.stopRefresh();

                                if(list.size()<total){
                                    hasMoreData = true ;
                                    pageIndex = current_page+1 ;
                                }else {
                                    hasMoreData = false ;
                                }

                                if (hasMoreData) {
                                    xListView.showFooter();
                                } else {
                                    xListView.hideFooter();
                                }
                            }
                        }
                    });
                }else {
                    showMsg(BaseHttp.getJsonMsg(rr));
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                            multiStateView.setViewState(MultiStateView.VIEW_STATE_ERROR);

                            xListView.setRefreshTime();
                            xListView.stopLoadMore();
                            xListView.stopRefresh();
                        }
                    });
                }

            }
        });


    }
}
