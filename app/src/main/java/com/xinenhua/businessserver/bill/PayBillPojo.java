package com.xinenhua.businessserver.bill;

/**
 * 实体 虚拟 缴费账单 列表pojo的基类
 */

public class PayBillPojo {
    private String custom_name;

    private String house_num;

    private String dingdanhao;

    private String zhangdan_status;

    public void setCustom_name(String custom_name){
        this.custom_name = custom_name;
    }
    public String getCustom_name(){
        return this.custom_name;
    }
    public void setHouse_num(String house_num){
        this.house_num = house_num;
    }
    public String getHouse_num(){
        return this.house_num;
    }
    public void setDingdanhao(String dingdanhao){
        this.dingdanhao = dingdanhao;
    }
    public String getDingdanhao(){
        return this.dingdanhao;
    }
    public void setZhangdan_status(String zhangdan_status){
        this.zhangdan_status = zhangdan_status;
    }
    public String getZhangdan_status(){
        return this.zhangdan_status;
    }


}
