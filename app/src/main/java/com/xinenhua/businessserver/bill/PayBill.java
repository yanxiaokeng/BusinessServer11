package com.xinenhua.businessserver.bill;

import android.view.View;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.ActivityUtils;
import com.githang.statusbar.StatusBarCompat;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.xinenhua.businessserver.R;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

/**
 * 个人中心-------缴费账单
 */
@ContentView(R.layout.paybill)
public class PayBill extends BaseFragmentActivity {
    @ViewInject(R.id.fic)
    RelativeLayout fic;

    @ViewInject(R.id.entity)
    RelativeLayout entity;

    @Override
    protected void start() {
        super.start();
        StatusBarCompat.setStatusBarColor(PayBill.this, getResources().getColor(R.color.app_green));
        actionBar.hide();

        //去虚拟缴费账单列表
        fic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(PayBill.this,FicPayBill.class);
            }
        });
        //去实体缴费账单列表
        entity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(PayBill.this,EntityPayBill.class);
            }
        });

        findViewById(R.id.warpper_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
