package com.xinenhua.businessserver.bill;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kennyc.view.MultiStateView;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.ToastUtils;
import com.oasis.oasislib.view.XListView;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.house.ListPojo;
import com.xinenhua.businessserver.http.BaseHttp;

import org.apache.commons.lang3.StringUtils;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * 个人中心----缴费账单----实体  （列表）
 *
 * listview  例子 样例  注释
 *
 * */
@ContentView(R.layout.entitypaybill)
public class EntityPayBill extends BaseFragmentActivity implements XListView.XListViewListener{

    //状态view
    @ViewInject(R.id.multiStateView)
    MultiStateView multiStateView ;

    @ViewInject(R.id.xlist_campaign)
    XListView xListView ;
    //实体类
    ArrayList<EntityPayBillPojo> list = new ArrayList<>() ;
    //是否还有更多的数据
    private boolean hasMoreData = true ;
    //当前第几页
    private int pageIndex = 1 ;

    @ViewInject(R.id.te3xtView5)
    EditText searchEditText;

    //数据适配器
    PayBillAdapter adapter;
    String keyword="";

    @Override
    protected void start() {

        StatusBarCompat.setStatusBarColor(EntityPayBill.this, getResources().getColor(R.color.app_green));
        actionBar.hide();

        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);

        //xListView初始化配置
        xListView.setPullLoadEnable(true);
        xListView.setPullRefreshEnable(true);
        xListView.setHeaderProgressDrawable(ContextCompat.getDrawable(context,R.mipmap.default_ptr_rotate));
        xListView.setRefreshTime();
        xListView.setPULL_LOAD_MORE_DELTA(60);
        xListView.setSCROLL_DURATION(400);
        xListView.setOFFSET_RADIO(3.0f);
        xListView.setXListViewListener(this);

        adapter = new PayBillAdapter(EntityPayBill.this) ;
        adapter.setList(list);
        xListView.setAdapter(adapter);
        xListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //跳转缴费详情
                Intent openWelcomeActivityIntent=new Intent();
                Bundle bundle=new Bundle();
                bundle.putString("dingdanhao",String.valueOf(list.get(position-1).getDingdanhao()));

                openWelcomeActivityIntent.putExtras(bundle);
                openWelcomeActivityIntent.setClass(context, PayBillDetail.class);
                startActivity(openWelcomeActivityIntent);
            }
        });

        getData();

        //搜索输入监听
        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    String searchkey = searchEditText.getText().toString();
                    if(!StringUtils.isBlank(searchkey)){
                        keyword = searchkey;
                        onRefresh();
                    }
                    return true;
                }
                return false;
            }
        });

        //关闭页面
        findViewById(R.id.warpper_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    //刷新
    @Override
    public void onRefresh() {
        list.clear();
        pageIndex = 1 ;
        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        //adapter.notifyDataSetChanged();
        getData();
    }
    //加载更多
    @Override
    public void onLoadMore() {
        if (hasMoreData) {
            getData() ;
        }else {
            xListView.hideFooter();
        }
    }

    //加载数据
    private void getData() {

        HashMap<String,String> map = new HashMap<>() ;
        map.put("page",String.valueOf(pageIndex));
        map.put("list_rows", Const.list_rows+"");
        map.put("type","2"); //（1:虚拟代缴账单 2：实体代缴账单）
        map.put("key",keyword);


        LogUtil.e(TAG,"search---page--"+String.valueOf(pageIndex));
        LogUtil.e(TAG,"search---list_rows--"+Const.list_rows+"");
        LogUtil.e(TAG,"search---keyword--"+keyword);


        Call call = BaseHttp.getCallWithUrlAndData(Const.ServicepayBill,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //发生错误
                        multiStateView.setViewState(MultiStateView.VIEW_STATE_ERROR);
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                //http数据返回正确
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);

                    final int total = BaseHttp.getJsonDataAsInt(s,"total") ;
                    int per_page = BaseHttp.getJsonDataAsInt(s,"per_page") ;
                    final int current_page = BaseHttp.getJsonDataAsInt(s,"current_page") ;
                    int last_page = BaseHttp.getJsonDataAsInt(s,"last_page") ;

                    String son = BaseHttp.getJsonDataAsString(s);
                    // json ------>  object
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<EntityPayBillPojo>>(){}.getType();
                    List<EntityPayBillPojo> projectList = gson.fromJson(son, listType);

                    list.addAll(projectList);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(total==0){
                                //数据为0  显示空状态
                                multiStateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
                            }else {
                                //数据的数目不为0  显示内容
                                adapter.notifyDataSetChanged();
                                multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
                                //刷新状态恢复
                                xListView.setRefreshTime();
                                xListView.stopLoadMore();
                                xListView.stopRefresh();

                                //当前列表数目 小于 总的记录数  证明还有数据  hasMoreData = true ;
                                if(list.size()<total){
                                    hasMoreData = true ;
                                    pageIndex = current_page+1 ;
                                }else {
                                    hasMoreData = false ;
                                }
                                //没有数据了 隐藏footer
                                if (hasMoreData) {
                                    xListView.showFooter();
                                } else {
                                    xListView.hideFooter();
                                }
                            }
                        }
                    });
                }else {//http返回的数据有问题
                    //显示服务器返回的错误
                    showMsg(BaseHttp.getJsonMsg(rr));
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            adapter.notifyDataSetChanged();
                            multiStateView.setViewState(MultiStateView.VIEW_STATE_ERROR);

                            xListView.setRefreshTime();
                            xListView.stopLoadMore();
                            xListView.stopRefresh();
                        }
                    });
                }

            }
        });


    }
}
