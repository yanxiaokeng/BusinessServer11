package com.xinenhua.businessserver.bill;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.kennyc.view.MultiStateView;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.DialogUtils;
import com.oasis.oasislib.utils.LogUtil;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.http.BaseHttp;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * 个人中心 待缴账单 实体缴费账单 缴费账单列表 缴费详情
 */
@ContentView(R.layout.paybilldetail)
public class PayBillDetail extends BaseFragmentActivity {

    String dingdanhao ;

    @ViewInject(R.id.linear)
    LinearLayout linear;

    @Override
    protected void start() {
        super.start();

        StatusBarCompat.setStatusBarColor(PayBillDetail.this, getResources().getColor(R.color.app_green));
        actionBar.hide();

        dingdanhao = getIntent().getStringExtra("dingdanhao");
        LogUtil.e(TAG,"dingdanhao----"+dingdanhao);

        findViewById(R.id.warpper_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //获取数据
        getData();

    }

    Dialog dialog;

    private void getData() {

        dialog = DialogUtils.createLoadingDialog(context) ;
        dialog.show();

        HashMap<String,String> map = new HashMap<>() ;
        map.put("dingdanhao",dingdanhao);

        LogUtil.e(TAG,"search---dingdanhao--"+dingdanhao);


        Call call = BaseHttp.getCallWithUrlAndData(Const.ServicepayDetails,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LogUtil.e(TAG,"Fail");
                showMsg("fail");
                dialog.dismiss();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    LogUtil.e(TAG,"s"+s);
                    JsonParser parser = new JsonParser();
                    final JsonArray jsonArray = parser.parse(s).getAsJsonArray();

                    //可能会有好几期 所以for循环添加数据
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            for(int i=0 ;i<jsonArray.size();i++){
                                JsonObject jsonObject = (JsonObject) jsonArray.get(i);
                                String qishu = jsonObject.get("qishu").getAsString();
                                String begin_time = jsonObject.get("begin_time").getAsString();
                                String finsh_time = jsonObject.get("finsh_time").getAsString();
                                String fukuanfangshi = jsonObject.get("fukuanfangshi").getAsString();
                                String is_jiaona = jsonObject.get("is_jiaona").getAsString();

                                LogUtil.e(TAG,"qishu--"+qishu);

                                View view = LayoutInflater.from(context).inflate(R.layout.paybilldetail_item,null) ;
                                TextView qishut = (TextView) view.findViewById(R.id.qishu);
                                TextView begint = (TextView) view.findViewById(R.id.begin);
                                TextView endt = (TextView) view.findViewById(R.id.end);
                                TextView jiaonat = (TextView) view.findViewById(R.id.jiaona);
                                TextView fukuanfangshit = (TextView) view.findViewById(R.id.fukuanfangshi);


                                begint.setText(begin_time);
                                qishut.setText(qishu);
                                endt.setText(finsh_time);
                                jiaonat.setText(is_jiaona);
                                fukuanfangshit.setText(fukuanfangshi);


                                linear.addView(view);
                            }
                        }
                    });

                }else {
                    showMsg(BaseHttp.getJsonMsg(rr));
                }
                dialog.dismiss();
            }
        });


    }





}
