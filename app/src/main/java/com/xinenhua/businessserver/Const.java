package com.xinenhua.businessserver;

import com.blankj.utilcode.util.SPUtils;
import com.google.gson.Gson;
import com.xinenhua.businessserver.entity.BuildPojo;
import com.xinenhua.businessserver.login.pojo.User;

import org.apache.commons.lang3.StringUtils;
import org.xutils.common.util.MD5;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * 各种配置项  url等
 */
public class Const {
    static String ip = "https://api.xinenhua.com";
    // static String ip = "http://192.168.0.66";
    static String part = "/api" ;


    public  static final int list_rows = 15;

    public  static  String houseentity = ip+part+"/house/entity";
    public static  String housefictitious = ip+part+"/house/fictitious";


    public static String update = ip+part+"/common/appversion";

    public static String register = ip+part+"/admin/register" ;
    public static String login = ip+part+"/admin/login" ;
    public static String changepsw = ip+part+"/admin/changepassword";

    //所有注册用户信息表
    public static String adminHousemember = ip+part+"/adminhouse/member1";

    //----------------------------------------------

    //职位信息
    public static String adminposition = ip+part+"/admin/position" ;

    //项目列表接口
    public static String adminproject = ip+part+"/admin/project" ;

    //服务端验证码请求
    public static String admincode = ip+part+"/admin/code" ;

    //注册
    public static String adminregister = ip+part+"/admin/register" ;

    //修改密码
    public static String changepassword = ip+part+"/admin/changepassword" ;

    //实体/虚拟所有客户资料信息
    public static String ServicememberInfo = ip+part+"/Service/memberInfo" ;

    //服务端房源详情
    public static String adminhousedetail = ip+part+"/adminhouse/detail" ;

    //服务端实体房源列表
    public static String adminhouseentity = ip+part+"/adminhouse/entity" ;


    //服务端虚拟房源列表
    public static String adminhousefictitious = ip+part+"/adminhouse/fictitious" ;

    //服务端列表筛选条件
    public static String adminfilte = ip+part+"/admin/filte" ;

    //合同-房源位置检索
    public static String servicehouseSearch = ip+part+"/service/houseSearch" ;

    //服务端付款方式
    public static String servicefukuantype = ip+part+"/service/fukuantype" ;

    //服务端服务端推送合同
    public static String servicehetongtui = ip+part+"/service/hetongtui" ;

    //到帐确认列表
    public static String ServicezhangdanList = ip+part+"/service/zhangdanList" ;

    //项目经理/助理确认支付凭证
    public static String auditVoucher = ip+part+"/adminhouse/auditVoucher" ;

    //服务端查看支付凭证
    public static String adminhousevoucher = ip+part+"/adminhouse/voucher" ;

    //服务端登录
    public static String adminlogin = ip+part+"/admin/login" ;

    //服务端登录
    public static String adminhouseyuyuedetail = ip+part+"/adminhouse/yuyuedetail" ;

    //服务端预约列表
    public static String adminhouseyuyue = ip+part+"/adminhouse/yuyue" ;

    //服务端到账确认详情
    public static String zhangdanDetail = ip+part+"/service/zhangdanDetail" ;

    //我的签约详情
    public static String ServicecontractDetails = ip+part+"/service/contractDetails" ;

    //服务端注册用户信息
    public static String adminhousemember = ip+part+"/adminhouse/member" ;

    //服务端注册用户信息
    public static String ServicememberDetail = ip+part+"/Service/memberDetail" ;

    //填写物业交割信息
    public static String adminhousewuYe = ip+part+"/adminhouse/wuYe" ;

    //热门楼盘 (这个接口  客户端和服务端 两个app 用的是一样的)
    public static String commonbuild = ip+part+"/common/build" ;

    //热门楼盘详情
    public static String buildDetail = ip+part+"/adminhouse/buildDetail" ;

    //修改姓名（个人资料）
    public static String admineditInfo = ip+part+"/admin/editInfo" ;

    //修改手机号（个人资料）
    public static String admineditPhone = ip+part+"/admin/editPhone" ;

    //查看密码是否正确（个人资料）
    public static String adminverifyPassword = ip+part+"/admin/verifyPassword" ;

    //服务端修改头像
    public static String adminupdateImg = ip+part+"/admin/updateImg" ;

    //财务确认到帐
    public static String Serviceaccount = ip+part+"/Service/account" ;

    //首页轮播图
    public static String servicebanner = ip+part+"/service/banner" ;

    //我的签约
    public static String Servicecontract = ip+part+"/Service/contract" ;

    //生成物业交割单
    public static String wuyejiaoge = "http://vue.xinenhua.com/#/wuye" ;

    //生成合同
    public static String hetong = "http://vue.xinenhua.com/#/hetong" ;

    //预览生成的合同
    public static String yulan = ip+part+"/index/tmpdetails?id=" ;

    //服务端逾期待缴账单
    public static String ServicepayBill = ip+part+"/Service/payBill" ;

    //服务端代缴账单详情
    public static String ServicepayDetails = ip+part+"/Service/payDetails" ;


    public static final String errortip = "获取数据失败 请重试";

    public static String access_token(){
        Date day=new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String dd = df.format(day) ;
        String apikey = "dBJ$vY9sY#uGKQhuY^DkJsvtIx0ZHX%R";
        String finalstring =  dd+apikey ;
        return MD5.md5(finalstring) ;
        //return "7289194177341378fe9736bc220b6b68" ;
    }

    //得到登錄用戶信息
    public static User getUser(){
        SPUtils spUtils = SPUtils.getInstance();
        String jjjj = spUtils.getString("user","");

        if(StringUtils.isBlank(jjjj)){
            return null;
        }else {
            Gson gson = new Gson();
            User user =  gson.fromJson(jjjj,User.class);
            return user;
        }
    }




}
