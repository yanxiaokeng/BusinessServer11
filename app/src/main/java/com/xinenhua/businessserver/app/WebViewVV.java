package com.xinenhua.businessserver.app;

import android.app.Activity;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Button;

import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.jwkj.CommWebView;
import com.jwkj.WebViewCallback;
import com.kennyc.view.MultiStateView;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.ToastUtils;
import com.xinenhua.businessserver.R;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

/**
 *  webview的测试类 使用CommWebView
 */
@ContentView(R.layout.webview1)
public class WebViewVV extends BaseFragmentActivity {

    @ViewInject(R.id.testbutton)
    Button testButton;

    @ViewInject(R.id.webView)
    CommWebView mWebView ;

    @ViewInject(R.id.multiStateView)
    MultiStateView mMultiStateView;

    @Override
    protected void start() {
        super.start();
        mWebView.setCurWebUrl("http://www.baidu.com")
                .startCallback(new WebViewCallback() {
                    @Override
                    public void onStart() {
                        Log.e(TAG,"开始调用了");

                    }

                    @Override
                    public void onProgress(int curProgress) {
                       Log.e(TAG,"onProgress"+String.valueOf(curProgress));

                    }

                    @Override
                    public void onError(int errorCode, String description, String failingUrl) {
                        super.onError(errorCode, description, failingUrl);
                        Log.e(TAG,errorCode + " \t" + description + "\t" + failingUrl);
                    }
                });


        //mWebView.loadUrl("http://x.oliwen.cn");
        //mWebView.loadUrl("http://www.baidu.com");

        mMultiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);


    }


    //桥
    public class JsJavaBridge {

        private Activity activity;
        private BridgeWebView webView;

        public JsJavaBridge(Activity activity, BridgeWebView webView) {
            this.activity = activity;
            this.webView = webView;
        }

        @JavascriptInterface
        public void onFinishActivity() {
            activity.finish();
        }

        @JavascriptInterface
        public void showToast(String msg) {
            ToastUtils.shortToast(activity,msg);
        }
    }




}
