package com.xinenhua.businessserver.app;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.kennyc.view.MultiStateView;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.ToastUtils;
import com.xinenhua.businessserver.R;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

/**
 * webview 的测试类 使用 BridgeWebView
 *
 * */

@ContentView(R.layout.webview)
public class WebViewV extends BaseFragmentActivity {

    @ViewInject(R.id.testbutton)
    Button testButton;

    @ViewInject(R.id.webView)
    BridgeWebView mWebView ;

    @ViewInject(R.id.multiStateView)
    MultiStateView mMultiStateView;

    @Override
    protected void start() {
        super.start();

        //actionBar.hide();

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl("http://www.baidu.com");


        WebSettings webSettings = mWebView.getSettings();
        // 使能JavaScript
        webSettings.setJavaScriptEnabled(true);


        Intent intent = getIntent() ;
        String title = intent.getStringExtra("title") ;
        String url = intent.getStringExtra("url") ;

        //让网页自适应设备大小
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);

        mWebView.setWebViewClient(new WebViewClient(){



            @Override
            public void onPageFinished(WebView view, String url) {
                // TODO Auto-generated method stub
                super.onPageFinished(view, url);
                mMultiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);


                testButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mWebView.loadUrl("javascript:success('安卓调用js')");
                        //ToastUtils.shortToast(context,"1111");
                    }
                });

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // TODO Auto-generated method stub
                super.onPageStarted(view, url, favicon);
                mMultiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
            }

        });

        mWebView.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && mWebView.canGoBack()) {  //表示按返回键时的操作
                        mWebView.goBack();   //后退
                        return true;    //已处理
                    }
                }
                return false;
            }
        });

        mWebView.addJavascriptInterface(new JsJavaBridge(this, mWebView), "$App");

    }



    public class JsJavaBridge {

        private Activity activity;
        private BridgeWebView webView;

        public JsJavaBridge(Activity activity, BridgeWebView webView) {
            this.activity = activity;
            this.webView = webView;
        }

        @JavascriptInterface
        public void onFinishActivity() {
            activity.finish();
        }

        @JavascriptInterface
        public void showToast(String msg) {
            ToastUtils.shortToast(activity,msg);
        }
    }




}
