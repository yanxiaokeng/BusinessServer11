package com.xinenhua.businessserver.buildd;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.AdapterView;

import com.githang.statusbar.StatusBarCompat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kennyc.view.MultiStateView;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.ToastUtils;
import com.oasis.oasislib.view.XListView;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.entity.BuildDetail;
import com.xinenhua.businessserver.http.BaseHttp;


import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * 热门楼盘 列表
 */
@ContentView(R.layout.hotbuilding)
public class HotBuilding extends BaseFragmentActivity implements XListView.XListViewListener{

    @ViewInject(R.id.multiStateView)
    MultiStateView multiStateView ;

    @ViewInject(R.id.xlist_campaign)
    XListView xListView ;

    ArrayList<HotBuildingPojo> list = new ArrayList<>() ;

    HotBuildingAdapter adapter;

    @Override
    protected void start() {
        super.start();
        actionBar.hide();

        findViewById(R.id.warpper_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        StatusBarCompat.setStatusBarColor(HotBuilding.this, getResources().getColor(R.color.app_green));

        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        xListView.setPullLoadEnable(false);
        xListView.setPullRefreshEnable(true);
        xListView.setHeaderProgressDrawable(ContextCompat.getDrawable(HotBuilding.this,R.mipmap.default_ptr_rotate));
        xListView.setRefreshTime();
        xListView.setPULL_LOAD_MORE_DELTA(60);
        xListView.setSCROLL_DURATION(400);
        xListView.setOFFSET_RADIO(3.0f);
        xListView.setXListViewListener(this);

        adapter = new HotBuildingAdapter(HotBuilding.this) ;
        adapter.setList(list);
        xListView.setAdapter(adapter);
        xListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //热门楼盘 不跳转大厦详情了。。。

               /* Bundle bundle=new Bundle();
                bundle.putString("id",String.valueOf(list.get(position-1).getId()));

                Intent intent = new Intent();
                intent.putExtras(bundle);
                intent.setClass(HotBuilding.this, BuildDetail.class);
                startActivity(intent);*/
            }
        });

        getData();
    }

    private void getData() {

        //这个接口的url  是使用的客户端app的url
        Call call = BaseHttp.getCallWithUrlAndData(Const.commonbuild,null);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        multiStateView.setViewState(MultiStateView.VIEW_STATE_ERROR);
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                LogUtil.e(TAG,"热门楼盘--"+rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);

                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<HotBuildingPojo>>(){}.getType();
                    List<HotBuildingPojo> listfromweb = gson.fromJson(s, listType);
                    list.addAll(listfromweb);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);

                            xListView.setRefreshTime();
                            xListView.stopLoadMore();
                            xListView.stopRefresh();
                        }
                    });

                    System.out.println(list);
                    System.out.println("------------------------------------------");
                }else {
                    ToastUtils.shortToast(context,BaseHttp.getJsonMsg(rr));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                            multiStateView.setViewState(MultiStateView.VIEW_STATE_ERROR);

                            xListView.setRefreshTime();
                            xListView.stopLoadMore();
                            xListView.stopRefresh();
                        }
                    });
                }

            }
        });

    }

    @Override
    public void onRefresh() {
        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        list.clear();
        adapter.notifyDataSetChanged();
        getData();

    }

    @Override
    public void onLoadMore() {

    }
}
