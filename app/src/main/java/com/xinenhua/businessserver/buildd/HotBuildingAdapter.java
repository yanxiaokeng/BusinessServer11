package com.xinenhua.businessserver.buildd;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasis.oasislib.base.AppBaseAdapter;
import com.oasis.oasislib.utils.LogUtil;
import com.xinenhua.businessserver.R;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

/**
 * 热门楼盘列表适配器
 */

public class HotBuildingAdapter extends AppBaseAdapter<HotBuildingPojo> {

    public HotBuildingAdapter(Activity context) {
        super(context);
    }

    private Context context;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HotBuildingAdapter.ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.hotbuildingitem, parent, false);
            holder = new HotBuildingAdapter.ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (HotBuildingAdapter.ViewHolder) convertView.getTag();
        }
        final HotBuildingPojo t = list.get(position) ;

        holder.loc.setText(t.getAddress()==null?"":t.getAddress());
        holder.title.setText(t.getName()==null?"":t.getName());

        LogUtil.e("热门楼盘 ---" +t  );

        return convertView;
    }

    static class ViewHolder {

        @ViewInject(R.id.textVi3ew15)
        TextView title ;

        @ViewInject(R.id.textVi33ew15)
        TextView loc;

        public ViewHolder(View itemView) {
            x.view().inject(this, itemView);
        }
    }

}
