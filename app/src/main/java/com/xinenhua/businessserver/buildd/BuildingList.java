package com.xinenhua.businessserver.buildd;

/**
 * 热门楼盘  也就是大厦 列表pojo
 */

public class BuildingList {

    private int id;

    private String name;

    private String address;

    private String create_time;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return this.address;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getCreate_time() {
        return this.create_time;
    }

}
