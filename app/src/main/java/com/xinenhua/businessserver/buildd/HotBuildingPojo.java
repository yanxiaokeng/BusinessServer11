package com.xinenhua.businessserver.buildd;

/**
 * 热门楼盘 列表pojo
 */

public class HotBuildingPojo {
    private int id;
    private String name ;
    private String address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String create_time;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }


    @Override
    public String toString() {
        return "id  "+id+ " name "+name;
    }
}
