package com.xinenhua.businessserver.personal;

import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ActivityUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.ToastUtils;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.Holder;
import com.orhanobut.dialogplus.ViewHolder;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.http.BaseHttp;
import com.xinenhua.businessserver.login.Login;
import com.xinenhua.businessserver.login.pojo.User;

import org.apache.commons.lang3.StringUtils;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * 個人中心  个人资料  修改手机号
 */
@ContentView(R.layout.changephone)
public class ChangePhone extends BaseFragmentActivity {

    @ViewInject(R.id.textView7)
    TextView textView7;

    @ViewInject(R.id.imageButton)
    RelativeLayout imageButton;

    //当前手机号码
    @ViewInject(R.id.textView14)
    TextView nowPhone;

    //当前登录用户
    User user;

    @Override
    protected void start() {
       actionBar.hide();

       user = Const.getUser();


        nowPhone.setText("你的手机号："+user.getPhone());

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Holder holder = new ViewHolder(R.layout.personinfo_inputpsw);
                final DialogPlus dialog = DialogPlus.newDialog(ChangePhone.this)
                        .setContentBackgroundResource(android.R.color.transparent)
                        .setGravity(Gravity.CENTER)
                        .setContentHolder(holder)
                        .create();

                View holderView = dialog.getHolderView();
                //输入框文本
                final EditText pswedit = (EditText) holderView.findViewById(R.id.editText3);
                //取消
                holderView.findViewById(R.id.textView20).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LogUtil.e(TAG,"取消");
                        //取消
                        dialog.dismiss();
                    }
                });
                //确认
                holderView.findViewById(R.id.okkk).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LogUtil.e(TAG,"确认");
                        String psw = pswedit.getText().toString();
                        if(!StringUtils.isBlank(psw)){
                            next(psw);
                        }
                    }
                });

                dialog.show();
            }


        });
    }
    private void next(String psw) {

        //code = 1 账号密码正确 code=0  账号密码错误

        HashMap<String,String> map = new HashMap<>() ;
        map.put("phone",user.getPhone());
        map.put("password",psw);

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminverifyPassword,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){

                    String s = BaseHttp.getJsonDataAsString(rr);
                    JsonParser parser = new JsonParser();
                    JsonArray jsonArray = parser.parse(s).getAsJsonArray();
                    String finalr = jsonArray.get(0).getAsJsonObject().get("code").getAsString();

                    if(finalr.equals("1")){
                        //密码正确
                        LogUtil.e(TAG,"密码正确");
                        ActivityUtils.startActivity(ChangePhoneReal.class);
                    }else {
                        //密码错误
                        LogUtil.e(TAG,"密码错误");
                        showMsg("密码错误");

                    }
                    System.out.println("------------------------------------------");
                }else {
                    showMsg(BaseHttp.getJsonMsg(rr));
                }

            }
        });

    }



}
