package com.xinenhua.businessserver.personal.finance;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasis.oasislib.base.AppBaseAdapter;
import com.oasis.oasislib.utils.LogUtil;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.bill.AccountListPojo;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

/**
 *
 */

public class FinanceAdapter extends AppBaseAdapter<AccountListPojo> {


    public FinanceAdapter(Activity context) {
        super(context);
    }


    private Context context;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FinanceAdapter.ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.confirmfinancelist_item, parent, false);
            holder = new FinanceAdapter.ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (FinanceAdapter.ViewHolder) convertView.getTag();
        }
        final AccountListPojo t = list.get(position) ;

        LogUtil.e("!!!!!!!!!",t.toString());

        holder.date.setText(t.getJihuo_time());
        holder.name.setText(t.getUsername());


        return convertView;
    }

    static class ViewHolder {


        @ViewInject(R.id.date)
        TextView date ;

        @ViewInject(R.id.name)
        TextView name;


        public ViewHolder(View itemView) {
            x.view().inject(this, itemView);
        }
    }

}
