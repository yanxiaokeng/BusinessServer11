package com.xinenhua.businessserver.personal;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasis.oasislib.base.AppBaseAdapter;
import com.oasis.oasislib.utils.LogUtil;
import com.xinenhua.businessserver.R;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

/**
 * Created by oasis on 2019/3/11.
 */
public class FicCustomerInfoAdapter extends AppBaseAdapter<FicCustomerInfoPojo> {


    public FicCustomerInfoAdapter(Activity context) {
        super(context);
    }

    private Context context;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FicCustomerInfoAdapter.ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.ficcustomerinfo_item, parent, false);
            holder = new FicCustomerInfoAdapter.ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (FicCustomerInfoAdapter.ViewHolder) convertView.getTag();
        }
        final FicCustomerInfoPojo t = list.get(position) ;

        LogUtil.e("!!!!!!!!!",t.toString());

        final FicCustomerInfoPojo t1 = list.get(position) ;

        holder.user.setText(t1.getYf_jinjilianxiren());
        holder.company.setText(t1.getYf_chengzufang());
        holder.phone.setText(t1.getYf_lianxifangshi());


        return convertView;
    }

    static class ViewHolder {


        @ViewInject(R.id.textView19)
        TextView user ;

        @ViewInject(R.id.textView22)
        TextView company;

        @ViewInject(R.id.textView23)
        TextView phone ;



        public ViewHolder(View itemView) {
            x.view().inject(this, itemView);
        }
    }

}
