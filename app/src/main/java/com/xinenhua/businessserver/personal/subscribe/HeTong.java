package com.xinenhua.businessserver.personal.subscribe;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.kennyc.view.MultiStateView;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.ToastUtils;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.login.pojo.User;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

/**
 *
 * 生产环境的生成合同   请求的url  :   public static String hetong = "http://vue.xinenhua.com/#/hetong" ;
 *
 */
@ContentView(R.layout.hetong)
public class HeTong extends BaseFragmentActivity {


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String event) {
        LogUtil.e(TAG,"onMessageEvent-----"+event);
        finish();
    }


    @ViewInject(R.id.webView)
    BridgeWebView mWebView ;

    @ViewInject(R.id.multiStateView)
    MultiStateView mMultiStateView;

    String house_id ;
    String yuyue_id;
    User user ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void start() {
        super.start();

        user = Const.getUser();

        house_id = getIntent().getStringExtra("house_id");
        yuyue_id = getIntent().getStringExtra("yuyue_id");

        LogUtil.e(TAG,"house_id----"+house_id);
        LogUtil.e(TAG,"yuyue_id----"+yuyue_id);

        mWebView.getSettings().setJavaScriptEnabled(true);
        String url = Const.hetong+"?house_id="+house_id+"&token="+Const.access_token()+"&admin_id="+user.getAdmin_id()+"&yuyue_id="+yuyue_id;
        LogUtil.e(TAG,"vue url----"+url);
        mWebView.loadUrl(url);

        WebSettings webSettings = mWebView.getSettings();
        // 使能JavaScript
        webSettings.setJavaScriptEnabled(true);


        //让网页自适应设备大小
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);

        mWebView.setWebViewClient(new WebViewClient(){

            @Override
            public void onPageFinished(WebView view, String url) {
                // TODO Auto-generated method stub
                super.onPageFinished(view, url);
                mMultiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // TODO Auto-generated method stub
                super.onPageStarted(view, url, favicon);
                mMultiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
            }

        });

        mWebView.addJavascriptInterface(new HeTong.JsJavaBridge(this, mWebView), "$App");

    }



    public class JsJavaBridge {

        private Activity activity;
        private BridgeWebView webView;

        public JsJavaBridge(Activity activity, BridgeWebView webView) {
            this.activity = activity;
            this.webView = webView;
        }

        @JavascriptInterface
        public void onFinishActivity(String id) {
            LogUtil.e(TAG,"onFinishActivity id="+id);
            //finish();
            Intent openWelcomeActivityIntent=new Intent();
            Bundle bundle=new Bundle();
            bundle.putString("id",id);
            openWelcomeActivityIntent.putExtras(bundle);
            openWelcomeActivityIntent.setClass(context, HeTongConfirm.class);
            startActivity(openWelcomeActivityIntent);


        }


        @JavascriptInterface
        public void showToast(String msg) {
            LogUtil.e(TAG,"showToast");
            ToastUtils.shortToast(activity,msg);
        }
    }

}
