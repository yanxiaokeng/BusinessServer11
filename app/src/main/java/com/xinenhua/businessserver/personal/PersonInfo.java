package com.xinenhua.businessserver.personal;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.IntentUtils;
import com.blankj.utilcode.util.SPUtils;
import com.githang.statusbar.StatusBarCompat;
import com.google.gson.Gson;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.ToastUtils;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.Holder;
import com.orhanobut.dialogplus.OnClickListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.http.BaseHttp;
import com.xinenhua.businessserver.login.pojo.User;

import org.apache.commons.lang3.StringUtils;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * 个人资料页面
 */
@ContentView(R.layout.personinfo)
public class PersonInfo extends BaseFragmentActivity{

    @ViewInject(R.id.name)
    RelativeLayout name;

    @ViewInject(R.id.changephone)
    RelativeLayout changephone;


    /**
     * 职位和项目登录后不可修改
     */
    @ViewInject(R.id.zhiwei)
    RelativeLayout zhiwei;

    @ViewInject(R.id.xiangmu)
    RelativeLayout xiangmu;


    //姓名
    @ViewInject(R.id.textView12)
    TextView nameTextView ;

    //联系电话
    @ViewInject(R.id.tex1tView12)
    TextView phoneTextView ;

    //所属职位
    @ViewInject(R.id.tex13tVi3ew12)
    TextView zhiweiTextView ;

    //所属项目
    @ViewInject(R.id.tex13t1Vi3ew12)
    TextView xiangmuTextView ;

    //当前登录用户
    User user;

    @ViewInject(R.id.warpper_message)
    RelativeLayout warpper_message;

    @Override
    protected void start() {
        StatusBarCompat.setStatusBarColor(PersonInfo.this, getResources().getColor(R.color.app_green));
        actionBar.hide();

        user = Const.getUser() ;

        nameTextView.setText(user.getNickname());
        phoneTextView.setText(user.getPhone());
        zhiweiTextView.setText(user.getPosition());
        xiangmuTextView.setText(user.getProject());


        //修改昵称
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Holder holder = new ViewHolder(R.layout.personinfo_username);
                DialogPlus dialog = DialogPlus.newDialog(PersonInfo.this)
                        .setContentHolder(holder)
                        .setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(final DialogPlus dialog, final View view) {
                                //取消
                                dialog.getHolderView().findViewById(R.id.quxiao).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                    }
                                });
                                //确认
                                dialog.getHolderView().findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        LogUtil.e(TAG,"确认");
                                        TextView textView = (TextView) dialog.getHolderView().findViewById(R.id.editText3);
                                        String content = textView.getText().toString();
                                        if(!StringUtils.isBlank(content)){
                                            changename(user.getAdmin_id()+"",content);
                                        }
                                        LogUtil.e(TAG,"修改后的名字："+textView.getText());
                                        dialog.dismiss();
                                    }
                                });
                            }
                        })
                        .setContentBackgroundResource(android.R.color.transparent)
                        .setGravity(Gravity.CENTER)
                        .create();



                dialog.show();
            }
        });


        changephone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(ChangePhone.class);
            }
        });


        warpper_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



       /* zhiwei.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogPlus dialog = DialogPlus.newDialog(PersonInfo.this)
                        .setAdapter(new ArrayAdapter<String>(PersonInfo.this, android.R.layout.simple_list_item_1, new ArrayList<String>(Arrays.asList(array))))
                        .setContentBackgroundResource(android.R.color.white)
                        .setGravity(Gravity.CENTER)
                        .setOnItemClickListener(new OnItemClickListener() {
                            @Override
                            public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                                dialog.dismiss();
                                ToastUtils.shortToast(context,array[position]);
                            }
                        })
                        .create();

                dialog.show();
            }
        });

        xiangmu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogPlus dialog = DialogPlus.newDialog(PersonInfo.this)
                        .setAdapter(new ArrayAdapter<String>(PersonInfo.this, android.R.layout.simple_list_item_1, new ArrayList<String>(Arrays.asList(array))))
                        .setContentBackgroundResource(android.R.color.white)
                        .setGravity(Gravity.CENTER)
                        .setOnItemClickListener(new OnItemClickListener() {
                            @Override
                            public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                                dialog.dismiss();
                                ToastUtils.shortToast(context,array[position]);
                            }
                        })
                        .create();

                dialog.show();
            }
        });*/



    }

    //修改姓名
    private void changename(final String userid, final String content){

        HashMap<String,String> map = new HashMap<>() ;
        map.put("admin_id",userid);
        map.put("nickname",content);

        Call call = BaseHttp.getCallWithUrlAndData(Const.admineditInfo,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");
                showMsg("修改失败");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                LogUtil.e(TAG,rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    System.out.println("String--"+s);

                    user.setNickname(content);

                    //存储用户信息
                    SPUtils spUtils = SPUtils.getInstance();
                    spUtils.put("user",new Gson().toJson(user));

                    showMsg(BaseHttp.getJsonMsg(rr));
                    //重新回到這個界面
                    ActivityUtils.startActivity(PersonInfo.class);

                    //关闭此界面
                    finish();
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                    showMsg(BaseHttp.getJsonMsg(rr));
                }

            }
        });

    }


}
