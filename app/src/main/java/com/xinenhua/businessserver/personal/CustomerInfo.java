package com.xinenhua.businessserver.personal;

import android.view.View;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.ActivityUtils;
import com.githang.statusbar.StatusBarCompat;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.xinenhua.businessserver.R;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

/**
 * 个人中心 客户资料
 */
@ContentView(R.layout.customerinfo)
public class CustomerInfo extends BaseFragmentActivity {

    @ViewInject(R.id.fic)
    RelativeLayout fic;

    @ViewInject(R.id.entity)
    RelativeLayout entity;

    @ViewInject(R.id.warpper_message)
    RelativeLayout warpper_message;

    @Override
    protected void start() {
        super.start();
        StatusBarCompat.setStatusBarColor(CustomerInfo.this, getResources().getColor(R.color.app_green));
        actionBar.hide();
        //跳转到虚拟客户资料
        fic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(CustomerInfo.this,FicCustomerInfo.class);
            }
        });
        //跳转到实体客户资料
        entity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(CustomerInfo.this,EntityCustomerInfo.class);
            }
        });


        warpper_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


}
