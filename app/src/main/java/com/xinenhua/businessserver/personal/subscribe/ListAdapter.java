package com.xinenhua.businessserver.personal.subscribe;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ActivityUtils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.oasis.oasislib.base.AppBaseAdapter;
import com.oasis.oasislib.utils.LogUtil;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.entity.YuyueListItemPojo;

import org.apache.commons.lang3.StringUtils;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.Arrays;

import co.lujun.androidtagview.TagContainerLayout;

/**
 * 个人中心  实体预约   预约列表 adapter
 */

public class ListAdapter extends AppBaseAdapter<YuyueListItemPojo> {

    public ListAdapter(Activity context) {
        super(context);
        this.context = context;
    }


    private Context context;


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.entitysubscribelistitem, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final YuyueListItemPojo t = list.get(position) ;

        LogUtil.e("!!!!!!!!!",t.toString());

        //显示图片
        ImageLoader.getInstance().displayImage(t.getImage(),holder.imageView11);

        //标题
        holder.title.setText(t.getTitle());

        //房源描述
        String des = "" ;
        des = des+t.getSquare()+"㎡ | "+t.getFloor()+"/"+t.getTotal_floor() ;
        holder.des.setText(des);


        holder.lieanr_tag.removeAllViews();
        //标签
        if(!StringUtils.isBlank(t.getTag())){
            String[] tags = t.getTag().split(",");

            for(String t1 :tags){
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT
                        ,ViewGroup.LayoutParams.MATCH_PARENT);
                View view =  LayoutInflater.from(context).inflate(R.layout.listtag_textview,null);
                TextView tv = (TextView) view.findViewById(R.id.tvtv);
                tv.setText(t1);
                holder.lieanr_tag.addView(view,lp);
            }

        }

        //房源状态
        holder.tagg.setText(t.getEntity_status());

        //详情 跳转
        holder.textView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent openWelcomeActivityIntent=new Intent();
                Bundle bundle=new Bundle();
                bundle.putString("id",t.getId()+"");
                LogUtil.e("ListAdapter",t.getId()+"");

                openWelcomeActivityIntent.putExtras(bundle);
                openWelcomeActivityIntent.setClass(context, SubscribeDetail.class);
                context.startActivity(openWelcomeActivityIntent);
            }
        });

        return convertView;
    }

    static class ViewHolder {

        @ViewInject(R.id.imageView11)
        ImageView imageView11 ;

        @ViewInject(R.id.title)
        TextView title ;

        @ViewInject(R.id.tagg)
        TextView tagg;

        @ViewInject(R.id.des)
        TextView des ;

        @ViewInject(R.id.des2)
        TextView des2 ;

        @ViewInject(R.id.lieanr_tag)
        LinearLayout lieanr_tag ;

        @ViewInject(R.id.textView4)
        TextView textView4;

        public ViewHolder(View itemView) {
            x.view().inject(this, itemView);
        }
    }
}
