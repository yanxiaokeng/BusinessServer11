package com.xinenhua.businessserver.personal.order;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.githang.statusbar.StatusBarCompat;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.personal.SimpleCardFragment;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;

/**
 * 个人中心  我的签约 虚拟签约
 */
@ContentView(R.layout.myfictitiousorder)
public class MyFictitiousOrder extends BaseFragmentActivity implements OnTabSelectListener {

    private ArrayList<SimpleCardFragment> mFragments = new ArrayList<>();

    private final String[] mTitles = {
            "全部", "已完成", "待上传"
            , "业审核","财审核"
    };

    @ViewInject(R.id.tl_3)
    SlidingTabLayout tabLayout_3 ;

    @ViewInject(R.id.vp)
    ViewPager vp;

    private MyFictitiousOrder.MyPagerAdapter mAdapter;

    @ViewInject(R.id.warpper_message)
    RelativeLayout warpper_message;

    @Override
    protected void start() {
        super.start();
        actionBar.hide();
        StatusBarCompat.setStatusBarColor(MyFictitiousOrder.this, getResources().getColor(R.color.app_green));


        mFragments.add(SimpleCardFragment.getInstance("","1"));
        mFragments.add(SimpleCardFragment.getInstance("5","1"));
        mFragments.add(SimpleCardFragment.getInstance("2","1"));
        mFragments.add(SimpleCardFragment.getInstance("3","1"));
        mFragments.add(SimpleCardFragment.getInstance("4","1"));


        mAdapter = new MyFictitiousOrder.MyPagerAdapter(getSupportFragmentManager());
        vp.setAdapter(mAdapter);
        vp.setOffscreenPageLimit(5);
        tabLayout_3.setViewPager(vp);


        warpper_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }



    @Override
    public void onTabSelect(int position) {
        //Toast.makeText(context, "onTabSelect&position--->" + position, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTabReselect(int position) {
        //Toast.makeText(context, "onTabReselect&position--->" + position, Toast.LENGTH_SHORT).show();
    }


    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mTitles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }

    private void getData(){

    }
}
