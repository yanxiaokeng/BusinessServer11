package com.xinenhua.businessserver.personal.order;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.oasis.oasislib.base.AppBaseAdapter;
import com.oasis.oasislib.utils.LogUtil;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.entity.ListPojo;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import co.lujun.androidtagview.TagContainerLayout;


public class OrderListPojo{

    private int zid;

    private String phone;

    private String custom_name;

    private String nickname;

    private String dingdanhao;

    private int zhangdan_status;

    public void setZid(int zid){
        this.zid = zid;
    }
    public int getZid(){
        return this.zid;
    }
    public void setPhone(String phone){
        this.phone = phone;
    }
    public String getPhone(){
        return this.phone;
    }
    public void setCustom_name(String custom_name){
        this.custom_name = custom_name;
    }
    public String getCustom_name(){
        return this.custom_name;
    }
    public void setNickname(String nickname){
        this.nickname = nickname;
    }
    public String getNickname(){
        return this.nickname;
    }
    public void setDingdanhao(String dingdanhao){
        this.dingdanhao = dingdanhao;
    }
    public String getDingdanhao(){
        return this.dingdanhao;
    }
    public void setZhangdan_status(int zhangdan_status){
        this.zhangdan_status = zhangdan_status;
    }
    public int getZhangdan_status(){
        return this.zhangdan_status;
    }
}
