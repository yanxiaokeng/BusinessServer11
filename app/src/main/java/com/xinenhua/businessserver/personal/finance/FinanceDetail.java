package com.xinenhua.businessserver.personal.finance;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.base.ImageShowActivity;
import com.oasis.oasislib.utils.DialogUtils;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.ToastUtils;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.http.BaseHttp;
import com.xinenhua.businessserver.login.pojo.User;
import com.xinenhua.businessserver.personal.order.OrderDetail;

import org.apache.commons.lang3.StringUtils;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * 到账确认-------- 列表 -------------到账确认详情
 */
@ContentView(R.layout.financedetail)
public class FinanceDetail extends BaseFragmentActivity{

    String id = "" ;

    Dialog loadingDialog;

    @ViewInject(R.id.username)
    TextView username ;

    @ViewInject(R.id.dingdanhao)
    TextView dingdanhao ;

    @ViewInject(R.id.title)
    TextView title ;

    @ViewInject(R.id.hetongqixian)
    TextView hetongqixian ;



    @ViewInject(R.id.housenum)
    TextView housenum ;

    @ViewInject(R.id.huikuanxinxi)
    TextView huikuanxinxi ;

    @ViewInject(R.id.huikuanfeiyong)
    TextView huikuanfeiyong ;

    @ViewInject(R.id.pingzheng)
    Button pingzheng;

    @ViewInject(R.id.statebutton)
    Button statebutton;

    FinanceDetailPojo item ;

    User user ;

    @ViewInject(R.id.imageView11)
    ImageView imageLeft;


    @Override
    protected void start() {
        super.start();

        user = Const.getUser();

        //账单状态 1已生成待激活 2 已激活待上传缴费 3 待业务审核 4 待财务审核 5 完成
        //  这个列表只会显示   5：已确认 4：待确认 为空显示全部

        actionBar.hide();
        StatusBarCompat.setStatusBarColor(FinanceDetail.this, getResources().getColor(R.color.app_green));

        id =  getIntent().getStringExtra("id");
        LogUtil.e(TAG,"id----"+id);

        getData();

        //确认到账按钮
        statebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if("4".equals(item.getZhangdan_status()+"")){
                    //statebutton.setText("已确认");
                    LogUtil.e(TAG,"执行确认到账操作");
                    account();
                }

                if("5".equals(item.getZhangdan_status()+"")){
                    statebutton.setText("已确认");
                    statebutton.setClickable(false);
                }
            }
        });

        //查看凭证按钮
        pingzheng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!StringUtils.isBlank(item.getImage())){
                    //查看图片功能
                    Intent intent  = new Intent();

                    Bundle bundle=new Bundle();

                    ArrayList l = new ArrayList();
                    l.add(item.getImage());

                    bundle.putStringArrayList("showdata",l);
                    intent.putExtras(bundle);

                    intent.setClass(context, ImageShowActivity.class);
                    startActivity(intent);
                }

            }
        });

        findViewById(R.id.warpper_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
    //执行确认到账操作
    public void account(){

        loadingDialog = DialogUtils.createLoadingDialog(context);
        loadingDialog.show();

        HashMap<String,String> map = new HashMap<>() ;
        map.put("zhangdan_id",item.getZhangdan_id());
        map.put("admin_id",String.valueOf(user.getAdmin_id())) ;

        Call call = BaseHttp.getCallWithUrlAndData(Const.Serviceaccount,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");
                loadingDialog.dismiss();
                ToastUtils.shortToast(context,"发生错误");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String rr = new String(response.body().string());
                LogUtil.e(TAG,rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ToastUtils.shortToast(context,BaseHttp.getJsonMsg(rr));
                            getData();
                        }
                    });

                    loadingDialog.dismiss();
                }else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ToastUtils.shortToast(context,BaseHttp.getJsonMsg(rr));
                            finish();
                        }
                    });
                    loadingDialog.dismiss();
                }

            }
        });
    }


    //获取账单详细信息
    public void getData(){

        loadingDialog = DialogUtils.createLoadingDialog(context);
        loadingDialog.show();

        HashMap<String,String> map = new HashMap<>() ;
        map.put("zhangdan_id",id);

        Call call = BaseHttp.getCallWithUrlAndData(Const.zhangdanDetail,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");
                loadingDialog.dismiss();
                ToastUtils.shortToast(context,"发生错误");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String rr = new String(response.body().string());
                LogUtil.e(TAG,rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);

                    Gson gson = new Gson();
                     item = gson.fromJson(s, FinanceDetailPojo.class) ;

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            username.setText(item.getUsername());
                            dingdanhao.setText("订单号:  "+item.getDingdanhao());
                            title.setText("承租方:  "+item.getTitle());
                            hetongqixian.setText("合同期限:  "+item.getQixian());

                            housenum.setText("房间号:  "+item.getHousenum());
                            huikuanxinxi.setText("汇款信息："+item.getFukuanfangshi());
                            huikuanfeiyong.setText("需缴费用: "+item.getPrice());

                            String image = item.getImage();
                            if(!StringUtils.isBlank(image)){
                                ImageLoader.getInstance().displayImage(image,imageLeft);
                            }


                            if("4".equals(item.getZhangdan_status()+"")){
                                statebutton.setText("确认到账");
                            }

                            if("5".equals(item.getZhangdan_status()+"")){
                                statebutton.setText("已确认");
                                statebutton.setClickable(false);
                            }

                        }
                    });

                    loadingDialog.dismiss();
                }else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ToastUtils.shortToast(context,BaseHttp.getJsonMsg(rr));
                            finish();
                        }
                    });
                    loadingDialog.dismiss();
                }

            }
        });

    }

    }



