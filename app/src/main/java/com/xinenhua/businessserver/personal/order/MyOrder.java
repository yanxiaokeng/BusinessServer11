package com.xinenhua.businessserver.personal.order;

import android.view.View;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.ActivityUtils;
import com.githang.statusbar.StatusBarCompat;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.xinenhua.businessserver.R;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

/**
 * 个人中心  我的签约
 */
@ContentView(R.layout.myorder)
public class MyOrder extends BaseFragmentActivity {

    @ViewInject(R.id.fic)
    RelativeLayout fic;

    @ViewInject(R.id.entity)
    RelativeLayout entity;

    @Override
    protected void start() {
        super.start();
        StatusBarCompat.setStatusBarColor(MyOrder.this, getResources().getColor(R.color.app_green));
        actionBar.hide();

        fic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(MyOrder.this,MyFictitiousOrder.class);
            }
        });

        entity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(MyOrder.this,MyEntityOrder.class);
            }
        });

        findViewById(R.id.warpper_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
