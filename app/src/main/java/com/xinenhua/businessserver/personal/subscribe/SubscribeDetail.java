package com.xinenhua.businessserver.personal.subscribe;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.DialogUtils;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.ToastUtils;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.Holder;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.contract.ContractDetail;
import com.xinenhua.businessserver.entity.YuyueDetail;
import com.xinenhua.businessserver.http.BaseHttp;
import com.xinenhua.businessserver.personal.PersonInfo;

import org.apache.commons.lang3.StringUtils;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * 个人中心  预约列表    预约详情
 */

@ContentView(R.layout.yuyue)
public class SubscribeDetail extends BaseFragmentActivity {

    @ViewInject(R.id.contact)
    Button contact ;

    String id ;

    Dialog dialog ;

    YuyueDetail yuyueDetail;

    @ViewInject(R.id.refreshimg)
    ImageView refreshimg;

    //大图
    @ViewInject(R.id.bigimg)
    ImageView bigimg;

    //房源状态  空置还是什么？
    @ViewInject(R.id.tagg)
    TextView tagg;

    //平米楼层
    @ViewInject(R.id.des)
    TextView des ;

    //标题
    @ViewInject(R.id.title)
    TextView title;

    //标签
    @ViewInject(R.id.tag_linear)
    LinearLayout tag_linear;

    //姓名
    @ViewInject(R.id.editText4)
    TextView name ;
    //性别
    @ViewInject(R.id.sex)
    TextView sex ;
    //电话
    @ViewInject(R.id.editTe2xt4)
    TextView phone ;
    //留言
    @ViewInject(R.id.editText2)
    TextView liuyan ;

    //生成合同
    @ViewInject(R.id.generate)
    RelativeLayout generate ;

    //查看支付凭证
    @ViewInject(R.id.showPay)
    RelativeLayout showPay ;

    //生成物业交割单
    @ViewInject(R.id.wuye)
    RelativeLayout wuye ;

    @ViewInject(R.id.wuye_show)
    TextView wuye_show;



    private void updateView(final YuyueDetail yuyueDetail) {

        generate.setBackgroundColor(getResources().getColor(R.color.app_green));
        generate.setClickable(true);

        showPay.setBackgroundColor(getResources().getColor(R.color.app_green));
        showPay.setClickable(true);

        wuye.setBackgroundColor(getResources().getColor(R.color.app_green));
        wuye.setClickable(true);

        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.e(TAG,"生成合同");
                Intent openWelcomeActivityIntent=new Intent();
                Bundle bundle=new Bundle();
                bundle.putString("house_id",yuyueDetail.getHouse_id()+"");
                bundle.putString("yuyue_id",yuyueDetail.getYuyue_id()+"");
                openWelcomeActivityIntent.putExtras(bundle);
                openWelcomeActivityIntent.setClass(context, HeTong.class);
                startActivity(openWelcomeActivityIntent);
            }
        });

        showPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.e(TAG,"查看支付凭证");
                Intent openWelcomeActivityIntent=new Intent();
                Bundle bundle=new Bundle();
                bundle.putString("ding",yuyueDetail.getDingdanhao()+"");
                openWelcomeActivityIntent.putExtras(bundle);
                openWelcomeActivityIntent.setClass(context, ContractDetail.class);
                startActivity(openWelcomeActivityIntent);
            }
        });

        wuye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.e(TAG,"生成物业交割单");
                Intent openWelcomeActivityIntent=new Intent();
                Bundle bundle=new Bundle();
                bundle.putString("id",yuyueDetail.getDingdanhao());
                openWelcomeActivityIntent.putExtras(bundle);
                openWelcomeActivityIntent.setClass(context, Wuye.class);
                startActivity(openWelcomeActivityIntent);

            }
        });

        liuyan.setText(yuyueDetail.getPs());
        phone.setText(yuyueDetail.getPhone());

        String sexs = "" ;
        if(yuyueDetail!=null&&yuyueDetail.getSex()==1){
            sexs = "男" ;
        }
        if(yuyueDetail!=null&&yuyueDetail.getSex()==2){
            sexs = "女" ;
        }
        sex.setText(sexs);
        name.setText(yuyueDetail.getName());


        tag_linear.removeAllViews();
        if(!StringUtils.isBlank(yuyueDetail.getTag())){
            String[] tags = yuyueDetail.getTag().split(",");

            for(String t1 :tags){
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT
                        ,ViewGroup.LayoutParams.MATCH_PARENT);
                View view =  LayoutInflater.from(context).inflate(R.layout.listtag_textview,null);
                TextView tv = (TextView) view.findViewById(R.id.tvtv);
                tv.setText(t1);
                tag_linear.addView(view,lp);
            }

        }

        title.setText(yuyueDetail.getTitle());

        String dess = "" ;
        dess = dess+yuyueDetail.getSquare()+"㎡ | "+yuyueDetail.getFloor()+"/"+yuyueDetail.getTotal_floor() ;
        des.setText(dess);
        tagg.setText(yuyueDetail.getEntity_status());

        ImageLoader.getInstance().displayImage(yuyueDetail.getImage(),bigimg);

        int house_status = yuyueDetail.getHouse_status();
        int wuye_status = yuyueDetail.getWuye_status() ;

        switch (house_status){
            case 1:
                //生成合同 不能点击   查看支付凭证 可以点击
                generate.setBackgroundColor(getResources().getColor(R.color.gray_line));
                generate.setClickable(false);

                showPay.setBackgroundColor(getResources().getColor(R.color.app_green));
                showPay.setClickable(true);

                if(wuye_status==0){
                    //物业交割单 还没生成  所以可以点击
                    wuye.setBackgroundColor(getResources().getColor(R.color.app_green));
                    wuye.setClickable(true);
                }
                if(wuye_status==1){
                    //物业交割单生成了 所以就不能点击了
                    wuye_show.setText("已生成交割单");

                    wuye.setBackgroundColor(getResources().getColor(R.color.gray_line));
                    wuye.setClickable(false);
                }

                break;
            case 0:

                //0   没合同  可以生成合同       （ 查看支付凭证 和 物业交割单   不能点击   生成合同可以点击  ）

                showPay.setBackgroundColor(getResources().getColor(R.color.gray_line));
                showPay.setClickable(false);

                wuye.setBackgroundColor(getResources().getColor(R.color.gray_line));
                wuye.setClickable(false);

                break;
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        getData();
    }

    @Override
    protected void start() {
        super.start();
        StatusBarCompat.setStatusBarColor(SubscribeDetail.this, getResources().getColor(R.color.app_green));
        actionBar.hide();

        id = getIntent().getStringExtra("id");
        LogUtil.e(TAG,"SubscribeDetail getid "+id);

        findViewById(R.id.finish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //拨打电话
        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = DialogUtils.getConfirmDialog(context, "拨打电话？", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent(Intent.ACTION_CALL);
                        Uri data = Uri.parse("tel:" + yuyueDetail.getPhone());
                        intent.setData(data);
                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            com.blankj.utilcode.util.ToastUtils.showLong("无电话权限 请去授权");
                            return;
                        }
                        context.startActivity(intent);

                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                dialog.show();
            }
        });


        refreshimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.e(TAG,"刷新----------");
                getData();
            }
        });




    }

    private void test(){
        Holder holder = new ViewHolder(R.layout.subscribe_callphone);
        DialogPlus dialog = DialogPlus.newDialog(SubscribeDetail.this)
                .setContentBackgroundResource(android.R.color.transparent)
                .setGravity(Gravity.CENTER)
                .setContentHolder(holder)
                .create();

        dialog.show();
    }


    private void getData(){

        dialog = DialogUtils.createLoadingDialog(context);
        dialog.show();

        HashMap<String,String> map = new HashMap<>() ;
        map.put("yuyue_id",id);

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminhouseyuyuedetail,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                showMsg("Fail");
                dialog.dismiss();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                LogUtil.e(TAG,"adminhouseyuyuedetail "+rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    Gson gson = new Gson();
                    Type listType = new TypeToken<YuyueDetail>(){}.getType();
                    yuyueDetail = gson.fromJson(s, listType);
                    LogUtil.e(TAG,"详情--"+yuyueDetail.toString());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateView(yuyueDetail);
                        }
                    });


                    System.out.println("------------------------------------------");
                }else {
                    showMsg(BaseHttp.getJsonMsg(rr));
                }

                dialog.dismiss();

            }
        });
    }

    /**
     *
     * house_status    1   代表这个房源 有订单号  说明有合同  不能生成合同     （  生成合同 不能点击   查看支付凭证 可以点击    ）
     * 生成物业交割单 需要看 wuye_status   （如果 1   代表有交割单   不能生成物业交割单 不可以点击   如果 0   代表没有交割单   生成物业交割单 可以点击 ）
     *
     *
     *
     *
     * 0   没合同  可以生成合同       （ 查看支付凭证 和 物业交割单   不能点击   生成合同可以点击  ）
     *
     *
     * {
     "house_id": 4,
     "yuyue_id": 4,
     "title": "创智东园 河西陈塘庄CBD创智东园1",
     "image": "http://192.168.1.114/upload/picture/default/2.png",
     "tag": "停车场,纯写字楼,复式",
     "square": "40",
     "floor": "16",
     "total_floor": "16",
     "entity_status": "已租",
     "fictitious_status": "已注册",
     "name": "jjjjjjjj",
     "sex": 1,
     "phone": "123456456",
     "ps": "333",
     "yuyue_status": 1,
     "house_status": 1,
     "dingdanhao": "2019052095ce26ed4a66e7",
     "wuye_status": 1,
     "hetong_id": 93        房源详情里面  判断 这个 hetong_id   如果不为空   证明有合同    拿合同id  去查看客户资料
     }
     */




           /* //生成合同
        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.e(TAG,"生成合同");
                generate.setBackgroundColor(getResources().getColor(R.color.gray_line));
                generate.setClickable(false);
            }
        });
        //查看支付凭证
        showPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.e(TAG,"查看支付凭证");
            }
        });
        //生成物业交割单
        wuye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.e(TAG,"生成物业交割单");
            }
        });*/




}
