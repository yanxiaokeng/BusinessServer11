package com.xinenhua.businessserver.personal.subscribe;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.EditText;

import com.githang.statusbar.StatusBarCompat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kennyc.view.MultiStateView;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.view.XListView;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.entity.YuyueListItemPojo;
import com.xinenhua.businessserver.http.BaseHttp;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * 个人中心 实体预约   虚拟预约列表
 */
@ContentView(R.layout.entitysubscribe)
public class FicSubscribe extends  BaseFragmentActivity implements XListView.XListViewListener {

    @ViewInject(R.id.multiStateView)
    MultiStateView multiStateView ;

    @ViewInject(R.id.xlist_campaign)
    XListView xListView ;

    ArrayList<YuyueListItemPojo> list = new ArrayList<>() ;

    private boolean hasMoreData = true ;
    private int pageIndex = 1 ;

    ListAdapter adapter;

    @ViewInject(R.id.te3xtView5)
    EditText search ;

    @Override
    protected void start() {
        super.start();
        StatusBarCompat.setStatusBarColor(FicSubscribe.this, getResources().getColor(R.color.app_green));
        actionBar.hide();


        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        xListView.setPullLoadEnable(true);
        xListView.setPullRefreshEnable(true);
        xListView.setHeaderProgressDrawable(ContextCompat.getDrawable(FicSubscribe.this,R.mipmap.default_ptr_rotate));
        xListView.setRefreshTime();
        xListView.setPULL_LOAD_MORE_DELTA(60);
        xListView.setSCROLL_DURATION(400);
        xListView.setOFFSET_RADIO(3.0f);
        xListView.setXListViewListener(this);

        adapter =new ListAdapter(FicSubscribe.this);
        adapter.setList(list);
        xListView.setAdapter(adapter);

        findViewById(R.id.finish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getData();
    }

    @Override
    public void onRefresh() {
        list.clear();
        pageIndex = 1 ;
        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        adapter.notifyDataSetChanged();
        getData();
    }

    @Override
    public void onLoadMore() {
        if (hasMoreData) {
            getData() ;
        }else {
            xListView.hideFooter();
        }
    }

    private void getData() {

        HashMap<String,String> map = new HashMap<>() ;
        map.put("type","1"); //实体2  虚拟 1
        map.put("page",String.valueOf(pageIndex));
        map.put("list_rows",Const.list_rows+"");

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminhouseyuyue,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LogUtil.e(TAG,"adminhouseyuyue  onFailure");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        multiStateView.setViewState(MultiStateView.VIEW_STATE_ERROR);
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                LogUtil.e(TAG,"adminhouseyuyue---"+rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);

                    final int total = BaseHttp.getJsonDataAsInt(s,"total") ;
                    int per_page = BaseHttp.getJsonDataAsInt(s,"per_page") ;
                    final int current_page = BaseHttp.getJsonDataAsInt(s,"current_page") ;
                    int last_page = BaseHttp.getJsonDataAsInt(s,"last_page") ;

                    s =BaseHttp.getJsonDataAsString(s);

                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<YuyueListItemPojo>>(){}.getType();
                    List<YuyueListItemPojo> projectList = gson.fromJson(s, listType);

                    list.addAll(projectList);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(total==0){
                                multiStateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
                            }else {
                                adapter.notifyDataSetChanged();
                                multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);

                                xListView.setRefreshTime();
                                xListView.stopLoadMore();
                                xListView.stopRefresh();

                                if(list.size()<total){
                                    hasMoreData = true ;
                                    pageIndex = current_page+1 ;
                                }else {
                                    hasMoreData = false ;
                                }

                                if (hasMoreData) {
                                    xListView.showFooter();
                                } else {
                                    xListView.hideFooter();
                                }
                            }
                        }
                    });


                }else {
                    showMsg(BaseHttp.getJsonMsg(rr));
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                            multiStateView.setViewState(MultiStateView.VIEW_STATE_ERROR);

                            xListView.setRefreshTime();
                            xListView.stopLoadMore();
                            xListView.stopRefresh();
                        }
                    });
                }

            }
        });

    }
}
