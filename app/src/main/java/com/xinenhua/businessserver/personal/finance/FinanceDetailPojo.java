package com.xinenhua.businessserver.personal.finance;

/**
 * Created by oasis on 2019/5/8.
 */

public class FinanceDetailPojo {

    private String username;

    private String qishu;

    private String fukuanfangshi;

    private String price;

    private int zhangdan_status;

    private String huikuanjine;

    private String dingdanhao;

    private String image;

    private String title;

    private String qixian;

    private String housenum ;

    private String zhangdan_id;

    public String getZhangdan_id() {
        return zhangdan_id;
    }

    public void setZhangdan_id(String zhangdan_id) {
        this.zhangdan_id = zhangdan_id;
    }

    public String getHousenum() {
        return housenum;
    }

    public void setHousenum(String housenum) {
        this.housenum = housenum;
    }

    public void setUsername(String username){
        this.username = username;
    }
    public String getUsername(){
        return this.username;
    }
    public void setQishu(String qishu){
        this.qishu = qishu;
    }
    public String getQishu(){
        return this.qishu;
    }
    public void setFukuanfangshi(String fukuanfangshi){
        this.fukuanfangshi = fukuanfangshi;
    }
    public String getFukuanfangshi(){
        return this.fukuanfangshi;
    }
    public void setPrice(String price){
        this.price = price;
    }
    public String getPrice(){
        return this.price;
    }
    public void setZhangdan_status(int zhangdan_status){
        this.zhangdan_status = zhangdan_status;
    }
    public int getZhangdan_status(){
        return this.zhangdan_status;
    }
    public void setHuikuanjine(String huikuanjine){
        this.huikuanjine = huikuanjine;
    }
    public String getHuikuanjine(){
        return this.huikuanjine;
    }
    public void setDingdanhao(String dingdanhao){
        this.dingdanhao = dingdanhao;
    }
    public String getDingdanhao(){
        return this.dingdanhao;
    }
    public void setImage(String image){
        this.image = image;
    }
    public String getImage(){
        return this.image;
    }
    public void setTitle(String title){
        this.title = title;
    }
    public String getTitle(){
        return this.title;
    }
    public void setQixian(String qixian){
        this.qixian = qixian;
    }
    public String getQixian(){
        return this.qixian;
    }
}
