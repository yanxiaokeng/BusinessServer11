package com.xinenhua.businessserver.personal;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.blankj.utilcode.util.Utils;
import com.githang.statusbar.StatusBarCompat;
import com.google.gson.Gson;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.DialogUtils;
import com.oasis.oasislib.utils.LogUtil;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.login.Login;
import com.yanzhenjie.permission.Action;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.Permission;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.List;

/**
 * 个人中心 设置
 */
@ContentView(R.layout.kefu)
public class Setting extends BaseFragmentActivity {

    @ViewInject(R.id.text11View19)
    TextView version ;

    @ViewInject(R.id.text31View19)
    TextView loginout ;
    @Override
    protected void start() {
        super.start();
        StatusBarCompat.setStatusBarColor(Setting.this, getResources().getColor(R.color.app_green));
        actionBar.hide();


        version.setText("版本号："+getAppVersionName(context));


        loginout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.e(TAG,"退出------");
                final AlertDialog.Builder confirmDialog = DialogUtils.getConfirmDialog(context, "确认退出？", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //清空用户信息
                        SPUtils spUtils = SPUtils.getInstance();
                        spUtils.put("user","");
                        spUtils.put("phone","");
                        spUtils.put("psw","");
                        //回到登录页面
                        //方案就是跳转的时候清除当前这个栈，在从新建一个栈，这个新的登录界面就在这个新的栈里打开
                        Intent intent = new Intent(Setting.this, Login.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                Dialog dialog = confirmDialog.show();
            }
        });
    }


    public static String getAppVersionName(Context context) {
        String versionName = "";
        try {
            // ---get the package info---
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            versionName = pi.versionName;
            //versioncode = pi.versionCode;
            if (versionName == null || versionName.length() <= 0) {
                return "";
            }
        } catch (Exception e) {
            Log.e("VersionInfo", "Exception", e);
        }
        return versionName;
    }


}
