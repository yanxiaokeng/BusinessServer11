package com.xinenhua.businessserver.personal.finance;

/**
 * Created by oasis on 2019/3/11.
 */

public class FinanceListPojo {

    private String user;
    private String conpany;
    private String phone ;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getConpany() {
        return conpany;
    }

    public void setConpany(String conpany) {
        this.conpany = conpany;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
