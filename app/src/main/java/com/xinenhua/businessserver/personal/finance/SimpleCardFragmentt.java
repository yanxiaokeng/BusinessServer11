package com.xinenhua.businessserver.personal.finance;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.AdapterView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kennyc.view.MultiStateView;
import com.oasis.oasislib.base.BaseFragment;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.ToastUtils;
import com.oasis.oasislib.view.XListView;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.bill.AccountListPojo;
import com.xinenhua.businessserver.http.BaseHttp;
import com.xinenhua.businessserver.login.pojo.User;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * 个人中心 到账确认  三个fragment
 */
@ContentView(R.layout.list)
public class SimpleCardFragmentt extends BaseFragment implements XListView.XListViewListener{

    @ViewInject(R.id.multiStateView)
    MultiStateView multiStateView ;

    @ViewInject(R.id.xlist_campaign)
    XListView xListView ;

    String whichType;

    FinanceAdapter adapter;

    ArrayList<AccountListPojo> list = new ArrayList<>() ;

    private boolean hasMoreData = true ;

    private int pageIndex = 1 ;

    User user;


    //执行刷新操作
    public void refresh(){
        onRefresh();
    }


    public static SimpleCardFragmentt getInstance(String whichType) {
        SimpleCardFragmentt sf = new SimpleCardFragmentt();
        sf.whichType = whichType;
        return sf;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        user = Const.getUser();
        if(user!=null){
            LogUtil.e(TAG,user.toString());
        }else {
            LogUtil.e(TAG,"user null");
        }
    }

    @Override
    protected void start() {

        LogUtil.e(TAG,"SimpleCardFragmentt----whichType" + whichType);
        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        xListView.setPullLoadEnable(true);
        xListView.setPullRefreshEnable(true);
        xListView.setHeaderProgressDrawable(ContextCompat.getDrawable(getActivity(),R.mipmap.default_ptr_rotate));
        xListView.setRefreshTime();
        xListView.setPULL_LOAD_MORE_DELTA(60);
        xListView.setSCROLL_DURATION(400);
        xListView.setOFFSET_RADIO(3.0f);
        xListView.setXListViewListener(this);

        adapter = new FinanceAdapter(getActivity()) ;
        adapter.setList(list);
        xListView.setAdapter(adapter);
        xListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent();

                Bundle bundle=new Bundle();
                bundle.putString("id",String.valueOf(list.get(position-1).getId()));
                intent.putExtras(bundle);

                intent.setClass(getActivity(), FinanceDetail.class);
                startActivity(intent);

            }
        });

        getData();


    }


    @Override
    public void onRefresh() {
        list.clear();
        pageIndex = 1 ;
        multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        adapter.notifyDataSetChanged();
        getData();
    }

    @Override
    public void onLoadMore() {
        if (hasMoreData) {
            getData() ;
        }else {
            xListView.hideFooter();
        }
    }


    private void getData() {

        HashMap<String,String> map = new HashMap<>() ;
        map.put("list_rows",String.valueOf(Const.list_rows));
        map.put("page",String.valueOf(pageIndex));
        map.put("zhangdan_status",whichType);

        Call call = BaseHttp.getCallWithUrlAndData(Const.ServicezhangdanList,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        multiStateView.setViewState(MultiStateView.VIEW_STATE_ERROR);
                    }
                });

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                LogUtil.e(TAG,rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);

                    final int total = BaseHttp.getJsonDataAsInt(s,"total") ;
                    int per_page = BaseHttp.getJsonDataAsInt(s,"per_page") ;
                    int current_page = BaseHttp.getJsonDataAsInt(s,"current_page") ;
                    int last_page = BaseHttp.getJsonDataAsInt(s,"last_page") ;


                    String ssss = BaseHttp.getJsonDataAsString(s);
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<AccountListPojo>>(){}.getType();
                    List<AccountListPojo> projectList = gson.fromJson(ssss, listType);

                    list.addAll(projectList);

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);

                            xListView.setRefreshTime();
                            xListView.stopLoadMore();
                            xListView.stopRefresh();


                            if(list.size()<total){
                                hasMoreData = true ;
                            }else {
                                hasMoreData = false ;
                            }

                            if (hasMoreData) {
                                xListView.showFooter();
                            } else {
                                xListView.hideFooter();
                            }
                        }
                    });


                }else {
                    ToastUtils.shortToast(getActivity(),BaseHttp.getJsonMsg(rr));

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                            multiStateView.setViewState(MultiStateView.VIEW_STATE_ERROR);

                            xListView.setRefreshTime();
                            xListView.stopLoadMore();
                            xListView.stopRefresh();
                        }
                    });
                }

            }
        });

    }






}
