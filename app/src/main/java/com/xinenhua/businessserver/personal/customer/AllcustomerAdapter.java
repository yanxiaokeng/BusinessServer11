package com.xinenhua.businessserver.personal.customer;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasis.oasislib.base.AppBaseAdapter;
import com.oasis.oasislib.utils.LogUtil;
import com.xinenhua.businessserver.R;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

/**
 * 個人中心 注冊用戶  列表  适配器
 */

public class AllcustomerAdapter extends AppBaseAdapter<AllCustomerPojo> {


    public AllcustomerAdapter(Activity context) {
        super(context);
    }


    private Context context;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AllcustomerAdapter.ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.allcustomeritem, parent, false);
            holder = new AllcustomerAdapter.ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (AllcustomerAdapter.ViewHolder) convertView.getTag();
        }
        final AllCustomerPojo t = list.get(position) ;

        LogUtil.e("!!!!!!!!!",t.toString());

        holder.name.setText(t.getCustom_name());
        holder.date.setText(t.getCreate_time());


        return convertView;
    }

    static class ViewHolder {

        @ViewInject(R.id.name)
        TextView name ;

        @ViewInject(R.id.date)
        TextView date;

        public ViewHolder(View itemView) {
            x.view().inject(this, itemView);
        }
    }

}
