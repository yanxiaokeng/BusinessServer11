package com.xinenhua.businessserver.personal.order;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasis.oasislib.base.AppBaseAdapter;
import com.oasis.oasislib.utils.LogUtil;
import com.xinenhua.businessserver.R;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

/**
 *
 */

public class OrderAdapter extends AppBaseAdapter<OrderListPojo> {


    public OrderAdapter(Activity context) {
        super(context);
    }


    private Context context;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OrderAdapter.ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.order_item, parent, false);
            holder = new OrderAdapter.ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (OrderAdapter.ViewHolder) convertView.getTag();
        }
        final OrderListPojo t = list.get(position) ;

        LogUtil.e("!!!!!!!!!",t.toString());

        holder.user.setText(t.getNickname());
        holder.company.setText(t.getCustom_name());
        holder.phone.setText(t.getPhone());


        return convertView;
    }

    static class ViewHolder {


        @ViewInject(R.id.textView19)
        TextView user ;

        @ViewInject(R.id.textView22)
        TextView company;

        @ViewInject(R.id.textView23)
        TextView phone ;



        public ViewHolder(View itemView) {
            x.view().inject(this, itemView);
        }
    }

}
