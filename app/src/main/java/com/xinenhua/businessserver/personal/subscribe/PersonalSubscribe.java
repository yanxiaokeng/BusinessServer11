package com.xinenhua.businessserver.personal.subscribe;

import android.view.View;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.ActivityUtils;
import com.githang.statusbar.StatusBarCompat;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.xinenhua.businessserver.R;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

/**
 * 个人中心  客户预约
 */
@ContentView(R.layout.personalsubscribe)
public class PersonalSubscribe extends BaseFragmentActivity {


    @ViewInject(R.id.fic)
    RelativeLayout entity;

    @ViewInject(R.id.entity)
    RelativeLayout fic;

    @Override
    protected void start() {
        super.start();
        StatusBarCompat.setStatusBarColor(PersonalSubscribe.this, getResources().getColor(R.color.app_green));
        actionBar.hide();

        fic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(PersonalSubscribe.this,FicSubscribe.class);
            }
        });

        entity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(PersonalSubscribe.this,EntitySubscribe.class);
            }
        });

        findViewById(R.id.warpper_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
