package com.xinenhua.businessserver.personal.customer;

/**
 * Created by oasis on 2019/4/3.
 */

public class AllCustomerPojo {
    private int id;

    private String custom_name;

    private String mobile;

    private String create_time;

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setCustom_name(String custom_name){
        this.custom_name = custom_name;
    }
    public String getCustom_name(){
        return this.custom_name;
    }
    public void setMobile(String mobile){
        this.mobile = mobile;
    }
    public String getMobile(){
        return this.mobile;
    }
    public void setCreate_time(String create_time){
        this.create_time = create_time;
    }
    public String getCreate_time(){
        return this.create_time;
    }

    @Override
    public String toString() {
        return "AllCustomerPojo{" +
                "id=" + id +
                ", custom_name='" + custom_name + '\'' +
                ", mobile='" + mobile + '\'' +
                ", create_time='" + create_time + '\'' +
                '}';
    }
}
