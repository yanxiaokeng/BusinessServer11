package com.xinenhua.businessserver.personal;

import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.SPUtils;
import com.githang.statusbar.StatusBarCompat;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.CountDownUtil;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.ToastUtils;
import com.oasis.oasislib.view.PhoneCode;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.MainActivity;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.http.BaseHttp;
import com.xinenhua.businessserver.login.Login;
import com.xinenhua.businessserver.login.PositionEnum;
import com.xinenhua.businessserver.login.SetPsw;
import com.xinenhua.businessserver.login.pojo.User;
import com.xinenhua.businessserver.pojo.ProjectPojo;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * 更换手机号码 输入验证码
 */
@ContentView(R.layout.register2)
public class ChangePhoneSign extends BaseFragmentActivity {

    @ViewInject(R.id.textView7)
    TextView textView7;

    @ViewInject(R.id.pc_1)
    PhoneCode pc_1;

    @ViewInject(R.id.textView4)
    TextView yanzhengtextView4;

    //电话号码
    String phonenum;

    //服务器接收到的养正吗
    String servernum;

    //当前登录用户
    User user;

    //新的号码
    String newphone ;

    @Override
    protected void start() {
        super.start();
        mTitleBar.setCenterTitle("");
        mTitleBar.setBackViewVisiable(true);
        mTitleBar.setVisibility(View.GONE);
        StatusBarCompat.setStatusBarColor(this, Color.WHITE);
        actionBar.hide();

        user = Const.getUser();
        phonenum = user.getPhone();

        newphone = getIntent().getStringExtra("newphone") ;

        LogUtil.e(TAG,"getold phone---"+phonenum);
        LogUtil.e(TAG,"getnew phone---"+newphone);

        getCode();


        pc_1 = (PhoneCode) findViewById(R.id.pc_1);
        //注册事件回调（根据实际需要，可写，可不写）
        pc_1.setOnInputListener(new PhoneCode.OnInputListener() {
            @Override
            public void onSucess(String code) {
                //TODO: 例如底部【下一步】按钮可点击

                LogUtil.e(TAG,"pc_1  onSucess"+code);

               if(code.equals(servernum)){
                   //验证码比对成功了  可以修改手机号码了！！！！
                    doChangePhone(servernum,phonenum,newphone);
                }else {
                    ToastUtils.shortToast(context,"验证码错误，请重新输入");
                }
            }

            @Override
            public void onInput() {
                //TODO:例如底部【下一步】按钮不可点击
                LogUtil.e(TAG,"pc_1  onInput");
            }
        });


        yanzhengtextView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CountDownUtil(yanzhengtextView4)
                        .setCountDownMillis(20_000L)//倒计时60000ms
                        .setCountDownColor(R.color.app_green,R.color.app_phone_gray)//不同状态字体颜色
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                getCode();
                            }
                        })
                        .start();
            }
        });


        new CountDownUtil(yanzhengtextView4)
                .setCountDownMillis(20_000L)//倒计时60000ms
                .setCountDownColor(R.color.app_green,R.color.app_phone_gray)//不同状态字体颜色
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                      getCode();
                    }
                })
                .start();

    }

    private void doChangePhone(String sign,String phone,String newphone) {

        HashMap<String,String> map = new HashMap<>() ;
        map.put("phone",phone);
        map.put("newphone",newphone);
        map.put("sign",sign);

        Call call = BaseHttp.getCallWithUrlAndData(Const.admineditPhone,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");
                ToastUtils.shortToast(context,"修改失败");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    LogUtil.e(TAG,"String--"+s);
                    //得到了用户信息
                    Gson gson = new Gson();
                    User user = gson.fromJson(s, User.class);

                    //存储用户信息
                    SPUtils spUtils = SPUtils.getInstance();
                    spUtils.put("user",s);

                    System.out.println("------------------------------------------");
                    showMsg("修改成功");

                    Intent intent = new Intent(ChangePhoneSign.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                    ToastUtils.shortToast(context,BaseHttp.getJsonMsg(rr));
                }

            }
        });

    }

    //获取验证码
    private void getCode(){
        //11是注册 12是找回密码 13更换手机号
        HashMap<String,String> map = new HashMap<>() ;
        map.put("phone",newphone);
        map.put("type","13");
        map.put("ip","192.168.0.1");

        Call call = BaseHttp.getCallWithUrlAndData(Const.admincode,map);
        call.enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");
                showMsg("发生错误 请重试");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                LogUtil.e(TAG,rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    JsonParser parser = new JsonParser();
                    JsonObject asJsonObject = parser.parse(s).getAsJsonObject();
                    String sign = asJsonObject.get("sign").getAsString();
                    //从服务器端获取得到的验证码
                    LogUtil.e(TAG,sign);
                    servernum = sign;
                    showMsg("验证码发送成功");
                }else {
                    showMsg(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }


    @Event(value = R.id.textView7, type = View.OnClickListener.class)
    private void getyanzheng(View view) {
        change_sign_button_state();
    }

    private void change_sign_button_state(){
        new CountDownUtil(textView7)
                .setCountDownMillis(60_000L)//倒计时60000ms
                .setCountDownColor(R.color.app_gloden,android.R.color.darker_gray)//不同状态字体颜色
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getyanzheng(textView7);
                    }
                })
                .start();
    }



    @Event(value = R.id.imageView8, type = View.OnClickListener.class)
    private void finish(View view) {
        finish();
    }
}
