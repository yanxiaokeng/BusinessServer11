package com.xinenhua.businessserver.personal.order;

/**
 * 签约详情 ------缴费单
 */

public class Jiaofeidan {
    private int zhangdan_id;

    private String qishu;

    private String begin_time;

    private String finsh_time;

    private int zhangdan_status;

    public void setZhangdan_id(int zhangdan_id){
        this.zhangdan_id = zhangdan_id;
    }
    public int getZhangdan_id(){
        return this.zhangdan_id;
    }
    public void setQishu(String qishu){
        this.qishu = qishu;
    }
    public String getQishu(){
        return this.qishu;
    }
    public void setBegin_time(String begin_time){
        this.begin_time = begin_time;
    }
    public String getBegin_time(){
        return this.begin_time;
    }
    public void setFinsh_time(String finsh_time){
        this.finsh_time = finsh_time;
    }
    public String getFinsh_time(){
        return this.finsh_time;
    }
    public void setZhangdan_status(int zhangdan_status){
        this.zhangdan_status = zhangdan_status;
    }
    public int getZhangdan_status(){
        return this.zhangdan_status;
    }
}
