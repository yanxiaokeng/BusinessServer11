package com.xinenhua.businessserver.personal;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ActivityUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.oasis.oasislib.utils.LogUtil;
import com.oasis.oasislib.utils.ToastUtils;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.Holder;
import com.orhanobut.dialogplus.ViewHolder;
import com.xinenhua.businessserver.Const;
import com.xinenhua.businessserver.R;
import com.xinenhua.businessserver.http.BaseHttp;
import com.xinenhua.businessserver.login.pojo.User;

import org.apache.commons.lang3.StringUtils;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * 個人中心  个人资料  修改手机号真正的操作
 */
@ContentView(R.layout.changephonereal)
public class ChangePhoneReal extends BaseFragmentActivity {

    @ViewInject(R.id.textView7)
    TextView textView7;

    @ViewInject(R.id.imageButton)
    RelativeLayout imageButton;

    //当前手机号码
    @ViewInject(R.id.showphone)
    TextView showphone;

    //输入即将修改的手机号码
    @ViewInject(R.id.XEditText)
    EditText inputphone ;

    //当前登录用户
    User user;

    @ViewInject(R.id.nextrelative)
    RelativeLayout nextrelative;

    @Override
    protected void start() {
       actionBar.hide();

        user = Const.getUser();
        showphone.setText("当前手机号："+user.getPhone());

        nextrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(StringUtils.isBlank(inputphone.getText().toString())){
                    ToastUtils.shortToast(context,"请输入新手机号");
                    return;
                }

                Intent openWelcomeActivityIntent=new Intent();
                Bundle bundle=new Bundle();
                bundle.putString("newphone",inputphone.getText().toString());

                openWelcomeActivityIntent.putExtras(bundle);
                openWelcomeActivityIntent.setClass(ChangePhoneReal.this, ChangePhoneSign.class);
                startActivity(openWelcomeActivityIntent);

               // ActivityUtils.startActivity(ChangePhoneSign.class);
            }
        });


    }

}
