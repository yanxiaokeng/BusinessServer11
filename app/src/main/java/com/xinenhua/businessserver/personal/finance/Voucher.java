package com.xinenhua.businessserver.personal.finance;

/**
 * 服务端查看支付凭证  pojo
 */
public class Voucher {
    private int id;

    private String dingdanhao;

    private String huikuanjine;

    private String fukuanfangshi;

    private int zhangdan_status;

    private String images;

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setDingdanhao(String dingdanhao){
        this.dingdanhao = dingdanhao;
    }
    public String getDingdanhao(){
        return this.dingdanhao;
    }
    public void setHuikuanjine(String huikuanjine){
        this.huikuanjine = huikuanjine;
    }
    public String getHuikuanjine(){
        return this.huikuanjine;
    }
    public void setFukuanfangshi(String fukuanfangshi){
        this.fukuanfangshi = fukuanfangshi;
    }
    public String getFukuanfangshi(){
        return this.fukuanfangshi;
    }
    public void setZhangdan_status(int zhangdan_status){
        this.zhangdan_status = zhangdan_status;
    }
    public int getZhangdan_status(){
        return this.zhangdan_status;
    }
    public void setImages(String images){
        this.images = images;
    }
    public String getImages(){
        return this.images;
    }
}
