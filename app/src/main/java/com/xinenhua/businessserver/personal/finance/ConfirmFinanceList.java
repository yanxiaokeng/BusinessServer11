package com.xinenhua.businessserver.personal.finance;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.githang.statusbar.StatusBarCompat;
import com.oasis.oasislib.base.BaseFragmentActivity;
import com.xinenhua.businessserver.R;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import java.util.ArrayList;

/**
 * 个人中心---到账确认列表 （这个到账确认 是财务用的  ）
 */
@ContentView(R.layout.confirmfinancelist)
public class ConfirmFinanceList  extends BaseFragmentActivity implements OnTabSelectListener {

    private ArrayList<SimpleCardFragmentt> mFragments = new ArrayList<>();

    private final String[] mTitles = {
            "全部", "待确认", "已确认"
    };

    //（5：已确认 4：待确认  "",为空显示全部）

    @ViewInject(R.id.tl_3)
    SlidingTabLayout tabLayout_3 ;

    @ViewInject(R.id.vp)
    ViewPager vp;

    private ConfirmFinanceList.MyPagerAdapter mAdapter;

    //执行刷新操作！
    public void refresh(){

        for(SimpleCardFragmentt item:mFragments){
            item.onRefresh();
        }
    }

    @Override
    protected void start() {
        super.start();
        actionBar.hide();
        StatusBarCompat.setStatusBarColor(ConfirmFinanceList.this, getResources().getColor(R.color.app_green));

        //初始化 fragment
        mFragments.add(SimpleCardFragmentt.getInstance(""));
        mFragments.add(SimpleCardFragmentt.getInstance("4"));
        mFragments.add(SimpleCardFragmentt.getInstance("5"));

        mAdapter = new ConfirmFinanceList.MyPagerAdapter(getSupportFragmentManager());
        vp.setAdapter(mAdapter);
        vp.setOffscreenPageLimit(5);
        tabLayout_3.setViewPager(vp);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onTabSelect(int position) {

    }

    @Override
    public void onTabReselect(int position) {

    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mTitles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }


}
