package com.xinenhua.businessserver.personal.order;

import java.util.List;

/**
 *  我的签约详情
 */
public class OrderDetail {

    private int id;

    private String dingdanhao;

    private String mingcheng;

    private String weizhi;

    private String phone;

    private String qizhi;

    private String fukuanfangshi;

    private List<Jiaofeidan> jiaofeidan ;

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setDingdanhao(String dingdanhao){
        this.dingdanhao = dingdanhao;
    }
    public String getDingdanhao(){
        return this.dingdanhao;
    }
    public void setMingcheng(String mingcheng){
        this.mingcheng = mingcheng;
    }
    public String getMingcheng(){
        return this.mingcheng;
    }
    public void setWeizhi(String weizhi){
        this.weizhi = weizhi;
    }
    public String getWeizhi(){
        return this.weizhi;
    }
    public void setPhone(String phone){
        this.phone = phone;
    }
    public String getPhone(){
        return this.phone;
    }
    public void setQizhi(String qizhi){
        this.qizhi = qizhi;
    }
    public String getQizhi(){
        return this.qizhi;
    }
    public void setFukuanfangshi(String fukuanfangshi){
        this.fukuanfangshi = fukuanfangshi;
    }
    public String getFukuanfangshi(){
        return this.fukuanfangshi;
    }
    public void setJiaofeidan(List<Jiaofeidan> jiaofeidan){
        this.jiaofeidan = jiaofeidan;
    }
    public List<Jiaofeidan> getJiaofeidan(){
        return this.jiaofeidan;
    }
}
