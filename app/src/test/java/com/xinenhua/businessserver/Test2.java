package com.xinenhua.businessserver;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.xinenhua.businessserver.bill.AccountListPojo;
import com.xinenhua.businessserver.buildd.HotBuildingPojo;
import com.xinenhua.businessserver.entity.YuyueDetail;
import com.xinenhua.businessserver.entity.YuyueListItemPojo;
import com.xinenhua.businessserver.house.HouseDetailPojo;
import com.xinenhua.businessserver.house.ListPojo;
import com.xinenhua.businessserver.house.SelectFilter;
import com.xinenhua.businessserver.http.BaseHttp;
import com.xinenhua.businessserver.login.pojo.User;
import com.xinenhua.businessserver.personal.customer.AllCustomerPojo;
import com.xinenhua.businessserver.personal.finance.FinanceDetailPojo;
import com.xinenhua.businessserver.personal.finance.Voucher;
import com.xinenhua.businessserver.personal.order.OrderDetail;
import com.xinenhua.businessserver.pojo.CustomerDetail;
import com.xinenhua.businessserver.pojo.CustomerInfo;
import com.xinenhua.businessserver.pojo.ProjectPojo;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by oasis on 2019/4/17.
 */

public class Test2 {


    //我的签约
    public static void Servicecontract(){

        HashMap<String,String> map = new HashMap<>() ;
        map.put("list_rows",String.valueOf(Const.list_rows));
        map.put("page","1");
        map.put("type","2"); //1实体 2虚拟
        map.put("zhangdan_status","");//状态：为空显示全部；2 已生成待上传缴费 3 待业务审核 4 待财务审核 5 完成
        map.put("admin_id","26");

        Call call = BaseHttp.getCallWithUrlAndData(Const.Servicecontract,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);

                    int total = BaseHttp.getJsonDataAsInt(s,"total") ;
                    int per_page = BaseHttp.getJsonDataAsInt(s,"per_page") ;
                    int current_page = BaseHttp.getJsonDataAsInt(s,"current_page") ;
                    int last_page = BaseHttp.getJsonDataAsInt(s,"last_page") ;


                    String ssss = BaseHttp.getJsonDataAsString(s);


                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<AllCustomerPojo>>(){}.getType();
                    List<AllCustomerPojo> customerlist = gson.fromJson(ssss, listType);

                    System.out.println(s);
                    System.out.println("------------------------------------------");
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }


    //首页轮播图
    public static void banner(){

        Call call = BaseHttp.getCallWithUrlAndData(Const.servicebanner,null);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);

                    JsonParser parser = new JsonParser();
                    JsonObject jsonObject = parser.parse(s).getAsJsonObject();

                    JsonArray jsonArray = jsonObject.get("service").getAsJsonArray();

                    Iterator<JsonElement> iterator = jsonArray.iterator();
                    while (iterator.hasNext()){
                        JsonElement next = iterator.next();
                        JsonObject jo = next.getAsJsonObject();
                        String imageurl = jo.get("imageurl").getAsString();
                        String title = jo.get("title").getAsString();


                    }


                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }


    //查看密码是否正确1
    public static void updateImg(){
        HashMap<String,String> map = new HashMap<>() ;
        map.put("admin_id","15822838393");
        map.put("image","12345678");

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminupdateImg,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);

                    JsonParser parser = new JsonParser();
                    JsonArray jsonArray = parser.parse(s).getAsJsonArray();


                    String finalr = jsonArray.get(0).getAsJsonObject().get("code").getAsString();

                    System.out.println("String--"+finalr);

                    System.out.println("------------------------------------------");
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }



    //查看密码是否正确1
    public static void verifyPassword(){
        HashMap<String,String> map = new HashMap<>() ;
        map.put("phone","15822838393");
        map.put("password","12345678");

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminverifyPassword,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);

                    JsonParser parser = new JsonParser();
                    JsonArray jsonArray = parser.parse(s).getAsJsonArray();


                    String finalr = jsonArray.get(0).getAsJsonObject().get("code").getAsString();

                    System.out.println("String--"+finalr);

                    System.out.println("------------------------------------------");
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }


    //服务端修改手机号
    public static void editPhone(){
        HashMap<String,String> map = new HashMap<>() ;
        map.put("phone","15822838393");
        map.put("newphone","15222222222");
        map.put("sign","2116");

        Call call = BaseHttp.getCallWithUrlAndData(Const.admineditPhone,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    System.out.println("String--"+s);

                    System.out.println("------------------------------------------");
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }

    //服务端修改姓名
    public static void admineditInfo(){
        HashMap<String,String> map = new HashMap<>() ;
        map.put("admin_id","29");
        map.put("nickname","nickname");

        Call call = BaseHttp.getCallWithUrlAndData(Const.admineditInfo,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    System.out.println("String--"+s);

                    System.out.println("------------------------------------------");
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }



    //热门楼盘详情  大厦详情
    public static void adminhousebuildDetail(){

        HashMap<String,String> map = new HashMap<>() ;
        map.put("build_id","4");

        Call call = BaseHttp.getCallWithUrlAndData(Const.buildDetail,null);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    s = BaseHttp.getJsonDataAsString(s);

//                    Gson gson = new Gson();
//                    Type listType = new TypeToken<List<HotBuildingPojo>>(){}.getType();
//                    List<HotBuildingPojo> list = gson.fromJson(s, listType);

                    System.out.println();
                    System.out.println("------------------------------------------");
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }



    //热门楼盘
    public static void commonbuild(){


        Call call = BaseHttp.getCallWithUrlAndData(Const.commonbuild,null);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    s = BaseHttp.getJsonDataAsString(s);

                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<HotBuildingPojo>>(){}.getType();
                    List<HotBuildingPojo> list = gson.fromJson(s, listType);

                    System.out.println(list);
                    System.out.println("------------------------------------------");
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }



    //服务端实体/虚拟客户资料详情
    public static void ServicememberDetail(){

        HashMap<String,String> map = new HashMap<>() ;
        map.put("hetong_id","71");

        Call call = BaseHttp.getCallWithUrlAndData(Const.ServicememberDetail,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);



                    Gson gson = new Gson();
                    Type listType = new TypeToken<CustomerDetail>(){}.getType();
                    CustomerDetail customerDetail = gson.fromJson(s, listType);

                    System.out.println(customerDetail);
                    System.out.println("------------------------------------------");
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }


    //服务端注册用户信息
    public static void adminhousemember(){

        HashMap<String,String> map = new HashMap<>() ;
        map.put("list_rows","15");
        map.put("page","1");
        map.put("custom_key","");

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminhousemember,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);

                    int total = BaseHttp.getJsonDataAsInt(s,"total") ;
                    int per_page = BaseHttp.getJsonDataAsInt(s,"per_page") ;
                    int current_page = BaseHttp.getJsonDataAsInt(s,"current_page") ;
                    int last_page = BaseHttp.getJsonDataAsInt(s,"last_page") ;


                    String ssss = BaseHttp.getJsonDataAsString(s);


                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<AllCustomerPojo>>(){}.getType();
                    List<AllCustomerPojo> customerlist = gson.fromJson(ssss, listType);

                    System.out.println(s);
                    System.out.println("------------------------------------------");
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }


    //我的签约详情
    public static void contractDetails(){

        HashMap<String,String> map = new HashMap<>() ;
        map.put("dingdanhao","201905271235ceb896565d41");

        Call call = BaseHttp.getCallWithUrlAndData(Const.ServicecontractDetails,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);


                    Gson gson = new Gson();
                    Type listType = new TypeToken<OrderDetail>(){}.getType();
                    OrderDetail projectList = gson.fromJson(s, listType);

                    System.out.println(s);
                    System.out.println("------------------------------------------");
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }



    //服务端到账确认详情  （财务的功能  到账确认列表 ----  到账确认详情   ）
    public static void zhangdanDetail(){

        //对应的表是linjia_zhangdan 表

        HashMap<String,String> map = new HashMap<>() ;
        map.put("id","22");

        Call call = BaseHttp.getCallWithUrlAndData(Const.zhangdanDetail,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);

                    Gson gson = new Gson();
                    FinanceDetailPojo financeDetailPojo = gson.fromJson(s, FinanceDetailPojo.class) ;

                    System.out.println(s);
                    System.out.println("------------------------------------------");
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }


    //服务端预约列表
    public static void yuyuelist(){

        //（预约类型 1：虚拟 2：实体）

        HashMap<String,String> map = new HashMap<>() ;
        map.put("type","1");

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminhouseyuyue,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);


                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<YuyueListItemPojo>>(){}.getType();
                    List<YuyueListItemPojo> projectList = gson.fromJson(s, listType);

                    System.out.println(s);
                    System.out.println("------------------------------------------");
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }


    //服务端预约信息详情
    public static void yuyuedetail(){
        HashMap<String,String> map = new HashMap<>() ;
        map.put("yuyue_id","7");

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminhouseyuyuedetail,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    Gson gson = new Gson();
                    Type listType = new TypeToken<YuyueDetail>(){}.getType();
                    YuyueDetail detail = gson.fromJson(s, listType);

                    System.out.println("------------------------------------------");
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }


    //服务端修改密码
    public static void changepassword(){
        HashMap<String,String> map = new HashMap<>() ;
        map.put("phone","15822838393");
        map.put("password","12345678");
        map.put("sign","2116");

        Call call = BaseHttp.getCallWithUrlAndData(Const.changepassword,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    System.out.println("String--"+s);

                    System.out.println("------------------------------------------");
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }

    //服务端验证码请求
    public static void getsign(){

        //11是注册 12是找回密码 13更换手机号
        HashMap<String,String> map = new HashMap<>() ;
        map.put("phone","15822838393");
        map.put("type","12");
        map.put("ip","192.168.0.1");

        Call call = BaseHttp.getCallWithUrlAndData(Const.admincode,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    JsonParser parser = new JsonParser();
                    JsonObject asJsonObject = parser.parse(s).getAsJsonObject();
                    String sign = asJsonObject.get("sign").getAsString();
                    System.out.println(sign);
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }

    //登录
    public static void login(){
        //  查看linjia_admin 表   is_check 字段   0是待审核 1是审核通过  2是离职 3 审核未通过 审核状态

        HashMap<String,String> map = new HashMap<>() ;
        map.put("phone","15822838393");
        map.put("password","12345678");

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminlogin,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    Gson gson = new Gson();
                    Type listType = new TypeToken<User>(){}.getType();
                    User user = gson.fromJson(s, listType);

                    System.out.println(s);
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }


    //注册
    public static void register(){

        //把数据存入到了 linjia_admin 表

        //{"admin_id":24,"phone":"15822838393","is_check":0,"create_time":"2019-05-08 14:37:39","nickname":"15822838393","position":"4","project_id":4,"project":"创智",
        // "purview":{"gerenziliao":"error","kehuziliao":"error","daijiaozhangdan":"error","kehuyuyue":"error","wodeqianyue":"error","zhuceyonghu":"error","daozhangqueren":"error","querenhetong":"error"}}

        //project_id   数据 在 linjia_project 表里面
        //position  职位（固定数据：1：董事长 2： 项目经理 3：财务 4： 项目助理）；
        HashMap<String,String> map = new HashMap<>() ;
        map.put("phone","15822838393");
        map.put("password","123456");
        map.put("sign","7314");
        map.put("project_id","4");
        map.put("position","4");

        Call call = BaseHttp.getCallWithUrlAndData(Const.register,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);

                    Gson gson = new Gson();
                    Type listType = new TypeToken<User>(){}.getType();
                    User user = gson.fromJson(s, listType);

                    System.out.println(s);
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }


    //服务端查看支付凭证   通過id 查询了 ！！！！！！！！
    public static void adminhousevoucher(){
        //  查看linjia_zhangdan 表
        HashMap<String,String> map = new HashMap<>() ;
        map.put("zhangdan_id","34");//订单号

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminhousevoucher,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);

                    Gson g = new Gson();
                    Voucher voucher = g.fromJson(s, Voucher.class);

                    System.out.println(s);
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }




    //到帐确认列表 （财务的功能  到账确认列表 ）
    public static void zhangdanList(){
        HashMap<String,String> map = new HashMap<>() ;
        map.put("list_rows","10");
        map.put("page","1");
        map.put("zhangdan_status","");

        Call call = BaseHttp.getCallWithUrlAndData(Const.ServicezhangdanList,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
//                    JsonParser parser = new JsonParser();
//                    JsonObject asJsonObject = parser.parse(s).getAsJsonObject();
//                    String sign = asJsonObject.get("sign").getAsString();
                    int total = BaseHttp.getJsonDataAsInt(s,"total") ;
                    int per_page = BaseHttp.getJsonDataAsInt(s,"per_page") ;
                    int current_page = BaseHttp.getJsonDataAsInt(s,"current_page") ;
                    int last_page = BaseHttp.getJsonDataAsInt(s,"last_page") ;


                    String ssss = BaseHttp.getJsonDataAsString(s);
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<AccountListPojo>>(){}.getType();
                    List<AccountListPojo> projectList = gson.fromJson(ssss, listType);
                    System.out.println(Arrays.toString(projectList.toArray()));


                    System.out.println(s);
                    System.out.println(last_page);

                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }


    //服务端推送合同
    public static void servicehetongtui(){

        //当推送成功了  linjia_hetongtmp 里面的数据 就删除了

        HashMap<String,String> map = new HashMap<>() ;
        map.put("id","128");
        map.put("phone","18920712065");//将要推送给客户的那个手机号码
        map.put("admin_id","27");  //操作人id  在linjia_admin 那个表里面

        Call call = BaseHttp.getCallWithUrlAndData(Const.servicehetongtui,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
//                    JsonParser parser = new JsonParser();
//                    JsonObject asJsonObject = parser.parse(s).getAsJsonObject();
//                    String sign = asJsonObject.get("sign").getAsString();

                    System.out.println(s);
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }



    //服务端付款方式
    public static void fukuantype(){
        Call call = BaseHttp.getCallWithUrlAndData(Const.servicefukuantype,null);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
//                    JsonParser parser = new JsonParser();
//                    JsonObject asJsonObject = parser.parse(s).getAsJsonObject();
//                    String sign = asJsonObject.get("sign").getAsString();
                    //{"1":"老大3430建行","2":"姐4862建行","3":"公司微信","4":"公司支付宝","5":"恩华对公1944","6":"青林对公5445","7":"成功对公8121","8":"鼎力对公4300","9":"创智浦发基本户3972","10":"pos机","11":"创智建行一般户0727","12":"恩华财税"}
                    HashMap<String,String> resultmap = new HashMap<String, String>();


                    JsonParser parser = new JsonParser();
                    JsonElement jsonElement = parser.parse(s);
                    Set<Map.Entry<String, JsonElement>> entries = jsonElement.getAsJsonObject().entrySet();
                    Iterator<Map.Entry<String, JsonElement>> iterator = entries.iterator();
                    while (iterator.hasNext()){
                        Map.Entry<String, JsonElement> next = iterator.next();
                        String key = next.getKey();
                        String value = next.getValue().getAsString();

                        print(key+"-----"+value);
                        resultmap.put(key,value);
                    }



                    System.out.println(s);
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }


    //实体/虚拟所有客户资料信息
    public  static void ServicememberInfo(){
        HashMap<String,String> map = new HashMap<>() ;
        map.put("list_rows","10");
        map.put("page","1");
        map.put("type","1");
        map.put("key","");

        Call call = BaseHttp.getCallWithUrlAndData(Const.ServicememberInfo,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
//                    JsonParser parser = new JsonParser();
//                    JsonObject asJsonObject = parser.parse(s).getAsJsonObject();
//                    String sign = asJsonObject.get("sign").getAsString();
                    int total = BaseHttp.getJsonDataAsInt(s,"total") ;
                    int per_page = BaseHttp.getJsonDataAsInt(s,"per_page") ;
                    int current_page = BaseHttp.getJsonDataAsInt(s,"current_page") ;
                    int last_page = BaseHttp.getJsonDataAsInt(s,"last_page") ;
                    String jsonDataAsString = BaseHttp.getJsonDataAsString(s);

                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<CustomerInfo>>(){}.getType();
                    List<CustomerInfo> customerlist = gson.fromJson(jsonDataAsString, listType);



                    System.out.println(s);
                    System.out.println(last_page);

                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }


    //修改密码
    public void tttt(){
        HashMap<String,String> map = new HashMap<>() ;
        map.put("phone","15822838393");
        map.put("password","123456");
        map.put("sign","8273");

        Call call = BaseHttp.getCallWithUrlAndData(Const.changepassword,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
//                    JsonParser parser = new JsonParser();
//                    JsonObject asJsonObject = parser.parse(s).getAsJsonObject();
//                    String sign = asJsonObject.get("sign").getAsString();
                    System.out.println(s);
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });
    }








    //项目列表接口
    public void test1(){

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminproject,null);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<ProjectPojo>>(){}.getType();
                    List<ProjectPojo> projectList = gson.fromJson(s, listType);
                    System.out.println(Arrays.toString(projectList.toArray()));
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });

    }


    //服务端实体房源列表
    public static void adminhouseentity(){

        HashMap<String,String> map = new HashMap<>() ;
        map.put("house_id","1");
        map.put("page","1");
        map.put("build_id","");
        map.put("entity_status","");
        map.put("keyword","");

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminhouseentity,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    System.out.println("String--"+s);

                    int total = BaseHttp.getJsonDataAsInt(s,"total") ;
                    int per_page = BaseHttp.getJsonDataAsInt(s,"per_page") ;
                    int current_page = BaseHttp.getJsonDataAsInt(s,"current_page") ;
                    int last_page = BaseHttp.getJsonDataAsInt(s,"last_page") ;

                    String son = BaseHttp.getJsonDataAsString(s);

                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<ListPojo>>(){}.getType();
                    List<ListPojo> projectList = gson.fromJson(son, listType);
                    System.out.println(Arrays.toString(projectList.toArray()));
                    System.out.println("------------------------------------------");
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });

        try {
            Thread.sleep((BaseHttp.TIMEOUT_TIME_SECONDS+5)*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //服务端虚拟房源列表
    public static void adminhousefictitious(){

        HashMap<String,String> map = new HashMap<>() ;
        map.put("house_id","1");
        map.put("page","1");
        map.put("build_id","");
        map.put("entity_status","");
        map.put("keyword","");

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminhousefictitious,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    System.out.println("String--"+s);

                    int total = BaseHttp.getJsonDataAsInt(s,"total") ;
                    int per_page = BaseHttp.getJsonDataAsInt(s,"per_page") ;
                    int current_page = BaseHttp.getJsonDataAsInt(s,"current_page") ;
                    int last_page = BaseHttp.getJsonDataAsInt(s,"last_page") ;

                    String son = BaseHttp.getJsonDataAsString(s);

                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<ListPojo>>(){}.getType();
                    List<ListPojo> projectList = gson.fromJson(son, listType);
                    System.out.println(Arrays.toString(projectList.toArray()));
                    System.out.println("------------------------------------------");
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });

        try {
            Thread.sleep((BaseHttp.TIMEOUT_TIME_SECONDS+5)*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    //服务端列表筛选条件
    public static void adminfilte(){

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminfilte,null);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    System.out.println("String--"+s);

                    String column1 = BaseHttp.getJsonDataAsString(s,"column1") ;
                    String entity = BaseHttp.getJsonDataAsString(s,"entity") ;
                    String fictitious = BaseHttp.getJsonDataAsString(s,"fictitious") ;

                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<SelectFilter>>(){}.getType();
                    List<SelectFilter> column1list = gson.fromJson(column1, listType);

                    Type listType1 = new TypeToken<List<SelectFilter>>(){}.getType();
                    List<SelectFilter> entitylist = gson.fromJson(entity, listType1);

                    Type listType2 = new TypeToken<List<SelectFilter>>(){}.getType();
                    List<SelectFilter> fictitiouslist = gson.fromJson(fictitious, listType2);


                    System.out.println("------------------------------------------");
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });

    }


    //服务端房源详情
    public static void housedetail(){

        HashMap<String,String> map = new HashMap<>() ;
        map.put("house_id","1");

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminhousedetail,map);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);
                    System.out.println("String--"+s);
                    Gson gson = new Gson();
                    HouseDetailPojo h = gson.fromJson(s, HouseDetailPojo.class) ;
                    System.out.println("------------------------------------------");
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });

        try {
            Thread.sleep((BaseHttp.TIMEOUT_TIME_SECONDS+1)*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    //项目列表接口
    public static void project(){

        Call call = BaseHttp.getCallWithUrlAndData(Const.adminproject,null);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Fail");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String rr = new String(response.body().string());
                System.out.println(rr);
                int result = BaseHttp.preResult(rr);
                if(result==BaseHttp.DATA_OK){
                    String s = BaseHttp.getJsonDataAsString(rr);


                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<ProjectPojo>>(){}.getType();
                    List<ProjectPojo> projectList = gson.fromJson(s, listType);
                    System.out.println(Arrays.toString(projectList.toArray()));
                }else {
                    System.out.println(BaseHttp.getJsonMsg(rr));
                }

            }
        });



    }


    public static void print(Object o){
        System.out.println(o);
    }
}
